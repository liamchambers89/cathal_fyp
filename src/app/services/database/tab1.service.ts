import { Injectable } from '@angular/core';
import { ApiTab1Service } from "../api/api-tab1.service";
import { DatabaseService } from "./database.service";

@Injectable({
  providedIn: "root"
})
export class Tab1Service {
  url: String;
  herd: any;

  constructor(private db: DatabaseService, private api: ApiTab1Service) {
    this.url = api.getUrl();
  }

  async CallHerd() {
    try {
      var SQLArray = []; //hold the sql commands
      var i = 0;

      this.herd = [];

      // console.log(
      //   "HerdService: CallHerd: 1. ===  START LOADING HERD === " +
      //     moment().format("YYYYMMDDHHmmss")
      // );

      //console.log("HerdService: CallHerd: 2.url: " + this.url);

      await this.api.getHerd(this.url);

      //console.log("HerdService: CallHerd: 3. Called and finished Api.");

      this.herd = this.api.getHerdArray();

      // console.log(
      //   "HerdService: CallHerd: 4. HerdDetails Api: ",
      //   this.herd.length
      // );
      console.log(this.herd);

      // CORRECT AS FAR AS HERE ***********************

      // loop to insert into database
      await this.herd.forEach(herd => {
        let breed = "";
        for (let i = 0; i < herd.breed.length; i++) {
          breed += herd.breed[i].code + "(" + herd.breed[i].percent + "%) ";
        }
        console.log(
          "data going in" + herd.animal_id,
          herd.animal_name,
          herd.freeze_brand,
          herd.sex,
          herd.birth_date,
          herd.lactation,
          herd.dam_number,
          herd.sire_number,
          herd.sire_name,
          herd.arrive_date,
          breed,
          herd.scc_over_200,
          herd.scc,
          herd.return_code,
          herd.return_message
        );

        SQLArray.push([
          "INSERT OR REPLACE INTO HERD(ANIMAL_ID, ANIMAL_NAME , FREEZE_BRAND, SEX, BIRTH_DATE, LACTATION, DAM_NUMBER, SIRE_NUMBER, SIRE_NAME, ARRIVE_DATE, BREED, BIRTH_DTM_NUMERIC) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)",
          [
            herd.animal_id,
            herd.animal_name,
            herd.freeze_brand,
            herd.sex,
            herd.birth_date,
            herd.lactation,
            herd.dam_number,
            herd.sire_number,
            herd.sire_name,
            herd.arrive_date,
            breed,
            herd.birth_dtm_numeric
          ]
        ]);
        i = i + 1;
      });

      // console.log(
      //   "HerdService: CallHerd: 5. sqlBatch calling for " + i + " rows."
      // );

      await this.clearDatabase();

      await this.db.connection.sqlBatch(SQLArray).then(
        data => {
          console.log(
            "HerdService: CallHerd: 6. sqlBatch complete" + JSON.stringify(data)
          );
        },
        error => {
          console.log(
            "HerdService: CallHerd 6. error:" + (error as Error).message
          );
        }
      );

      // Replace null lactation values with 0's
      try {
        this.db.connection
          .executeSql(
            "UPDATE HERD SET LACTATION = ?  WHERE LACTATION IS NULL",
            [0]
          )
          .then(res => {
            console.log("UPDATED lactation nulls with 0 : ");
          })
          .catch(e => {
            console.log(e);
          });
      } catch (error) {
        console.log("error: UPDATE lactation " + error);
      }

     // console.log("HerdService: CallHerd: 7. Clearing local Array");
      this.herd = [];
      SQLArray = [];

      // clear the herd array
      //console.log("HerdService: CallHerd: 8. reset api array: ");
      this.api.clearHerdArray();

      // console.log(
      //   "HerdService: CallHerd: 9. === FINISHED LOADING HERD === " +
      //     moment().format("YYYYMMDDHHmmss")
      // );
    } catch (error) {
      console.log("CallHerd Error" + (error as Error).message);
    }
  }

  //  Load herd from SQLite db into array
  loadHerd() {
    return new Promise((resolve, reject) => {
      try {
        //console.log("db created - loading into array");
        this.db.connection
          .executeSql("SELECT * FROM HERD", [])
          .then(res => {
            var herd = [];
            console.log("loadHerd - now loading into array");
            console.log(res.rows.length);

            for (var i = 0; i < res.rows.length; i++) {
              herd.push({
                animal_number: res.rows.item(i).ANIMAL_ID,
                animal_name: res.rows.item(i).ANIMAL_NAME,
                freeze_brand: res.rows.item(i).FREEZE_BRAND,
                sex: res.rows.item(i).SEX,
                birth_date: res.rows.item(i).BIRTH_DATE,
                lactation: res.rows.item(i).LACTATION,
                dam_number: res.rows.item(i).DAM_NUMBER,
                sire_number: res.rows.item(i).SIRE_NUMBER,
                sire_name: res.rows.item(i).SIRE_NAME,
                arrive_date: res.rows.item(i).ARRIVE_DATE,
                breed: res.rows.item(i).BREED
              });
            }
            console.log("LOAD HERD:");
            console.log(herd);
            resolve(herd);
          })
          .catch(e => {
            console.log(e);
            reject(e);
          });
      } catch (error) {
        console.log("error: loadHerd - herd" + error);
        reject(error);
      }
    });
  }

  async clearDatabase() {
    this.db.connection.executeSql("DELETE FROM HERD", []);
  }
}

