import { Injectable } from "@angular/core";
import { SQLite, SQLiteObject } from "@ionic-native/sqlite/ngx";
import * as moment from "moment";
import { Platform } from "@ionic/angular";

@Injectable({
  providedIn: "root"
})
export class DatabaseService {
  public connection: SQLiteObject;
  public databaseName = "icbfMEQ";
  isCreated = false;

  constructor(private sqlite: SQLite, private plat: Platform) {
    this.plat.ready().then(() => {
      console.log("calling DB create ");
      this.sqlite.deleteDatabase({
        name: this.databaseName,
        location: "default"
      })
      this.createDatabaseAndTable();
    });
  }

  //********* CREATE DATABASE START *************/

  async createDatabaseAndTable() {
    console.log("create database and table");
    // Define the application SQLite database for herd details
    await this.sqlite
      .create({
        name: this.databaseName,
        location: "default"
      })
      .then((db: SQLiteObject) => {
        this.connection = db;
      })
      .catch(e => {
        console.log(e);
      });
      await this.creatAll();
    }
  
    async creatAll() {
      // Added the await command to wait for each function to finish before proceeding with the next
      await this.createHerdTable();
    }

  createHerdTable() {
    return new Promise((resolve, reject) => {
      try {
        this.connection
          .executeSql(
            "CREATE TABLE IF NOT EXISTS HERD(ID number, ANIMAL_ID text, ANIMAL_NAME text, FREEZE_BRAND number, SEX text, BIRTH_DATE text, LACTATION number, DAM_NUMBER text, SIRE_NUMBER text, SIRE_NAME text, ARRIVE_DATE text, BREED blob, BIRTH_DTM_NUMERIC text)",
            []
          )
          .then(res => {
            console.log("Executed SQL - TABLE HERD created");
            resolve(res);
          })
          .catch(e => {
            console.log(e);
            reject(e);
          });
      } catch (error) {
        console.log("error: createHerdTable" + error);
        reject(error);
      }
    });
  }

}
