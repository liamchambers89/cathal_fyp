import { Injectable } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ApiglobalService {
  // DEV
  public bearerstring: any;
  public baseurl = 'https://api.dev-icbf.com';
  public errorlist = [];

  public headers = new HttpHeaders();
  constructor(
    
  ) {
  }
  async getBearer() {
    console.log('---- GETTING BEARER ----');
    // await this.auth.retCurrentAccessToken()
    //   .then((data: AuthResponse) => {
    //     this.tokens = data;
    //   }
    //   ).catch(error => console.error("Error on getting Token Object ", error));
    // console.log("Token: ", this.tokens.access_token);
    // return this.tokens.access_token;
    return 'ef04b941b60a14ae415e7bcc5d21b30b1b5696ae';
  }
}