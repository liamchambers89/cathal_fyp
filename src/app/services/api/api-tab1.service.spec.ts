import { TestBed } from '@angular/core/testing';

import { ApiTab1Service } from './api-tab1.service';

describe('ApiTab1Service', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ApiTab1Service = TestBed.get(ApiTab1Service);
    expect(service).toBeTruthy();
  });
});
