import { Injectable } from '@angular/core';
import { ApiglobalService } from "../api/api-global.service";
import { HttpClient, HttpParams, HttpHeaders } from "@angular/common/http";
import { DatabaseService } from "../database/database.service";
import { Http, Headers, RequestOptions, URLSearchParams } from "@angular/http";
import { map } from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class ApiTab1Service {

  public herd = [];

  constructor(private apiglobal: ApiglobalService,
    private httpclient: HttpClient,
    private db: DatabaseService,
    private http: Http) { }

  // Get the url for herd details, each function will need this as its recursive
  public getUrl() {
    let url = this.apiglobal.baseurl + "/herd-plus/herd-animals";
    console.log("ApiTab1Service: getUrl: " + url);
    return url;
  }

  // Allows for Pagination by creating a recursive loop function
  // Also calling from HttpClient rather than Http which has been deprecated
  public getHerd(url) {
    let promise = new Promise(async (resolve, reject) => {
      try {
        let data: any;
        var token = await this.apiglobal.getBearer();

        let HttpOptions = {
          headers: new HttpHeaders().set(
            "Authorization",
            "Bearer " + token
          )
        };

        console.log(HttpOptions);

        console.log('ApiBullService:getbulls bearer', this.apiglobal.bearerstring, token);

        this.httpclient.get(url, HttpOptions).subscribe(
          (response: Response) => {
          console.log("ApiTab1Service:herd url", url);

            if (response != null) {
              data = response;
              //console.log("response from the api" + JSON.stringify(data));

              //NB Adding the next page of results to the array when copying make sure to check\change these parameters
              this.herd = this.herd.concat(data._embedded.herd_animals);

              resolve(data);
            }

            if (response == null) {
              console.log("ApiTab1Service:getHerd: resolved response null ><");
              resolve(data);
            }
          },
          // To handle an error from the herd details api
          (error: Error) => {
            console.log((error as Error).message);
            reject(error);
          }
        );
      } catch (error) {
        console.log("ApiTab1Service:getHerd error:" + (error as Error).message);

        // To handle an error from the herd details api
        reject(error);
      } finally {
      }
    });
    return promise;
  }

  // Called when getbulls has finished loading the array bulls
  public getHerdArray() {
    return this.herd;
  }

  // Clear Herd array
  public clearHerdArray() {
    this.herd = [];
  }
}
