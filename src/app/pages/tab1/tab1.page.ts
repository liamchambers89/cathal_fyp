import { Component, OnInit } from "@angular/core";
import { Tab1Service } from "../../services/database/tab1.service";
import { ApiTab1Service } from "../../services/api/api-tab1.service";
import { ModalController, MenuController, LoadingController } from "@ionic/angular";
import { ActivatedRoute, Router } from '@angular/router';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';

@Component({
  selector: "app-tab1",
  templateUrl: "tab1.page.html",
  styleUrls: ["tab1.page.scss"],
  providers: [Tab1Service]
})
export class Tab1Page {
  herd: any;
  hideSpinner = true;

  private loader;
  public filterActive = false;

  constructor(
    public sqlHerd: Tab1Service,
    private router: Router,
    private route: ActivatedRoute,
    public modalController: ModalController,
    public menu: MenuController,
    public loadingCtrl: LoadingController,
    public apiHerd: ApiTab1Service
  ) {
  }

  async ionViewDidEnter() {

    this.loader = await this.loadingCtrl.create({
      spinner: 'crescent',
      message: 'Loading Data..'
    });

    await this.loader.present();
    await this.sqlHerd.CallHerd();
    await this.delay(1000);
    await this.loadHerd();
    this.loader.dismiss();
  }

  async loadHerd() {
    console.log("loading herd");
    await this.sqlHerd.loadHerd().then(async herd => {

      console.log("res" + JSON.stringify(herd));

      this.herd = herd;
    });
  }

  delay(ms: number) {
    return new Promise( resolve => setTimeout(resolve, ms) );
  }
}
