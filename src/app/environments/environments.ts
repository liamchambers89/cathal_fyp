// export const environment = {
//     //prod
// api_url: 'https://api.icbf.com/'
// //dev
// //api_url: 'http://nc.api.dev64.icbf.com.dev64.icbf.com'
// }

export const environment = {
    production: false,
    baseUrl: 'https://api.icbf.com', //'http://apitest.dev64.icbf.com', 
    authUrl: 'https://api.icbf.com/oauth', //'http://apitest.dev64.icbf.com/oauth', 
    redirectUri: "https://api.icbf.com/client/callback"
  };
  

