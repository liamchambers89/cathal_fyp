import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpModule } from "../../node_modules/@angular/http";
import { HttpClientModule } from "@angular/common/http";
import {
  SecureStorage,
  SecureStorageObject
} from "@ionic-native/secure-storage/ngx";
import { NativeStorage } from "@ionic-native/native-storage/ngx";
import { SQLite } from '../../node_modules/@ionic-native/sqlite/ngx';
import { DatabaseService } from "./services/database/database.service";
import { Network } from '@ionic-native/network/ngx';

import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import { Tab2Page } from './pages/tab2/tab2.page';

import { AppVersion } from '@ionic-native/app-version/ngx';



@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [
    BrowserModule, 
    IonicModule.forRoot(), 
    AppRoutingModule,
    HttpClientModule,
    HttpModule,

  ],
  providers: [
    StatusBar,
    SplashScreen,
    SecureStorage,
    NativeStorage,
    DatabaseService,
    //QRScanner,
    BarcodeScanner,
    SQLite,
    Network,
    Tab2Page,
    AppVersion,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
