(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-dashboard-dashboard-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/dashboard/dashboard.page.html":
/*!*******************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/dashboard/dashboard.page.html ***!
  \*******************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<!-- <ion-header id=\"toolbar\">\n  <ion-toolbar>\n    <ion-title>dashboard</ion-title>\n  </ion-toolbar>\n</ion-header> -->\n\n<ion-content class=\"background\">\n  <div id=\"container\">\n    <ion-card id=\"card1\" (click)=\"scan()\">\n      <ion-grid id=\"grid\">\n        <ion-row id=\"row\">\n          <ion-col id=\"col\">\n              <span id=\"label\">Scan</span>\n          </ion-col>\n          <ion-col id=\"col2\">\n            <img style=\"width:80%; height:80%;\" src=\"../../../assets/images/qr-code.png\">\n          </ion-col>\n        </ion-row>\n      </ion-grid>\n    </ion-card>\n\n    <ion-card id=\"card2\" (click)=\"purchases()\">\n      <div id=\"container2\">\n        <ion-card-header>\n        <ion-card-title><span id=\"title\">Your Purchase History</span></ion-card-title>\n      </ion-card-header>\n      </div>\n      <div id=\"foot\">\n        Provide feedback on your purchases\n      </div>\n    </ion-card>\n\n    <ion-card id=\"card3\">\n      <div id=\"container2\">\n        <ion-card-header>\n        <ion-card-title><span id=\"title\">Most Popular Beef AI Sires in Suckler Herds</span></ion-card-title>\n      </ion-card-header>\n      </div>\n      <div id=\"foot\">\n        Coming Soon <!-- Click here for more details -->\n      </div>\n    </ion-card>\n    <h4 style='color:white; font-size:80%; text-align:center;'>Version {{VersionNumber}}</h4>\n  </div>\n</ion-content>\n");

/***/ }),

/***/ "./src/app/pages/dashboard/dashboard-routing.module.ts":
/*!*************************************************************!*\
  !*** ./src/app/pages/dashboard/dashboard-routing.module.ts ***!
  \*************************************************************/
/*! exports provided: DashboardPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DashboardPageRoutingModule", function() { return DashboardPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _dashboard_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./dashboard.page */ "./src/app/pages/dashboard/dashboard.page.ts");




const routes = [
    {
        path: '',
        component: _dashboard_page__WEBPACK_IMPORTED_MODULE_3__["DashboardPage"]
    }
];
let DashboardPageRoutingModule = class DashboardPageRoutingModule {
};
DashboardPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], DashboardPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/dashboard/dashboard.module.ts":
/*!*****************************************************!*\
  !*** ./src/app/pages/dashboard/dashboard.module.ts ***!
  \*****************************************************/
/*! exports provided: DashboardPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DashboardPageModule", function() { return DashboardPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _dashboard_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./dashboard-routing.module */ "./src/app/pages/dashboard/dashboard-routing.module.ts");
/* harmony import */ var _dashboard_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./dashboard.page */ "./src/app/pages/dashboard/dashboard.page.ts");







let DashboardPageModule = class DashboardPageModule {
};
DashboardPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _dashboard_routing_module__WEBPACK_IMPORTED_MODULE_5__["DashboardPageRoutingModule"]
        ],
        declarations: [_dashboard_page__WEBPACK_IMPORTED_MODULE_6__["DashboardPage"]]
    })
], DashboardPageModule);



/***/ }),

/***/ "./src/app/pages/dashboard/dashboard.page.scss":
/*!*****************************************************!*\
  !*** ./src/app/pages/dashboard/dashboard.page.scss ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".background {\n  --background: black;\n}\n\n#container {\n  height: 90%;\n  padding-left: 2%;\n  padding-right: 2%;\n  margin: auto;\n  position: absolute;\n  top: 0;\n  left: 0;\n  bottom: 0;\n  right: 0;\n}\n\n#card1 {\n  height: 31%;\n  background: #212121;\n  opacity: 0.9;\n  color: white;\n}\n\n#card2 {\n  height: 31%;\n  background: url('steakSliced.jpg') 0 0/100% 100% no-repeat;\n  color: white;\n}\n\n#card3 {\n  height: 31%;\n  background: url('herd.jpg') 0 0/100% 100% no-repeat;\n  color: white;\n}\n\n#scan {\n  font-size: 400%;\n  text-align: center;\n}\n\n#label {\n  display: flex;\n  align-items: center;\n  justify-content: center;\n}\n\n#grid {\n  height: 100%;\n}\n\n#row {\n  height: 100%;\n}\n\n#col {\n  height: 100%;\n  font-size: 400%;\n  display: flex;\n  align-items: center;\n  justify-content: center;\n}\n\n#col2 {\n  display: flex;\n  align-items: center;\n  justify-content: center;\n}\n\n#container2 {\n  height: 80%;\n}\n\n#foot {\n  background: #212121;\n  opacity: 0.9;\n  height: 20%;\n  display: flex;\n  align-items: center;\n  padding-left: 5%;\n}\n\n#title {\n  color: white;\n  font-size: 120%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9saWFtY2hhbWJlcnMvRG9jdW1lbnRzL21lYXRlYXRpbmdxdWFsaXR5L3NyYy9hcHAvcGFnZXMvZGFzaGJvYXJkL2Rhc2hib2FyZC5wYWdlLnNjc3MiLCJzcmMvYXBwL3BhZ2VzL2Rhc2hib2FyZC9kYXNoYm9hcmQucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBRUksbUJBQUE7QUNBSjs7QURHQTtFQUNJLFdBQUE7RUFDQSxnQkFBQTtFQUNBLGlCQUFBO0VBQ0EsWUFBQTtFQUNGLGtCQUFBO0VBQ0EsTUFBQTtFQUFRLE9BQUE7RUFBUyxTQUFBO0VBQVcsUUFBQTtBQ0c5Qjs7QURBQTtFQUNHLFdBQUE7RUFDQSxtQkFBQTtFQUNBLFlBQUE7RUFDQSxZQUFBO0FDR0g7O0FEQUE7RUFDSSxXQUFBO0VBQ0EsMERBQUE7RUFDQSxZQUFBO0FDR0o7O0FEQUM7RUFDRyxXQUFBO0VBQ0EsbURBQUE7RUFDQSxZQUFBO0FDR0o7O0FEQUE7RUFDSSxlQUFBO0VBQ0Esa0JBQUE7QUNHSjs7QURBQTtFQUNJLGFBQUE7RUFDQSxtQkFBQTtFQUNBLHVCQUFBO0FDR0o7O0FEQUE7RUFDSSxZQUFBO0FDR0o7O0FEQUE7RUFDSSxZQUFBO0FDR0o7O0FEQUE7RUFDSSxZQUFBO0VBQ0EsZUFBQTtFQUNBLGFBQUE7RUFDQSxtQkFBQTtFQUNBLHVCQUFBO0FDR0o7O0FEQUE7RUFDSSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSx1QkFBQTtBQ0dKOztBREFBO0VBQ0ksV0FBQTtBQ0dKOztBREFBO0VBQ0ksbUJBQUE7RUFDQSxZQUFBO0VBQ0EsV0FBQTtFQUNBLGFBQUE7RUFDQSxtQkFBQTtFQUNBLGdCQUFBO0FDR0o7O0FEQUE7RUFDSSxZQUFBO0VBQ0EsZUFBQTtBQ0dKIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvZGFzaGJvYXJkL2Rhc2hib2FyZC5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuYmFja2dyb3VuZHtcbiAgICAvLy0tYmFja2dyb3VuZDogdXJsKC4uLy4uLy4uL2Fzc2V0cy9pbWFnZXMvaW1hZ2UuanBnKSAwIDAvMTAwJSAxMDAlIG5vLXJlcGVhdDtcbiAgICAtLWJhY2tncm91bmQ6IGJsYWNrO1xufVxuXG4jY29udGFpbmVye1xuICAgIGhlaWdodDogOTAlO1xuICAgIHBhZGRpbmctbGVmdDoyJTtcbiAgICBwYWRkaW5nLXJpZ2h0OjIlO1xuICAgIG1hcmdpbjogYXV0bztcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB0b3A6IDA7IGxlZnQ6IDA7IGJvdHRvbTogMDsgcmlnaHQ6IDA7XG59XG5cbiNjYXJkMXtcbiAgIGhlaWdodDozMSU7XG4gICBiYWNrZ3JvdW5kOiMyMTIxMjE7XG4gICBvcGFjaXR5OjAuOTtcbiAgIGNvbG9yOndoaXRlO1xufVxuXG4jY2FyZDJ7XG4gICAgaGVpZ2h0OjMxJTtcbiAgICBiYWNrZ3JvdW5kOiB1cmwoLi4vLi4vLi4vYXNzZXRzL2ltYWdlcy9zdGVha1NsaWNlZC5qcGcpMCAwLzEwMCUgMTAwJSBuby1yZXBlYXQ7XG4gICAgY29sb3I6d2hpdGU7XG4gfVxuXG4gI2NhcmQze1xuICAgIGhlaWdodDozMSU7XG4gICAgYmFja2dyb3VuZDogdXJsKC4uLy4uLy4uL2Fzc2V0cy9pbWFnZXMvaGVyZC5qcGcpIDAgMC8xMDAlIDEwMCUgbm8tcmVwZWF0O1xuICAgIGNvbG9yOndoaXRlO1xuIH1cblxuI3NjYW57XG4gICAgZm9udC1zaXplOjQwMCU7XG4gICAgdGV4dC1hbGlnbjpjZW50ZXI7XG59XG5cbiNsYWJlbHtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7IFxuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xufVxuXG4jZ3JpZHtcbiAgICBoZWlnaHQ6MTAwJTtcbn1cblxuI3Jvd3tcbiAgICBoZWlnaHQ6MTAwJTtcbn1cblxuI2NvbHtcbiAgICBoZWlnaHQ6MTAwJTtcbiAgICBmb250LXNpemU6NDAwJTtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7IFxuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xufVxuXG4jY29sMntcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7IFxuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xufVxuXG4jY29udGFpbmVyMntcbiAgICBoZWlnaHQ6ODAlO1xufVxuXG4jZm9vdHtcbiAgICBiYWNrZ3JvdW5kOiAjMjEyMTIxO1xuICAgIG9wYWNpdHk6MC45O1xuICAgIGhlaWdodDoyMCU7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBhbGlnbi1pdGVtczogY2VudGVyOyBcbiAgICBwYWRkaW5nLWxlZnQ6NSU7XG59XG5cbiN0aXRsZXtcbiAgICBjb2xvcjp3aGl0ZTtcbiAgICBmb250LXNpemU6MTIwJTtcbn1cbiIsIi5iYWNrZ3JvdW5kIHtcbiAgLS1iYWNrZ3JvdW5kOiBibGFjaztcbn1cblxuI2NvbnRhaW5lciB7XG4gIGhlaWdodDogOTAlO1xuICBwYWRkaW5nLWxlZnQ6IDIlO1xuICBwYWRkaW5nLXJpZ2h0OiAyJTtcbiAgbWFyZ2luOiBhdXRvO1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHRvcDogMDtcbiAgbGVmdDogMDtcbiAgYm90dG9tOiAwO1xuICByaWdodDogMDtcbn1cblxuI2NhcmQxIHtcbiAgaGVpZ2h0OiAzMSU7XG4gIGJhY2tncm91bmQ6ICMyMTIxMjE7XG4gIG9wYWNpdHk6IDAuOTtcbiAgY29sb3I6IHdoaXRlO1xufVxuXG4jY2FyZDIge1xuICBoZWlnaHQ6IDMxJTtcbiAgYmFja2dyb3VuZDogdXJsKC4uLy4uLy4uL2Fzc2V0cy9pbWFnZXMvc3RlYWtTbGljZWQuanBnKSAwIDAvMTAwJSAxMDAlIG5vLXJlcGVhdDtcbiAgY29sb3I6IHdoaXRlO1xufVxuXG4jY2FyZDMge1xuICBoZWlnaHQ6IDMxJTtcbiAgYmFja2dyb3VuZDogdXJsKC4uLy4uLy4uL2Fzc2V0cy9pbWFnZXMvaGVyZC5qcGcpIDAgMC8xMDAlIDEwMCUgbm8tcmVwZWF0O1xuICBjb2xvcjogd2hpdGU7XG59XG5cbiNzY2FuIHtcbiAgZm9udC1zaXplOiA0MDAlO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG5cbiNsYWJlbCB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xufVxuXG4jZ3JpZCB7XG4gIGhlaWdodDogMTAwJTtcbn1cblxuI3JvdyB7XG4gIGhlaWdodDogMTAwJTtcbn1cblxuI2NvbCB7XG4gIGhlaWdodDogMTAwJTtcbiAgZm9udC1zaXplOiA0MDAlO1xuICBkaXNwbGF5OiBmbGV4O1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbn1cblxuI2NvbDIge1xuICBkaXNwbGF5OiBmbGV4O1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbn1cblxuI2NvbnRhaW5lcjIge1xuICBoZWlnaHQ6IDgwJTtcbn1cblxuI2Zvb3Qge1xuICBiYWNrZ3JvdW5kOiAjMjEyMTIxO1xuICBvcGFjaXR5OiAwLjk7XG4gIGhlaWdodDogMjAlO1xuICBkaXNwbGF5OiBmbGV4O1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBwYWRkaW5nLWxlZnQ6IDUlO1xufVxuXG4jdGl0bGUge1xuICBjb2xvcjogd2hpdGU7XG4gIGZvbnQtc2l6ZTogMTIwJTtcbn0iXX0= */");

/***/ }),

/***/ "./src/app/pages/dashboard/dashboard.page.ts":
/*!***************************************************!*\
  !*** ./src/app/pages/dashboard/dashboard.page.ts ***!
  \***************************************************/
/*! exports provided: DashboardPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DashboardPage", function() { return DashboardPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_native_app_version_ngx__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic-native/app-version/ngx */ "./node_modules/@ionic-native/app-version/ngx/index.js");




let DashboardPage = class DashboardPage {
    constructor(router, appVersion) {
        this.router = router;
        this.appVersion = appVersion;
    }
    ngOnInit() {
    }
    ionViewWillEnter() {
        this.appVersion.getVersionNumber().then(value => {
            this.VersionNumber = value;
        }).catch(err => {
            console.log(err);
        });
        console.log(this.appVersion);
    }
    scan() {
        this.router.navigateByUrl("/tabs");
    }
    purchases() {
        this.router.navigateByUrl("/tabs/tab2");
    }
};
DashboardPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _ionic_native_app_version_ngx__WEBPACK_IMPORTED_MODULE_3__["AppVersion"] }
];
DashboardPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-dashboard',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./dashboard.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/dashboard/dashboard.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./dashboard.page.scss */ "./src/app/pages/dashboard/dashboard.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
        _ionic_native_app_version_ngx__WEBPACK_IMPORTED_MODULE_3__["AppVersion"]])
], DashboardPage);



/***/ })

}]);
//# sourceMappingURL=pages-dashboard-dashboard-module-es2015.js.map