(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["tab1-tab1-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/tab1/tab1.page.html":
/*!*********************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/tab1/tab1.page.html ***!
  \*********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header [translucent]=\"true\">\n  <ion-toolbar>\n    <ion-title>\n      <ion-icon (click)=\"home()\" id=\"home\" src=\"assets/icon/home.svg\" ></ion-icon>\n    </ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content [fullscreen]=\"true\" class=\"background\">\n  <ion-header collapse=\"condense\">\n    <ion-toolbar>\n    </ion-toolbar>\n  </ion-header>\n  <ion-content class=\"background\">\n    <div id=\"container\">\n      <ion-grid id=\"grid\">\n        <ion-row id=\"row1\">\n          <ion-col>\n            <label id=\"scanLabel\">Tap to scan</label>\n          </ion-col>\n        </ion-row>\n        <ion-row id=\"row2\">\n          <ion-col>\n            <span (click)=\"scanCode()\" class='pulse-button'><ion-icon id=\"qrPulse\" src=\"assets/icon/qr-code.svg\"></ion-icon></span>\n          </ion-col>\n        </ion-row>\n        <br>\n        <br>\n        <br>\n        <br>\n        <ion-row id=\"inputRow\">\n          <ion-col>\n            <label id=\"manual\">\n              Trouble Scanning? Enter manually.\n            </label>\n            <ion-input id=\"manualInput\" (ionChange)=\"validateTag()\" [(ngModel)]=\"manualTag\" placeholder=\"Enter Tag\"></ion-input>\n          </ion-col>\n        </ion-row>\n        <ion-row id=\"elem\" style=\"width:100%; padding-left:30%; padding-right:30%;\">\n          <ion-col>\n            <br>\n            <ion-button disabled={{buttonDisabled}} (click)=\"manualScan()\" id=\"manualBtn\" background=\"black\" shape=\"round\" expand=\"block\" color=\"white\" fill=\"outline\"><span style=\"text-transform:none!important;\">Continue</span></ion-button>\n          </ion-col>\n        </ion-row>\n      </ion-grid> \n      <!-- <div class='container1'>\n        <label id=\"scanLabel\">Tap to scan.</label>\n      </div>\n      <div class='container2'>\n        <span class='pulse-button'><ion-icon id=\"qrPulse\" src=\"assets/icon/qr-code.svg\"></ion-icon></span>\n      </div> -->\n    </div>\n  </ion-content>\n\n  <!-- <app-explore-container name=\"Tab 1 page\"></app-explore-container> -->\n</ion-content>\n");

/***/ }),

/***/ "./src/app/pages/tab1/tab1-routing.module.ts":
/*!***************************************************!*\
  !*** ./src/app/pages/tab1/tab1-routing.module.ts ***!
  \***************************************************/
/*! exports provided: Tab1PageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Tab1PageRoutingModule", function() { return Tab1PageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _tab1_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./tab1.page */ "./src/app/pages/tab1/tab1.page.ts");




const routes = [
    {
        path: '',
        component: _tab1_page__WEBPACK_IMPORTED_MODULE_3__["Tab1Page"],
    }
];
let Tab1PageRoutingModule = class Tab1PageRoutingModule {
};
Tab1PageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })
], Tab1PageRoutingModule);



/***/ }),

/***/ "./src/app/pages/tab1/tab1.module.ts":
/*!*******************************************!*\
  !*** ./src/app/pages/tab1/tab1.module.ts ***!
  \*******************************************/
/*! exports provided: Tab1PageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Tab1PageModule", function() { return Tab1PageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _tab1_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./tab1.page */ "./src/app/pages/tab1/tab1.page.ts");
/* harmony import */ var _explore_container_explore_container_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../explore-container/explore-container.module */ "./src/app/explore-container/explore-container.module.ts");
/* harmony import */ var _tab1_routing_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./tab1-routing.module */ "./src/app/pages/tab1/tab1-routing.module.ts");








let Tab1PageModule = class Tab1PageModule {
};
Tab1PageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
        imports: [
            _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["IonicModule"],
            _angular_common__WEBPACK_IMPORTED_MODULE_3__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"],
            _explore_container_explore_container_module__WEBPACK_IMPORTED_MODULE_6__["ExploreContainerComponentModule"],
            _tab1_routing_module__WEBPACK_IMPORTED_MODULE_7__["Tab1PageRoutingModule"]
        ],
        declarations: [_tab1_page__WEBPACK_IMPORTED_MODULE_5__["Tab1Page"]]
    })
], Tab1PageModule);



/***/ }),

/***/ "./src/app/pages/tab1/tab1.page.scss":
/*!*******************************************!*\
  !*** ./src/app/pages/tab1/tab1.page.scss ***!
  \*******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("#home {\n  color: white;\n  font-size: 150%;\n  padding-top: 1%;\n}\n\n.background {\n  --background: black;\n  text-align: center;\n}\n\n#grid {\n  height: 100%;\n}\n\n#row1 {\n  height: 10%;\n  width: 100%;\n}\n\n#row2 {\n  height: 40%;\n  width: 100%;\n}\n\n#row3 {\n  height: 20%;\n  width: 100%;\n  color: white;\n  text-align: center;\n  padding-top: 5%;\n}\n\n#barCol {\n  height: 100%;\n}\n\n#qrCol {\n  height: 100%;\n}\n\n#barCard {\n  height: 95%;\n  background: #212121;\n}\n\n#qrCard {\n  height: 95%;\n  --background: url('blur.jpg') 0 0/100% 100% no-repeat;\n}\n\n#manualCol {\n  color: white;\n  text-align: center;\n  padding-top: 5%;\n}\n\nion-input {\n  --color: white;\n}\n\n#qrImgCol {\n  height: 100%;\n  font-size: 400%;\n  display: flex;\n  align-items: center;\n  justify-content: center;\n}\n\n#labelCol {\n  height: 100%;\n  font-size: 400%;\n  display: flex;\n  align-items: center;\n  justify-content: center;\n}\n\n#label {\n  display: flex;\n  align-items: center;\n  justify-content: center;\n  color: white;\n  font-size: 65%;\n  padding-left: 10%;\n}\n\n#row {\n  height: 100%;\n}\n\n#grid2 {\n  height: 100%;\n}\n\n#scanPic {\n  width: 40%;\n  margin-bottom: 5%;\n}\n\n#scanBtn {\n  color: white;\n  height: 120%;\n  width: 100%;\n  font-size: 125%;\n  padding-left: 2%;\n  padding-right: 2%;\n}\n\n#container {\n  padding-top: 10%;\n  height: 100%;\n  width: 100%;\n  position: absolute;\n  /*it can be fixed too*/\n  left: 0;\n  right: 0;\n  top: 0;\n  bottom: 0;\n  margin: auto;\n  display: flex;\n  align-items: center;\n  justify-content: center;\n}\n\n#grid {\n  height: 100%;\n}\n\n#manual {\n  margin-top: 5%;\n  color: white;\n  text-align: center;\n}\n\n#manualInput {\n  color: white;\n  margin-top: 5%;\n  border-bottom: 1px solid white;\n}\n\n#inputRow {\n  padding-left: 10%;\n  padding-right: 10%;\n}\n\n#or {\n  font-size: 250%;\n  text-align: center;\n  color: white;\n}\n\n#orRow {\n  margin-top: 10%;\n  margin-bottom: 10%;\n}\n\n#manualBtn {\n  margin-top: 5%;\n}\n\n.container1 {\n  width: 100%;\n  height: 20%;\n  margin: 0 auto 0;\n  perspective: 1000;\n  -webkit-backface-visibility: hidden;\n          backface-visibility: hidden;\n  background: black;\n  border: 1px solid white;\n}\n\n.container2 {\n  width: 100%;\n  height: 50%;\n  margin: 0 auto 0;\n  perspective: 1000;\n  -webkit-backface-visibility: hidden;\n          backface-visibility: hidden;\n  background: black;\n  border: 1px solid white;\n}\n\n.pulse-button {\n  position: absolute;\n  /*it can be fixed too*/\n  left: 0;\n  right: 0;\n  top: 0;\n  bottom: 0;\n  margin: auto;\n  display: flex;\n  align-items: center;\n  justify-content: center;\n  display: block;\n  width: 130px;\n  height: 130px;\n  font-size: 1.3em;\n  font-weight: light;\n  font-family: \"Trebuchet MS\", sans-serif;\n  text-transform: uppercase;\n  text-align: center;\n  line-height: 130px;\n  letter-spacing: -1px;\n  color: white;\n  border: none;\n  border-radius: 50%;\n  background: #232b2b;\n  cursor: pointer;\n  box-shadow: 0 0 0 0 rgba(96, 96, 96, 0.5);\n  -webkit-animation: pulse 1.5s infinite;\n          animation: pulse 1.5s infinite;\n}\n\n.pulse-button:hover {\n  -webkit-animation: none;\n          animation: none;\n}\n\n@-webkit-keyframes pulse {\n  0% {\n    transform: scale(0.9);\n  }\n  70% {\n    transform: scale(1);\n    box-shadow: 0 0 0 50px rgba(90, 153, 212, 0);\n  }\n  100% {\n    transform: scale(0.9);\n    box-shadow: 0 0 0 0 rgba(90, 153, 212, 0);\n  }\n}\n\n@keyframes pulse {\n  0% {\n    transform: scale(0.9);\n  }\n  70% {\n    transform: scale(1);\n    box-shadow: 0 0 0 50px rgba(90, 153, 212, 0);\n  }\n  100% {\n    transform: scale(0.9);\n    box-shadow: 0 0 0 0 rgba(90, 153, 212, 0);\n  }\n}\n\n#qrPulse {\n  font-size: 230%;\n  position: absolute;\n  /*it can be fixed too*/\n  left: 0;\n  right: 0;\n  top: 0;\n  bottom: 0;\n  margin: auto;\n  display: flex;\n  align-items: center;\n  justify-content: center;\n}\n\n#scanLabel {\n  color: white;\n  text-align: center;\n  font-size: 160%;\n  margin-bottom: 20%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9saWFtY2hhbWJlcnMvRG9jdW1lbnRzL21lYXRlYXRpbmdxdWFsaXR5L3NyYy9hcHAvcGFnZXMvdGFiMS90YWIxLnBhZ2Uuc2NzcyIsInNyYy9hcHAvcGFnZXMvdGFiMS90YWIxLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFDQTtFQUNJLFlBQUE7RUFDQSxlQUFBO0VBQ0EsZUFBQTtBQ0FKOztBREdBO0VBRUksbUJBQUE7RUFDQSxrQkFBQTtBQ0RKOztBRElBO0VBQ0ksWUFBQTtBQ0RKOztBREtBO0VBQ0ksV0FBQTtFQUNBLFdBQUE7QUNGSjs7QURLQTtFQUNJLFdBQUE7RUFDQSxXQUFBO0FDRko7O0FES0E7RUFDSSxXQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSxrQkFBQTtFQUNBLGVBQUE7QUNGSjs7QURLQTtFQUNJLFlBQUE7QUNGSjs7QURNQTtFQUNJLFlBQUE7QUNISjs7QURPQTtFQUNJLFdBQUE7RUFDQSxtQkFBQTtBQ0pKOztBRE9BO0VBQ0ksV0FBQTtFQUVBLHFEQUFBO0FDTEo7O0FEUUE7RUFDSSxZQUFBO0VBQ0Esa0JBQUE7RUFDQSxlQUFBO0FDTEo7O0FEUUE7RUFDSSxjQUFBO0FDTEo7O0FEUUE7RUFDSSxZQUFBO0VBQ0EsZUFBQTtFQUNBLGFBQUE7RUFDQSxtQkFBQTtFQUNBLHVCQUFBO0FDTEo7O0FEUUE7RUFDSSxZQUFBO0VBQ0EsZUFBQTtFQUNBLGFBQUE7RUFDQSxtQkFBQTtFQUNBLHVCQUFBO0FDTEo7O0FEU0E7RUFDSSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSx1QkFBQTtFQUNBLFlBQUE7RUFDQSxjQUFBO0VBQ0EsaUJBQUE7QUNOSjs7QURTQTtFQUNJLFlBQUE7QUNOSjs7QURTQTtFQUNJLFlBQUE7QUNOSjs7QURTQTtFQUNFLFVBQUE7RUFDQSxpQkFBQTtBQ05GOztBRFVBO0VBQ0ksWUFBQTtFQUNBLFlBQUE7RUFDQSxXQUFBO0VBQ0EsZUFBQTtFQUNBLGdCQUFBO0VBQ0EsaUJBQUE7QUNQSjs7QURVQTtFQUNJLGdCQUFBO0VBQ0EsWUFBQTtFQUNBLFdBQUE7RUFDQSxrQkFBQTtFQUFtQixzQkFBQTtFQUNmLE9BQUE7RUFBUSxRQUFBO0VBQ1IsTUFBQTtFQUFPLFNBQUE7RUFDUCxZQUFBO0VBQ0EsYUFBQTtFQUNKLG1CQUFBO0VBQ0EsdUJBQUE7QUNKSjs7QURRQTtFQUNJLFlBQUE7QUNMSjs7QURRQTtFQUNJLGNBQUE7RUFDQSxZQUFBO0VBQ0Esa0JBQUE7QUNMSjs7QURRQTtFQUNJLFlBQUE7RUFDQSxjQUFBO0VBQ0EsOEJBQUE7QUNMSjs7QURRQTtFQUNJLGlCQUFBO0VBQ0Esa0JBQUE7QUNMSjs7QURRQTtFQUNJLGVBQUE7RUFDQSxrQkFBQTtFQUNBLFlBQUE7QUNMSjs7QURRQTtFQUNJLGVBQUE7RUFDQSxrQkFBQTtBQ0xKOztBRFFBO0VBQ0ksY0FBQTtBQ0xKOztBRFFBO0VBQ0ksV0FBQTtFQUNBLFdBQUE7RUFDQSxnQkFBQTtFQUNBLGlCQUFBO0VBQ0EsbUNBQUE7VUFBQSwyQkFBQTtFQUNBLGlCQUFBO0VBQ0EsdUJBQUE7QUNMSjs7QURRQTtFQUNJLFdBQUE7RUFDQSxXQUFBO0VBQ0EsZ0JBQUE7RUFDQSxpQkFBQTtFQUNBLG1DQUFBO1VBQUEsMkJBQUE7RUFDQSxpQkFBQTtFQUNBLHVCQUFBO0FDTEo7O0FEUUU7RUFDRSxrQkFBQTtFQUFtQixzQkFBQTtFQUNmLE9BQUE7RUFBUSxRQUFBO0VBQ1IsTUFBQTtFQUFPLFNBQUE7RUFDUCxZQUFBO0VBQ0EsYUFBQTtFQUNKLG1CQUFBO0VBQ0EsdUJBQUE7RUFDQSxjQUFBO0VBQ0EsWUFBQTtFQUNBLGFBQUE7RUFDQSxnQkFBQTtFQUNBLGtCQUFBO0VBQ0EsdUNBQUE7RUFDQSx5QkFBQTtFQUNBLGtCQUFBO0VBQ0Esa0JBQUE7RUFDQSxvQkFBQTtFQUNBLFlBQUE7RUFDQSxZQUFBO0VBQ0Esa0JBQUE7RUFDQSxtQkFBQTtFQUNBLGVBQUE7RUFDQSx5Q0FBQTtFQUNBLHNDQUFBO1VBQUEsOEJBQUE7QUNGSjs7QURJRTtFQUNFLHVCQUFBO1VBQUEsZUFBQTtBQ0RKOztBRElFO0VBQ0U7SUFDQyxxQkFBQTtFQ0RIO0VER0U7SUFDRSxtQkFBQTtJQUNBLDRDQUFBO0VDREo7RURHSTtJQUNDLHFCQUFBO0lBQ0QseUNBQUE7RUNESjtBQUNGOztBRFZFO0VBQ0U7SUFDQyxxQkFBQTtFQ0RIO0VER0U7SUFDRSxtQkFBQTtJQUNBLDRDQUFBO0VDREo7RURHSTtJQUNDLHFCQUFBO0lBQ0QseUNBQUE7RUNESjtBQUNGOztBRElFO0VBQ0UsZUFBQTtFQUNBLGtCQUFBO0VBQW1CLHNCQUFBO0VBQ2YsT0FBQTtFQUFRLFFBQUE7RUFDUixNQUFBO0VBQU8sU0FBQTtFQUNQLFlBQUE7RUFDQSxhQUFBO0VBQ0osbUJBQUE7RUFDQSx1QkFBQTtBQ0NKOztBREVFO0VBQ0ksWUFBQTtFQUNBLGtCQUFBO0VBQ0EsZUFBQTtFQUNBLGtCQUFBO0FDQ04iLCJmaWxlIjoic3JjL2FwcC9wYWdlcy90YWIxL3RhYjEucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiXG4jaG9tZXtcbiAgICBjb2xvcjp3aGl0ZTtcbiAgICBmb250LXNpemU6MTUwJTtcbiAgICBwYWRkaW5nLXRvcDoxJTtcbn1cblxuLmJhY2tncm91bmR7XG4gICAgLy8tLWJhY2tncm91bmQ6IHVybCguLi8uLi8uLi9hc3NldHMvaW1hZ2VzL2ltYWdlLmpwZykgMCAwLzEwMCUgMTAwJSBuby1yZXBlYXQ7XG4gICAgLS1iYWNrZ3JvdW5kOiBibGFjaztcbiAgICB0ZXh0LWFsaWduOmNlbnRlcjtcbn1cblxuI2dyaWR7XG4gICAgaGVpZ2h0OjEwMCU7XG4gICAgXG59XG5cbiNyb3cxe1xuICAgIGhlaWdodDoxMCU7XG4gICAgd2lkdGg6MTAwJTtcbn1cblxuI3JvdzJ7XG4gICAgaGVpZ2h0OjQwJTtcbiAgICB3aWR0aDoxMDAlO1xufVxuXG4jcm93M3tcbiAgICBoZWlnaHQ6MjAlO1xuICAgIHdpZHRoOjEwMCU7XG4gICAgY29sb3I6d2hpdGU7XG4gICAgdGV4dC1hbGlnbjpjZW50ZXI7XG4gICAgcGFkZGluZy10b3A6NSU7XG59XG5cbiNiYXJDb2x7XG4gICAgaGVpZ2h0OjEwMCU7XG5cbn1cblxuI3FyQ29se1xuICAgIGhlaWdodDoxMDAlO1xuXG59XG5cbiNiYXJDYXJke1xuICAgIGhlaWdodDo5NSU7XG4gICAgYmFja2dyb3VuZDogIzIxMjEyMTtcbn1cblxuI3FyQ2FyZHtcbiAgICBoZWlnaHQ6OTUlO1xuICAgIC8vYmFja2dyb3VuZDogIzIxMjEyMTtcbiAgICAtLWJhY2tncm91bmQ6IHVybCguLi8uLi8uLi9hc3NldHMvaW1hZ2VzL2JsdXIuanBnKSAwIDAvMTAwJSAxMDAlIG5vLXJlcGVhdDtcbn1cblxuI21hbnVhbENvbHtcbiAgICBjb2xvcjp3aGl0ZTtcbiAgICB0ZXh0LWFsaWduOmNlbnRlcjtcbiAgICBwYWRkaW5nLXRvcDo1JTtcbn1cblxuaW9uLWlucHV0e1xuICAgIC0tY29sb3I6IHdoaXRlO1xufVxuXG4jcXJJbWdDb2x7XG4gICAgaGVpZ2h0OjEwMCU7XG4gICAgZm9udC1zaXplOjQwMCU7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBhbGlnbi1pdGVtczogY2VudGVyOyBcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbn1cblxuI2xhYmVsQ29se1xuICAgIGhlaWdodDoxMDAlO1xuICAgIGZvbnQtc2l6ZTo0MDAlO1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjsgXG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgXG59XG5cbiNsYWJlbHtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7IFxuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgIGNvbG9yOndoaXRlO1xuICAgIGZvbnQtc2l6ZTo2NSU7XG4gICAgcGFkZGluZy1sZWZ0OjEwJVxufVxuXG4jcm93e1xuICAgIGhlaWdodDoxMDAlO1xufVxuXG4jZ3JpZDJ7XG4gICAgaGVpZ2h0OjEwMCU7XG59XG5cbiNzY2FuUGlje1xuICB3aWR0aDo0MCU7XG4gIG1hcmdpbi1ib3R0b206NSU7XG4gICAgXG59XG5cbiNzY2FuQnRue1xuICAgIGNvbG9yOndoaXRlO1xuICAgIGhlaWdodDoxMjAlO1xuICAgIHdpZHRoOjEwMCU7XG4gICAgZm9udC1zaXplOjEyNSU7XG4gICAgcGFkZGluZy1sZWZ0OjIlO1xuICAgIHBhZGRpbmctcmlnaHQ6MiU7XG59XG5cbiNjb250YWluZXJ7XG4gICAgcGFkZGluZy10b3A6MTAlO1xuICAgIGhlaWdodDoxMDAlO1xuICAgIHdpZHRoOjEwMCU7XG4gICAgcG9zaXRpb246YWJzb2x1dGU7IC8qaXQgY2FuIGJlIGZpeGVkIHRvbyovXG4gICAgICAgIGxlZnQ6MDsgcmlnaHQ6MDtcbiAgICAgICAgdG9wOjA7IGJvdHRvbTowO1xuICAgICAgICBtYXJnaW46YXV0bztcbiAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICBhbGlnbi1pdGVtczogY2VudGVyOyBcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcblxufVxuXG4jZ3JpZHtcbiAgICBoZWlnaHQ6MTAwJTtcbn1cblxuI21hbnVhbHtcbiAgICBtYXJnaW4tdG9wOjUlO1xuICAgIGNvbG9yOndoaXRlO1xuICAgIHRleHQtYWxpZ246Y2VudGVyO1xufVxuXG4jbWFudWFsSW5wdXR7XG4gICAgY29sb3I6d2hpdGU7XG4gICAgbWFyZ2luLXRvcDo1JTtcbiAgICBib3JkZXItYm90dG9tOjFweCBzb2xpZCB3aGl0ZTtcbn1cblxuI2lucHV0Um93e1xuICAgIHBhZGRpbmctbGVmdDoxMCU7XG4gICAgcGFkZGluZy1yaWdodDoxMCU7XG59XG5cbiNvcntcbiAgICBmb250LXNpemU6MjUwJTtcbiAgICB0ZXh0LWFsaWduOmNlbnRlcjtcbiAgICBjb2xvcjp3aGl0ZTtcbn1cblxuI29yUm93e1xuICAgIG1hcmdpbi10b3A6MTAlO1xuICAgIG1hcmdpbi1ib3R0b206MTAlO1xufVxuXG4jbWFudWFsQnRue1xuICAgIG1hcmdpbi10b3A6NSU7XG59XG5cbi5jb250YWluZXIxIHtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBoZWlnaHQ6IDIwJTtcbiAgICBtYXJnaW46IDAgYXV0byAwO1xuICAgIHBlcnNwZWN0aXZlOiAxMDAwO1xuICAgIGJhY2tmYWNlLXZpc2liaWxpdHk6IGhpZGRlbjtcbiAgICBiYWNrZ3JvdW5kOiBibGFjaztcbiAgICBib3JkZXI6MXB4IHNvbGlkIHdoaXRlO1xuICB9XG5cbi5jb250YWluZXIyIHtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBoZWlnaHQ6IDUwJTtcbiAgICBtYXJnaW46IDAgYXV0byAwO1xuICAgIHBlcnNwZWN0aXZlOiAxMDAwO1xuICAgIGJhY2tmYWNlLXZpc2liaWxpdHk6IGhpZGRlbjtcbiAgICBiYWNrZ3JvdW5kOiBibGFjaztcbiAgICBib3JkZXI6MXB4IHNvbGlkIHdoaXRlO1xuICB9XG4gIFxuICAucHVsc2UtYnV0dG9uIHtcbiAgICBwb3NpdGlvbjphYnNvbHV0ZTsgLyppdCBjYW4gYmUgZml4ZWQgdG9vKi9cbiAgICAgICAgbGVmdDowOyByaWdodDowO1xuICAgICAgICB0b3A6MDsgYm90dG9tOjA7XG4gICAgICAgIG1hcmdpbjphdXRvO1xuICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7IFxuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgIHdpZHRoOiAxMzBweDtcbiAgICBoZWlnaHQ6IDEzMHB4O1xuICAgIGZvbnQtc2l6ZTogMS4zZW07XG4gICAgZm9udC13ZWlnaHQ6IGxpZ2h0O1xuICAgIGZvbnQtZmFtaWx5OiAnVHJlYnVjaGV0IE1TJywgc2Fucy1zZXJpZjtcbiAgICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICBsaW5lLWhlaWdodDogMTMwcHg7XG4gICAgbGV0dGVyLXNwYWNpbmc6IC0xcHg7XG4gICAgY29sb3I6IHdoaXRlO1xuICAgIGJvcmRlcjogbm9uZTtcbiAgICBib3JkZXItcmFkaXVzOiA1MCU7XG4gICAgYmFja2dyb3VuZDogIzIzMmIyYjtcbiAgICBjdXJzb3I6IHBvaW50ZXI7XG4gICAgYm94LXNoYWRvdzogMCAwIDAgMCByZ2JhKCM2MDYwNjAsIC41KTtcbiAgICBhbmltYXRpb246IHB1bHNlIDEuNXMgaW5maW5pdGU7XG4gIH1cbiAgLnB1bHNlLWJ1dHRvbjpob3ZlciB7XG4gICAgYW5pbWF0aW9uOiBub25lO1xuICB9XG4gIFxuICBAa2V5ZnJhbWVzIHB1bHNlIHtcbiAgICAwJSB7XG4gICAgIHRyYW5zZm9ybTogc2NhbGUoMC45KTtcbiAgICB9XG4gICAgNzAlIHtcbiAgICAgIHRyYW5zZm9ybTogc2NhbGUoMSk7XG4gICAgICBib3gtc2hhZG93OiAwIDAgMCA1MHB4IHJnYmEoOTAsIDE1MywgMjEyLCAwKTtcbiAgICB9XG4gICAgICAxMDAlIHtcbiAgICAgICB0cmFuc2Zvcm06IHNjYWxlKDAuOSk7XG4gICAgICBib3gtc2hhZG93OiAwIDAgMCAwIHJnYmEoOTAsIDE1MywgMjEyLCAwKTtcbiAgICB9XG4gIH1cblxuICAjcXJQdWxzZXtcbiAgICBmb250LXNpemU6MjMwJTtcbiAgICBwb3NpdGlvbjphYnNvbHV0ZTsgLyppdCBjYW4gYmUgZml4ZWQgdG9vKi9cbiAgICAgICAgbGVmdDowOyByaWdodDowO1xuICAgICAgICB0b3A6MDsgYm90dG9tOjA7XG4gICAgICAgIG1hcmdpbjphdXRvO1xuICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7IFxuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICB9XG5cbiAgI3NjYW5MYWJlbHtcbiAgICAgIGNvbG9yOndoaXRlO1xuICAgICAgdGV4dC1hbGlnbjpjZW50ZXI7XG4gICAgICBmb250LXNpemU6MTYwJTtcbiAgICAgIG1hcmdpbi1ib3R0b206MjAlO1xuICB9XG5cbiIsIiNob21lIHtcbiAgY29sb3I6IHdoaXRlO1xuICBmb250LXNpemU6IDE1MCU7XG4gIHBhZGRpbmctdG9wOiAxJTtcbn1cblxuLmJhY2tncm91bmQge1xuICAtLWJhY2tncm91bmQ6IGJsYWNrO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG5cbiNncmlkIHtcbiAgaGVpZ2h0OiAxMDAlO1xufVxuXG4jcm93MSB7XG4gIGhlaWdodDogMTAlO1xuICB3aWR0aDogMTAwJTtcbn1cblxuI3JvdzIge1xuICBoZWlnaHQ6IDQwJTtcbiAgd2lkdGg6IDEwMCU7XG59XG5cbiNyb3czIHtcbiAgaGVpZ2h0OiAyMCU7XG4gIHdpZHRoOiAxMDAlO1xuICBjb2xvcjogd2hpdGU7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgcGFkZGluZy10b3A6IDUlO1xufVxuXG4jYmFyQ29sIHtcbiAgaGVpZ2h0OiAxMDAlO1xufVxuXG4jcXJDb2wge1xuICBoZWlnaHQ6IDEwMCU7XG59XG5cbiNiYXJDYXJkIHtcbiAgaGVpZ2h0OiA5NSU7XG4gIGJhY2tncm91bmQ6ICMyMTIxMjE7XG59XG5cbiNxckNhcmQge1xuICBoZWlnaHQ6IDk1JTtcbiAgLS1iYWNrZ3JvdW5kOiB1cmwoLi4vLi4vLi4vYXNzZXRzL2ltYWdlcy9ibHVyLmpwZykgMCAwLzEwMCUgMTAwJSBuby1yZXBlYXQ7XG59XG5cbiNtYW51YWxDb2wge1xuICBjb2xvcjogd2hpdGU7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgcGFkZGluZy10b3A6IDUlO1xufVxuXG5pb24taW5wdXQge1xuICAtLWNvbG9yOiB3aGl0ZTtcbn1cblxuI3FySW1nQ29sIHtcbiAgaGVpZ2h0OiAxMDAlO1xuICBmb250LXNpemU6IDQwMCU7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xufVxuXG4jbGFiZWxDb2wge1xuICBoZWlnaHQ6IDEwMCU7XG4gIGZvbnQtc2l6ZTogNDAwJTtcbiAgZGlzcGxheTogZmxleDtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG59XG5cbiNsYWJlbCB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBjb2xvcjogd2hpdGU7XG4gIGZvbnQtc2l6ZTogNjUlO1xuICBwYWRkaW5nLWxlZnQ6IDEwJTtcbn1cblxuI3JvdyB7XG4gIGhlaWdodDogMTAwJTtcbn1cblxuI2dyaWQyIHtcbiAgaGVpZ2h0OiAxMDAlO1xufVxuXG4jc2NhblBpYyB7XG4gIHdpZHRoOiA0MCU7XG4gIG1hcmdpbi1ib3R0b206IDUlO1xufVxuXG4jc2NhbkJ0biB7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgaGVpZ2h0OiAxMjAlO1xuICB3aWR0aDogMTAwJTtcbiAgZm9udC1zaXplOiAxMjUlO1xuICBwYWRkaW5nLWxlZnQ6IDIlO1xuICBwYWRkaW5nLXJpZ2h0OiAyJTtcbn1cblxuI2NvbnRhaW5lciB7XG4gIHBhZGRpbmctdG9wOiAxMCU7XG4gIGhlaWdodDogMTAwJTtcbiAgd2lkdGg6IDEwMCU7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgLyppdCBjYW4gYmUgZml4ZWQgdG9vKi9cbiAgbGVmdDogMDtcbiAgcmlnaHQ6IDA7XG4gIHRvcDogMDtcbiAgYm90dG9tOiAwO1xuICBtYXJnaW46IGF1dG87XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xufVxuXG4jZ3JpZCB7XG4gIGhlaWdodDogMTAwJTtcbn1cblxuI21hbnVhbCB7XG4gIG1hcmdpbi10b3A6IDUlO1xuICBjb2xvcjogd2hpdGU7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxuI21hbnVhbElucHV0IHtcbiAgY29sb3I6IHdoaXRlO1xuICBtYXJnaW4tdG9wOiA1JTtcbiAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkIHdoaXRlO1xufVxuXG4jaW5wdXRSb3cge1xuICBwYWRkaW5nLWxlZnQ6IDEwJTtcbiAgcGFkZGluZy1yaWdodDogMTAlO1xufVxuXG4jb3Ige1xuICBmb250LXNpemU6IDI1MCU7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgY29sb3I6IHdoaXRlO1xufVxuXG4jb3JSb3cge1xuICBtYXJnaW4tdG9wOiAxMCU7XG4gIG1hcmdpbi1ib3R0b206IDEwJTtcbn1cblxuI21hbnVhbEJ0biB7XG4gIG1hcmdpbi10b3A6IDUlO1xufVxuXG4uY29udGFpbmVyMSB7XG4gIHdpZHRoOiAxMDAlO1xuICBoZWlnaHQ6IDIwJTtcbiAgbWFyZ2luOiAwIGF1dG8gMDtcbiAgcGVyc3BlY3RpdmU6IDEwMDA7XG4gIGJhY2tmYWNlLXZpc2liaWxpdHk6IGhpZGRlbjtcbiAgYmFja2dyb3VuZDogYmxhY2s7XG4gIGJvcmRlcjogMXB4IHNvbGlkIHdoaXRlO1xufVxuXG4uY29udGFpbmVyMiB7XG4gIHdpZHRoOiAxMDAlO1xuICBoZWlnaHQ6IDUwJTtcbiAgbWFyZ2luOiAwIGF1dG8gMDtcbiAgcGVyc3BlY3RpdmU6IDEwMDA7XG4gIGJhY2tmYWNlLXZpc2liaWxpdHk6IGhpZGRlbjtcbiAgYmFja2dyb3VuZDogYmxhY2s7XG4gIGJvcmRlcjogMXB4IHNvbGlkIHdoaXRlO1xufVxuXG4ucHVsc2UtYnV0dG9uIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICAvKml0IGNhbiBiZSBmaXhlZCB0b28qL1xuICBsZWZ0OiAwO1xuICByaWdodDogMDtcbiAgdG9wOiAwO1xuICBib3R0b206IDA7XG4gIG1hcmdpbjogYXV0bztcbiAgZGlzcGxheTogZmxleDtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIGRpc3BsYXk6IGJsb2NrO1xuICB3aWR0aDogMTMwcHg7XG4gIGhlaWdodDogMTMwcHg7XG4gIGZvbnQtc2l6ZTogMS4zZW07XG4gIGZvbnQtd2VpZ2h0OiBsaWdodDtcbiAgZm9udC1mYW1pbHk6IFwiVHJlYnVjaGV0IE1TXCIsIHNhbnMtc2VyaWY7XG4gIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgbGluZS1oZWlnaHQ6IDEzMHB4O1xuICBsZXR0ZXItc3BhY2luZzogLTFweDtcbiAgY29sb3I6IHdoaXRlO1xuICBib3JkZXI6IG5vbmU7XG4gIGJvcmRlci1yYWRpdXM6IDUwJTtcbiAgYmFja2dyb3VuZDogIzIzMmIyYjtcbiAgY3Vyc29yOiBwb2ludGVyO1xuICBib3gtc2hhZG93OiAwIDAgMCAwIHJnYmEoOTYsIDk2LCA5NiwgMC41KTtcbiAgYW5pbWF0aW9uOiBwdWxzZSAxLjVzIGluZmluaXRlO1xufVxuXG4ucHVsc2UtYnV0dG9uOmhvdmVyIHtcbiAgYW5pbWF0aW9uOiBub25lO1xufVxuXG5Aa2V5ZnJhbWVzIHB1bHNlIHtcbiAgMCUge1xuICAgIHRyYW5zZm9ybTogc2NhbGUoMC45KTtcbiAgfVxuICA3MCUge1xuICAgIHRyYW5zZm9ybTogc2NhbGUoMSk7XG4gICAgYm94LXNoYWRvdzogMCAwIDAgNTBweCByZ2JhKDkwLCAxNTMsIDIxMiwgMCk7XG4gIH1cbiAgMTAwJSB7XG4gICAgdHJhbnNmb3JtOiBzY2FsZSgwLjkpO1xuICAgIGJveC1zaGFkb3c6IDAgMCAwIDAgcmdiYSg5MCwgMTUzLCAyMTIsIDApO1xuICB9XG59XG4jcXJQdWxzZSB7XG4gIGZvbnQtc2l6ZTogMjMwJTtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICAvKml0IGNhbiBiZSBmaXhlZCB0b28qL1xuICBsZWZ0OiAwO1xuICByaWdodDogMDtcbiAgdG9wOiAwO1xuICBib3R0b206IDA7XG4gIG1hcmdpbjogYXV0bztcbiAgZGlzcGxheTogZmxleDtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG59XG5cbiNzY2FuTGFiZWwge1xuICBjb2xvcjogd2hpdGU7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgZm9udC1zaXplOiAxNjAlO1xuICBtYXJnaW4tYm90dG9tOiAyMCU7XG59Il19 */");

/***/ }),

/***/ "./src/app/pages/tab1/tab1.page.ts":
/*!*****************************************!*\
  !*** ./src/app/pages/tab1/tab1.page.ts ***!
  \*****************************************/
/*! exports provided: Tab1Page */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Tab1Page", function() { return Tab1Page; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _info_info_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../info/info.page */ "./src/app/pages/info/info.page.ts");
/* harmony import */ var _services_database_product_info_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/database/product-info.service */ "./src/app/services/database/product-info.service.ts");
/* harmony import */ var _ionic_native_barcode_scanner_ngx__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic-native/barcode-scanner/ngx */ "./node_modules/@ionic-native/barcode-scanner/ngx/index.js");
/* harmony import */ var _tab2_tab2_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../tab2/tab2.page */ "./src/app/pages/tab2/tab2.page.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");








let Tab1Page = class Tab1Page {
    constructor(router, 
    //private qrScanner: QRScanner,
    modalController, db, barcodeScanner, tab2, loadingCtrl, alertController) {
        //this.scanCode();
        this.router = router;
        this.modalController = modalController;
        this.db = db;
        this.barcodeScanner = barcodeScanner;
        this.tab2 = tab2;
        this.loadingCtrl = loadingCtrl;
        this.alertController = alertController;
        this.buttonDisabled = true;
    }
    scanCode() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const alert = yield this.alertController.create({
                header: 'Invalid Tag',
                message: 'The code you have scanned is not a valid tag.',
                buttons: ['OK']
            });
            let showAlert = false;
            this.barcodeScanner
                .scan()
                .then((barcodeData) => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
                let tag = barcodeData.text.toUpperCase();
                if (tag.length >= 1) {
                    let val = tag.substring(0, 2);
                    console.log("start" + val);
                    if (val.toUpperCase() === "IE" && tag.length <= 14 || val.toUpperCase() === "UK" && tag.length <= 14) {
                        this.tag = barcodeData.text;
                        this.saveScan();
                    }
                    else if (val === "37") {
                        let valid = this.check372Tag(tag);
                        if (valid == true) {
                            this.tag = barcodeData.text;
                            this.saveScan();
                        }
                        else {
                            yield alert.present();
                        }
                    }
                    else if (tag.length == 19) {
                        this.tag = barcodeData.text;
                        this.saveScan();
                    }
                    else if (tag.length <= 12) {
                        this.tag = 'IE' + barcodeData.text;
                        this.saveScan();
                    }
                    else {
                        yield alert.present();
                    }
                }
                else {
                    yield alert.present();
                }
            }))
                .catch(err => {
                console.log("Error", err);
            });
        });
    }
    home() {
        this.router.navigateByUrl("dashboard");
    }
    manualScan() {
        this.tag = this.manualTag.toUpperCase();
        if (this.tag.length <= 12) {
            this.tag = 'IE' + this.tag;
        }
        this.manualTag = '';
        this.saveScan();
    }
    saveScan() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            if (this.tag != '') {
                this.loader = yield this.loadingCtrl.create({
                    spinner: 'crescent',
                    // duration: 240000, // E Fitz 13-sep-2019 will spin for 4 minutes or until the task is finished
                    message: 'Retrieving info...'
                });
                // Show the spinner
                this.loader.present().then(() => { });
                console.log("saveTag" + this.tag);
                let date = this.formatDate();
                yield this.db.saveScan(this.tag, date);
                yield this.db.callProductInfo(this.tag);
                this.loader.dismiss();
                this.openInfoModal();
            }
            else {
            }
        });
    }
    formatDate() {
        var today = new Date();
        var dd = String(today.getDate()).padStart(2, '0');
        var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
        var yyyy = today.getFullYear();
        var date = mm + '/' + dd + '/' + yyyy;
        return date;
    }
    openInfoModal() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const modal = yield this.modalController.create({
                component: _info_info_page__WEBPACK_IMPORTED_MODULE_3__["InfoPage"],
                componentProps: {
                    tag: this.tag
                }
            });
            modal.present();
            const { data } = yield modal.onWillDismiss();
            console.log(data);
            if (data == 'save') {
                yield this.tab2.loadPurchases();
                this.router.navigateByUrl("tabs/tab2");
            }
        });
    }
    validateTag() {
        console.log("tag" + this.manualTag);
        console.log("tag length" + this.manualTag.length);
        if (this.manualTag.length >= 1) {
            let val = this.manualTag.substring(0, 2);
            console.log("start" + val);
            if (val.toUpperCase() === "IE" && this.manualTag.length <= 14) {
                this.buttonDisabled = false;
                if (val.toUpperCase() === "IE" && this.manualTag.length <= 14) {
                    this.buttonDisabled = false;
                }
            }
            else if (val.toUpperCase() === "UK" && this.manualTag.length <= 14) {
                this.buttonDisabled = false;
            }
            else if (val === "37") {
                let valid = this.check372Tag(this.manualTag);
                if (valid == true) {
                    this.buttonDisabled = false;
                }
                else
                    this.buttonDisabled = true;
            }
            else if (this.manualTag.length == 18) {
                this.buttonDisabled = false;
            }
            else if (this.manualTag.length <= 12) {
                this.buttonDisabled = false;
            }
            else {
                this.buttonDisabled = true;
            }
        }
    }
    isNumeric(n) {
        return !isNaN(parseFloat(n)) && isFinite(n);
    }
    check372Tag(intag) {
        /********************
        E Fitz
        12-03-2020
        
        Check to see if its a valid 372 tag.
      
       *********************/
        console.log('check372Tag intag:', intag);
        let lvTagtemp = intag.trim().toUpperCase();
        let lvTagCalc;
        let result = false;
        let checkdigit;
        let calcdigit;
        console.log('check372Tag lvTagtemp.substr(1, 3):', lvTagtemp.substr(0, 3));
        if (lvTagtemp.substr(0, 3) === '372') {
            if (lvTagtemp.length === 15) {
                console.log('check372Tag lvTag Before:', lvTagtemp);
                // need to remove the 372 and the check digit form the tag
                // PLSQL BUT array starts at 1 not 0 so 3 11 rather than 4 12 lvTag := SUBSTR(ivTag,4,7) ||SUBSTR(ivTag,12);
                lvTagCalc = ' ' + lvTagtemp.substr(3, 7) + lvTagtemp.substr(11, 5); // need to add the space as array in plsql starts at 1
                if (this.isNumeric(lvTagCalc)) {
                    // valid that this is a number
                    console.log('lvTag After:', lvTagCalc);
                    var lnNumToAdd = 0;
                    var lnChar = 0;
                    var lnNum = 0;
                    var lsNum = '';
                    var lnSummed = 0; //      lnSummed := 0;
                    var cal_it = 0;
                    var lnSub = 0;
                    for (var i = 1; i <= 11; i++) {
                        //FOR i IN lnStartPos..lnEndPos LOOP
                        let lvChar = lvTagCalc.substr(i, 1); //lvChar := SUBSTR(lvTag,i,1);
                        // console.log(i,' lvChar:', lvChar);
                        lnChar = +lvChar; // convert to number
                        // console.log(i,' lnChar:', lnChar);
                        cal_it = i + 1;
                        lnNumToAdd = ((cal_it % 2) + 1) * lnChar; //lnNumToAdd := (MOD(i+1,2) +1) * TO_NUMBER(lvChar);
                        lnNum = lnNum + lnNumToAdd; //lnNum := lnNum + lnNumToAdd;
                        //  console.log(i,' lnNum:', lnNum);
                    }
                    // console.log('lnNum:', lnNum);
                    lsNum = '' + lnNum; // convert to string
                    // console.log ('lsNum', lsNum);
                    //console.log ('lsNum.length', lsNum.length);
                    var countloops; // prevent a endless loop
                    while (lsNum.length > 1 || countloops > 500) {
                        //  WHILE LENGTH(TO_CHAR(lnNum)) >1 LOOP
                        lnSummed = 0;
                        countloops = countloops + 1;
                        for (var j = 0; j < lsNum.length; j++) {
                            //      FOR i IN 1..LENGTH(TO_CHAR(lnNum)) LOOP
                            lnSub = +lsNum.substr(j, 1);
                            lnSummed = lnSummed + lnSub; // lnSummed :=lnSummed + SUBSTR(TO_CHAR(lnNum),i,1);
                            //    console.log(j, ' lnSummed:', lnSummed);
                            countloops = countloops + 1;
                        }
                        lsNum = '' + lnSummed; //       lnNum := lnSummed;
                        //  console.log(j, ' lsNum:', lsNum);
                    }
                    // check to see if its valid
                    // this.resultdigit = lsNum;
                    checkdigit = lvTagtemp.substr(10, 1);
                    calcdigit = lsNum;
                    console.log('check372Tag lvTag checkdigit:', checkdigit, ' calcdigit: ', calcdigit);
                    if (checkdigit == calcdigit) {
                        result = true;
                    }
                    else {
                        //ERROR checksum failed
                        //todo display error message not numeric
                        console.log('check372Tag check sum FAILED');
                        result = false;
                    }
                } // end of if
                else {
                    //ERROR number is not numeric
                    //todo display error message not numeric
                    console.log('check372Tag not not numeric');
                    result = false;
                }
            }
            else {
                //ERROR not 15 characters long
                //todo display error message not numeric
                console.log('check372Tag not 15 characters');
                result = false;
            }
        }
        else {
            //ERROR doesn't start with 372
            //todo display error message not numeric
            console.log('check372Tag not starting with 372');
            result = true;
        }
        return result;
    }
};
Tab1Page.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["ModalController"] },
    { type: _services_database_product_info_service__WEBPACK_IMPORTED_MODULE_4__["ProductInfoService"] },
    { type: _ionic_native_barcode_scanner_ngx__WEBPACK_IMPORTED_MODULE_5__["BarcodeScanner"] },
    { type: _tab2_tab2_page__WEBPACK_IMPORTED_MODULE_6__["Tab2Page"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["LoadingController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["AlertController"] }
];
Tab1Page = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: "app-tab1",
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./tab1.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/tab1/tab1.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./tab1.page.scss */ "./src/app/pages/tab1/tab1.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["ModalController"],
        _services_database_product_info_service__WEBPACK_IMPORTED_MODULE_4__["ProductInfoService"],
        _ionic_native_barcode_scanner_ngx__WEBPACK_IMPORTED_MODULE_5__["BarcodeScanner"],
        _tab2_tab2_page__WEBPACK_IMPORTED_MODULE_6__["Tab2Page"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["LoadingController"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["AlertController"]])
], Tab1Page);



/***/ })

}]);
//# sourceMappingURL=tab1-tab1-module-es2015.js.map