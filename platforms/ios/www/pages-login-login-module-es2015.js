(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-login-login-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/login/login.page.html":
/*!***********************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/login/login.page.html ***!
  \***********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-content class=\"background\">\n  <!-- <h1 id=\"h1\" style='font-family:Raphtalia'>Welcome</h1> -->\n  <img id=\"welcome\" src=\"../../../assets/images/welcome.png\">\n  <div id=\"btnDiv\">\n    <ion-button (click)=\"onSignIn()\" id=\"loginBtn\" background=\"black\" shape=\"round\" expand=\"block\" color=\"white\" fill=\"outline\"><span style=\"text-transform:none!important;\">Login</span></ion-button>\n    <ion-button id=\"passwordBtn\" shape=\"round\" expand=\"block\" color=\"white\" fill=\"outline\"><span style=\"text-transform:none!important;\">Forgot Password?</span></ion-button>\n  </div>\n  \n</ion-content>\n");

/***/ }),

/***/ "./src/app/pages/login/login-routing.module.ts":
/*!*****************************************************!*\
  !*** ./src/app/pages/login/login-routing.module.ts ***!
  \*****************************************************/
/*! exports provided: LoginPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginPageRoutingModule", function() { return LoginPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _login_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./login.page */ "./src/app/pages/login/login.page.ts");




const routes = [
    {
        path: '',
        component: _login_page__WEBPACK_IMPORTED_MODULE_3__["LoginPage"]
    }
];
let LoginPageRoutingModule = class LoginPageRoutingModule {
};
LoginPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], LoginPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/login/login.module.ts":
/*!*********************************************!*\
  !*** ./src/app/pages/login/login.module.ts ***!
  \*********************************************/
/*! exports provided: LoginPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginPageModule", function() { return LoginPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _login_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./login-routing.module */ "./src/app/pages/login/login-routing.module.ts");
/* harmony import */ var _login_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./login.page */ "./src/app/pages/login/login.page.ts");







let LoginPageModule = class LoginPageModule {
};
LoginPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _login_routing_module__WEBPACK_IMPORTED_MODULE_5__["LoginPageRoutingModule"]
        ],
        declarations: [_login_page__WEBPACK_IMPORTED_MODULE_6__["LoginPage"]]
    })
], LoginPageModule);



/***/ }),

/***/ "./src/app/pages/login/login.page.scss":
/*!*********************************************!*\
  !*** ./src/app/pages/login/login.page.scss ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".background {\n  --background: url('image.jpg') 0 0/100% 100% no-repeat;\n  text-align: center;\n}\n\n#loginBtn {\n  color: white;\n  height: 35%;\n  width: 100%;\n  font-size: 125%;\n  padding-left: 2%;\n  padding-right: 2%;\n}\n\n#passwordBtn {\n  color: white;\n  height: 35%;\n  width: 100%;\n  font-size: 125%;\n  margin-top: 2%;\n  padding-left: 2%;\n  padding-right: 2%;\n}\n\n#btnDiv {\n  height: 20%;\n  margin: auto;\n  position: fixed;\n  width: 100%;\n  bottom: 0;\n  text-align: center;\n}\n\n#h1 {\n  color: white;\n  font-size: 300%;\n  text-align: center;\n}\n\n#welcome {\n  width: 65%;\n  padding-top: 5%;\n  padding-right: 3%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9saWFtY2hhbWJlcnMvRG9jdW1lbnRzL21lYXRlYXRpbmdxdWFsaXR5L3NyYy9hcHAvcGFnZXMvbG9naW4vbG9naW4ucGFnZS5zY3NzIiwic3JjL2FwcC9wYWdlcy9sb2dpbi9sb2dpbi5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxzREFBQTtFQUNBLGtCQUFBO0FDQ0o7O0FERUE7RUFDSSxZQUFBO0VBQ0EsV0FBQTtFQUNBLFdBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQSxpQkFBQTtBQ0NKOztBRENBO0VBQ0ksWUFBQTtFQUNBLFdBQUE7RUFDQSxXQUFBO0VBQ0EsZUFBQTtFQUNBLGNBQUE7RUFDQSxnQkFBQTtFQUNBLGlCQUFBO0FDRUo7O0FEQ0E7RUFDSSxXQUFBO0VBQ0EsWUFBQTtFQUNBLGVBQUE7RUFDQSxXQUFBO0VBQ0EsU0FBQTtFQUNBLGtCQUFBO0FDRUo7O0FEQ0E7RUFDSSxZQUFBO0VBQ0EsZUFBQTtFQUNBLGtCQUFBO0FDRUo7O0FEQ0E7RUFDSSxVQUFBO0VBQ0EsZUFBQTtFQUNBLGlCQUFBO0FDRUoiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9sb2dpbi9sb2dpbi5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuYmFja2dyb3VuZHtcbiAgICAtLWJhY2tncm91bmQ6IHVybCguLi8uLi8uLi9hc3NldHMvaW1hZ2VzL2ltYWdlLmpwZykgMCAwLzEwMCUgMTAwJSBuby1yZXBlYXQ7XG4gICAgdGV4dC1hbGlnbjpjZW50ZXI7XG59XG5cbiNsb2dpbkJ0bntcbiAgICBjb2xvcjp3aGl0ZTtcbiAgICBoZWlnaHQ6MzUlO1xuICAgIHdpZHRoOjEwMCU7XG4gICAgZm9udC1zaXplOjEyNSU7XG4gICAgcGFkZGluZy1sZWZ0OjIlO1xuICAgIHBhZGRpbmctcmlnaHQ6MiU7XG59XG4jcGFzc3dvcmRCdG57XG4gICAgY29sb3I6d2hpdGU7XG4gICAgaGVpZ2h0OjM1JTtcbiAgICB3aWR0aDoxMDAlO1xuICAgIGZvbnQtc2l6ZToxMjUlO1xuICAgIG1hcmdpbi10b3A6MiU7XG4gICAgcGFkZGluZy1sZWZ0OjIlO1xuICAgIHBhZGRpbmctcmlnaHQ6MiU7XG59XG5cbiNidG5EaXZ7XG4gICAgaGVpZ2h0OjIwJTtcbiAgICBtYXJnaW46IGF1dG87XG4gICAgcG9zaXRpb246IGZpeGVkO1xuICAgIHdpZHRoOjEwMCU7XG4gICAgYm90dG9tOiAwO1xuICAgIHRleHQtYWxpZ246Y2VudGVyO1xufVxuXG4jaDF7XG4gICAgY29sb3I6d2hpdGU7XG4gICAgZm9udC1zaXplOjMwMCU7XG4gICAgdGV4dC1hbGlnbjpjZW50ZXI7XG59XG5cbiN3ZWxjb21le1xuICAgIHdpZHRoOjY1JTsgXG4gICAgcGFkZGluZy10b3A6NSU7XG4gICAgcGFkZGluZy1yaWdodDozJTtcbn1cbiIsIi5iYWNrZ3JvdW5kIHtcbiAgLS1iYWNrZ3JvdW5kOiB1cmwoLi4vLi4vLi4vYXNzZXRzL2ltYWdlcy9pbWFnZS5qcGcpIDAgMC8xMDAlIDEwMCUgbm8tcmVwZWF0O1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG5cbiNsb2dpbkJ0biB7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgaGVpZ2h0OiAzNSU7XG4gIHdpZHRoOiAxMDAlO1xuICBmb250LXNpemU6IDEyNSU7XG4gIHBhZGRpbmctbGVmdDogMiU7XG4gIHBhZGRpbmctcmlnaHQ6IDIlO1xufVxuXG4jcGFzc3dvcmRCdG4ge1xuICBjb2xvcjogd2hpdGU7XG4gIGhlaWdodDogMzUlO1xuICB3aWR0aDogMTAwJTtcbiAgZm9udC1zaXplOiAxMjUlO1xuICBtYXJnaW4tdG9wOiAyJTtcbiAgcGFkZGluZy1sZWZ0OiAyJTtcbiAgcGFkZGluZy1yaWdodDogMiU7XG59XG5cbiNidG5EaXYge1xuICBoZWlnaHQ6IDIwJTtcbiAgbWFyZ2luOiBhdXRvO1xuICBwb3NpdGlvbjogZml4ZWQ7XG4gIHdpZHRoOiAxMDAlO1xuICBib3R0b206IDA7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxuI2gxIHtcbiAgY29sb3I6IHdoaXRlO1xuICBmb250LXNpemU6IDMwMCU7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxuI3dlbGNvbWUge1xuICB3aWR0aDogNjUlO1xuICBwYWRkaW5nLXRvcDogNSU7XG4gIHBhZGRpbmctcmlnaHQ6IDMlO1xufSJdfQ== */");

/***/ }),

/***/ "./src/app/pages/login/login.page.ts":
/*!*******************************************!*\
  !*** ./src/app/pages/login/login.page.ts ***!
  \*******************************************/
/*! exports provided: LoginPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginPage", function() { return LoginPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _auth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../auth.service */ "./src/app/auth.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _services_database_database_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../services/database/database.service */ "./src/app/services/database/database.service.ts");
/* harmony import */ var _services_database_history_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../services/database/history.service */ "./src/app/services/database/history.service.ts");
/* harmony import */ var _services_api_api_global_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../services/api/api-global.service */ "./src/app/services/api/api-global.service.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");









let LoginPage = class LoginPage {
    constructor(loadingCtrl, router, auth, db, history, apiglobal, httpclient) {
        this.loadingCtrl = loadingCtrl;
        this.router = router;
        this.auth = auth;
        this.db = db;
        this.history = history;
        this.apiglobal = apiglobal;
        this.httpclient = httpclient;
    }
    ngOnInit() {
    }
    onSignIn() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            this.loader = yield this.loadingCtrl.create({
                spinner: 'crescent',
                // duration: 240000, // E Fitz 13-sep-2019 will spin for 4 minutes or until the task is finished
                message: 'Signing in...'
            });
            // Show the spinner
            yield this.loader.present().then(() => { });
            yield this.auth
                .authenticate()
                .then((res) => {
                console.log("auth process complete");
                this.startProcedures();
            })
                .catch(error => console.error(error));
        });
    }
    getUrl() {
        let url = this.apiglobal.baseurl + "/meat-eating-quality/participant";
        console.log("ApiEventService: getUrl: " + url);
        return url;
    }
    checkUser() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            let promise = new Promise((resolve, reject) => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
                var token = yield this.apiglobal.getBearer();
                try {
                    let data;
                    let HttpOptions = {
                        headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_8__["HttpHeaders"]().set('Authorization', 'Bearer ' + token)
                    };
                    console.log('HttpOptions = ' + HttpOptions);
                    console.log('Bearer Token = ' + token);
                    let url = this.getUrl();
                    console.log("Validate user url: ", url);
                    //console.log('ApiBullService:getbulls bearer', this.apiglobal.bearerstring);
                    this.httpclient.get(url, HttpOptions).subscribe((response) => {
                        data = response;
                        if (response != null) {
                            console.log(response);
                            if (data._embedded.participant[0].in_project == 'Y') {
                                this.go();
                            }
                            else if (data._embedded.participant[0].in_project == 'N') {
                                this.blockUser();
                            }
                        }
                        if (response == null) {
                            console.log("Validate user: resolved response null ><");
                            resolve(data);
                        }
                    }, 
                    // To handle an error from the herd details api
                    (error) => {
                        this.apiglobal.errorlist.push("Validate user:" + error.message);
                        console.log(error.message);
                        reject(error);
                    });
                }
                catch (error) {
                    console.log("Validate user error:" + error.message);
                    // To handle an error from the herd details api
                    this.apiglobal.errorlist.push("Validate user:" + error.message);
                    reject(error);
                }
                finally {
                }
            }));
            return promise;
        });
    }
    startProcedures() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            this.checkUser();
            //this.blockUser();
        });
    }
    go() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            yield this.db.createDatabaseAndTable();
            yield this.history.callPurchaseHistory();
            this.loader.dismiss();
            this.router.navigateByUrl("dashboard");
        });
    }
    blockUser() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            this.loader.dismiss();
            this.router.navigateByUrl("validate");
        });
    }
};
LoginPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] },
    { type: _auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"] },
    { type: _services_database_database_service__WEBPACK_IMPORTED_MODULE_5__["DatabaseService"] },
    { type: _services_database_history_service__WEBPACK_IMPORTED_MODULE_6__["HistoryService"] },
    { type: _services_api_api_global_service__WEBPACK_IMPORTED_MODULE_7__["ApiglobalService"] },
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_8__["HttpClient"] }
];
LoginPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-login',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./login.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/login/login.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./login.page.scss */ "./src/app/pages/login/login.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"],
        _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"],
        _auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"],
        _services_database_database_service__WEBPACK_IMPORTED_MODULE_5__["DatabaseService"],
        _services_database_history_service__WEBPACK_IMPORTED_MODULE_6__["HistoryService"],
        _services_api_api_global_service__WEBPACK_IMPORTED_MODULE_7__["ApiglobalService"],
        _angular_common_http__WEBPACK_IMPORTED_MODULE_8__["HttpClient"]])
], LoginPage);



/***/ })

}]);
//# sourceMappingURL=pages-login-login-module-es2015.js.map