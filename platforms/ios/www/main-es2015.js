(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./$$_lazy_route_resource lazy recursive":
/*!******************************************************!*\
  !*** ./$$_lazy_route_resource lazy namespace object ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./pages/login/login.module": [
		"./src/app/pages/login/login.module.ts",
		"pages-login-login-module"
	],
	"./pages/tabs/tabs.module": [
		"./src/app/pages/tabs/tabs.module.ts",
		"pages-tabs-tabs-module"
	]
};
function webpackAsyncContext(req) {
	if(!__webpack_require__.o(map, req)) {
		return Promise.resolve().then(function() {
			var e = new Error("Cannot find module '" + req + "'");
			e.code = 'MODULE_NOT_FOUND';
			throw e;
		});
	}

	var ids = map[req], id = ids[0];
	return __webpack_require__.e(ids[1]).then(function() {
		return __webpack_require__(id);
	});
}
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = "./$$_lazy_route_resource lazy recursive";
module.exports = webpackAsyncContext;

/***/ }),

/***/ "./node_modules/@ionic/core/dist/esm lazy recursive ^\\.\\/.*\\.entry\\.js$ include: \\.entry\\.js$ exclude: \\.system\\.entry\\.js$":
/*!*****************************************************************************************************************************************!*\
  !*** ./node_modules/@ionic/core/dist/esm lazy ^\.\/.*\.entry\.js$ include: \.entry\.js$ exclude: \.system\.entry\.js$ namespace object ***!
  \*****************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./ion-action-sheet-controller_8.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-action-sheet-controller_8.entry.js",
		"common",
		0
	],
	"./ion-action-sheet-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-action-sheet-ios.entry.js",
		"common",
		1
	],
	"./ion-action-sheet-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-action-sheet-md.entry.js",
		"common",
		2
	],
	"./ion-alert-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-alert-ios.entry.js",
		"common",
		3
	],
	"./ion-alert-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-alert-md.entry.js",
		"common",
		4
	],
	"./ion-app_8-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-app_8-ios.entry.js",
		"common",
		5
	],
	"./ion-app_8-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-app_8-md.entry.js",
		"common",
		6
	],
	"./ion-avatar_3-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-avatar_3-ios.entry.js",
		"common",
		7
	],
	"./ion-avatar_3-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-avatar_3-md.entry.js",
		"common",
		8
	],
	"./ion-back-button-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-back-button-ios.entry.js",
		"common",
		9
	],
	"./ion-back-button-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-back-button-md.entry.js",
		"common",
		10
	],
	"./ion-backdrop-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-backdrop-ios.entry.js",
		11
	],
	"./ion-backdrop-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-backdrop-md.entry.js",
		12
	],
	"./ion-button_2-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-button_2-ios.entry.js",
		"common",
		13
	],
	"./ion-button_2-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-button_2-md.entry.js",
		"common",
		14
	],
	"./ion-card_5-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-card_5-ios.entry.js",
		"common",
		15
	],
	"./ion-card_5-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-card_5-md.entry.js",
		"common",
		16
	],
	"./ion-checkbox-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-checkbox-ios.entry.js",
		"common",
		17
	],
	"./ion-checkbox-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-checkbox-md.entry.js",
		"common",
		18
	],
	"./ion-chip-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-chip-ios.entry.js",
		"common",
		19
	],
	"./ion-chip-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-chip-md.entry.js",
		"common",
		20
	],
	"./ion-col_3.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-col_3.entry.js",
		21
	],
	"./ion-datetime_3-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-datetime_3-ios.entry.js",
		"common",
		22
	],
	"./ion-datetime_3-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-datetime_3-md.entry.js",
		"common",
		23
	],
	"./ion-fab_3-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-fab_3-ios.entry.js",
		"common",
		24
	],
	"./ion-fab_3-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-fab_3-md.entry.js",
		"common",
		25
	],
	"./ion-img.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-img.entry.js",
		26
	],
	"./ion-infinite-scroll_2-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-infinite-scroll_2-ios.entry.js",
		"common",
		27
	],
	"./ion-infinite-scroll_2-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-infinite-scroll_2-md.entry.js",
		"common",
		28
	],
	"./ion-input-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-input-ios.entry.js",
		"common",
		29
	],
	"./ion-input-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-input-md.entry.js",
		"common",
		30
	],
	"./ion-item-option_3-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-item-option_3-ios.entry.js",
		"common",
		31
	],
	"./ion-item-option_3-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-item-option_3-md.entry.js",
		"common",
		32
	],
	"./ion-item_8-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-item_8-ios.entry.js",
		"common",
		33
	],
	"./ion-item_8-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-item_8-md.entry.js",
		"common",
		34
	],
	"./ion-loading-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-loading-ios.entry.js",
		"common",
		35
	],
	"./ion-loading-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-loading-md.entry.js",
		"common",
		36
	],
	"./ion-menu_4-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-menu_4-ios.entry.js",
		"common",
		37
	],
	"./ion-menu_4-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-menu_4-md.entry.js",
		"common",
		38
	],
	"./ion-modal-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-modal-ios.entry.js",
		"common",
		39
	],
	"./ion-modal-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-modal-md.entry.js",
		"common",
		40
	],
	"./ion-nav_5.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-nav_5.entry.js",
		"common",
		41
	],
	"./ion-popover-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-popover-ios.entry.js",
		"common",
		42
	],
	"./ion-popover-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-popover-md.entry.js",
		"common",
		43
	],
	"./ion-progress-bar-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-progress-bar-ios.entry.js",
		"common",
		44
	],
	"./ion-progress-bar-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-progress-bar-md.entry.js",
		"common",
		45
	],
	"./ion-radio_2-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-radio_2-ios.entry.js",
		"common",
		46
	],
	"./ion-radio_2-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-radio_2-md.entry.js",
		"common",
		47
	],
	"./ion-range-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-range-ios.entry.js",
		"common",
		48
	],
	"./ion-range-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-range-md.entry.js",
		"common",
		49
	],
	"./ion-refresher_2-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-refresher_2-ios.entry.js",
		"common",
		50
	],
	"./ion-refresher_2-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-refresher_2-md.entry.js",
		"common",
		51
	],
	"./ion-reorder_2-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-reorder_2-ios.entry.js",
		"common",
		52
	],
	"./ion-reorder_2-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-reorder_2-md.entry.js",
		"common",
		53
	],
	"./ion-ripple-effect.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-ripple-effect.entry.js",
		54
	],
	"./ion-route_4.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-route_4.entry.js",
		"common",
		55
	],
	"./ion-searchbar-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-searchbar-ios.entry.js",
		"common",
		56
	],
	"./ion-searchbar-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-searchbar-md.entry.js",
		"common",
		57
	],
	"./ion-segment_2-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-segment_2-ios.entry.js",
		"common",
		58
	],
	"./ion-segment_2-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-segment_2-md.entry.js",
		"common",
		59
	],
	"./ion-select_3-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-select_3-ios.entry.js",
		"common",
		60
	],
	"./ion-select_3-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-select_3-md.entry.js",
		"common",
		61
	],
	"./ion-slide_2-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-slide_2-ios.entry.js",
		62
	],
	"./ion-slide_2-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-slide_2-md.entry.js",
		63
	],
	"./ion-spinner.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-spinner.entry.js",
		"common",
		64
	],
	"./ion-split-pane-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-split-pane-ios.entry.js",
		65
	],
	"./ion-split-pane-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-split-pane-md.entry.js",
		66
	],
	"./ion-tab-bar_2-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-tab-bar_2-ios.entry.js",
		"common",
		67
	],
	"./ion-tab-bar_2-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-tab-bar_2-md.entry.js",
		"common",
		68
	],
	"./ion-tab_2.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-tab_2.entry.js",
		"common",
		69
	],
	"./ion-text.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-text.entry.js",
		"common",
		70
	],
	"./ion-textarea-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-textarea-ios.entry.js",
		"common",
		71
	],
	"./ion-textarea-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-textarea-md.entry.js",
		"common",
		72
	],
	"./ion-toast-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-toast-ios.entry.js",
		"common",
		73
	],
	"./ion-toast-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-toast-md.entry.js",
		"common",
		74
	],
	"./ion-toggle-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-toggle-ios.entry.js",
		"common",
		75
	],
	"./ion-toggle-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-toggle-md.entry.js",
		"common",
		76
	],
	"./ion-virtual-scroll.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-virtual-scroll.entry.js",
		77
	]
};
function webpackAsyncContext(req) {
	if(!__webpack_require__.o(map, req)) {
		return Promise.resolve().then(function() {
			var e = new Error("Cannot find module '" + req + "'");
			e.code = 'MODULE_NOT_FOUND';
			throw e;
		});
	}

	var ids = map[req], id = ids[0];
	return Promise.all(ids.slice(1).map(__webpack_require__.e)).then(function() {
		return __webpack_require__(id);
	});
}
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = "./node_modules/@ionic/core/dist/esm lazy recursive ^\\.\\/.*\\.entry\\.js$ include: \\.entry\\.js$ exclude: \\.system\\.entry\\.js$";
module.exports = webpackAsyncContext;

/***/ }),

/***/ "./node_modules/moment/locale sync recursive ^\\.\\/.*$":
/*!**************************************************!*\
  !*** ./node_modules/moment/locale sync ^\.\/.*$ ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./af": "./node_modules/moment/locale/af.js",
	"./af.js": "./node_modules/moment/locale/af.js",
	"./ar": "./node_modules/moment/locale/ar.js",
	"./ar-dz": "./node_modules/moment/locale/ar-dz.js",
	"./ar-dz.js": "./node_modules/moment/locale/ar-dz.js",
	"./ar-kw": "./node_modules/moment/locale/ar-kw.js",
	"./ar-kw.js": "./node_modules/moment/locale/ar-kw.js",
	"./ar-ly": "./node_modules/moment/locale/ar-ly.js",
	"./ar-ly.js": "./node_modules/moment/locale/ar-ly.js",
	"./ar-ma": "./node_modules/moment/locale/ar-ma.js",
	"./ar-ma.js": "./node_modules/moment/locale/ar-ma.js",
	"./ar-sa": "./node_modules/moment/locale/ar-sa.js",
	"./ar-sa.js": "./node_modules/moment/locale/ar-sa.js",
	"./ar-tn": "./node_modules/moment/locale/ar-tn.js",
	"./ar-tn.js": "./node_modules/moment/locale/ar-tn.js",
	"./ar.js": "./node_modules/moment/locale/ar.js",
	"./az": "./node_modules/moment/locale/az.js",
	"./az.js": "./node_modules/moment/locale/az.js",
	"./be": "./node_modules/moment/locale/be.js",
	"./be.js": "./node_modules/moment/locale/be.js",
	"./bg": "./node_modules/moment/locale/bg.js",
	"./bg.js": "./node_modules/moment/locale/bg.js",
	"./bm": "./node_modules/moment/locale/bm.js",
	"./bm.js": "./node_modules/moment/locale/bm.js",
	"./bn": "./node_modules/moment/locale/bn.js",
	"./bn.js": "./node_modules/moment/locale/bn.js",
	"./bo": "./node_modules/moment/locale/bo.js",
	"./bo.js": "./node_modules/moment/locale/bo.js",
	"./br": "./node_modules/moment/locale/br.js",
	"./br.js": "./node_modules/moment/locale/br.js",
	"./bs": "./node_modules/moment/locale/bs.js",
	"./bs.js": "./node_modules/moment/locale/bs.js",
	"./ca": "./node_modules/moment/locale/ca.js",
	"./ca.js": "./node_modules/moment/locale/ca.js",
	"./cs": "./node_modules/moment/locale/cs.js",
	"./cs.js": "./node_modules/moment/locale/cs.js",
	"./cv": "./node_modules/moment/locale/cv.js",
	"./cv.js": "./node_modules/moment/locale/cv.js",
	"./cy": "./node_modules/moment/locale/cy.js",
	"./cy.js": "./node_modules/moment/locale/cy.js",
	"./da": "./node_modules/moment/locale/da.js",
	"./da.js": "./node_modules/moment/locale/da.js",
	"./de": "./node_modules/moment/locale/de.js",
	"./de-at": "./node_modules/moment/locale/de-at.js",
	"./de-at.js": "./node_modules/moment/locale/de-at.js",
	"./de-ch": "./node_modules/moment/locale/de-ch.js",
	"./de-ch.js": "./node_modules/moment/locale/de-ch.js",
	"./de.js": "./node_modules/moment/locale/de.js",
	"./dv": "./node_modules/moment/locale/dv.js",
	"./dv.js": "./node_modules/moment/locale/dv.js",
	"./el": "./node_modules/moment/locale/el.js",
	"./el.js": "./node_modules/moment/locale/el.js",
	"./en-SG": "./node_modules/moment/locale/en-SG.js",
	"./en-SG.js": "./node_modules/moment/locale/en-SG.js",
	"./en-au": "./node_modules/moment/locale/en-au.js",
	"./en-au.js": "./node_modules/moment/locale/en-au.js",
	"./en-ca": "./node_modules/moment/locale/en-ca.js",
	"./en-ca.js": "./node_modules/moment/locale/en-ca.js",
	"./en-gb": "./node_modules/moment/locale/en-gb.js",
	"./en-gb.js": "./node_modules/moment/locale/en-gb.js",
	"./en-ie": "./node_modules/moment/locale/en-ie.js",
	"./en-ie.js": "./node_modules/moment/locale/en-ie.js",
	"./en-il": "./node_modules/moment/locale/en-il.js",
	"./en-il.js": "./node_modules/moment/locale/en-il.js",
	"./en-nz": "./node_modules/moment/locale/en-nz.js",
	"./en-nz.js": "./node_modules/moment/locale/en-nz.js",
	"./eo": "./node_modules/moment/locale/eo.js",
	"./eo.js": "./node_modules/moment/locale/eo.js",
	"./es": "./node_modules/moment/locale/es.js",
	"./es-do": "./node_modules/moment/locale/es-do.js",
	"./es-do.js": "./node_modules/moment/locale/es-do.js",
	"./es-us": "./node_modules/moment/locale/es-us.js",
	"./es-us.js": "./node_modules/moment/locale/es-us.js",
	"./es.js": "./node_modules/moment/locale/es.js",
	"./et": "./node_modules/moment/locale/et.js",
	"./et.js": "./node_modules/moment/locale/et.js",
	"./eu": "./node_modules/moment/locale/eu.js",
	"./eu.js": "./node_modules/moment/locale/eu.js",
	"./fa": "./node_modules/moment/locale/fa.js",
	"./fa.js": "./node_modules/moment/locale/fa.js",
	"./fi": "./node_modules/moment/locale/fi.js",
	"./fi.js": "./node_modules/moment/locale/fi.js",
	"./fo": "./node_modules/moment/locale/fo.js",
	"./fo.js": "./node_modules/moment/locale/fo.js",
	"./fr": "./node_modules/moment/locale/fr.js",
	"./fr-ca": "./node_modules/moment/locale/fr-ca.js",
	"./fr-ca.js": "./node_modules/moment/locale/fr-ca.js",
	"./fr-ch": "./node_modules/moment/locale/fr-ch.js",
	"./fr-ch.js": "./node_modules/moment/locale/fr-ch.js",
	"./fr.js": "./node_modules/moment/locale/fr.js",
	"./fy": "./node_modules/moment/locale/fy.js",
	"./fy.js": "./node_modules/moment/locale/fy.js",
	"./ga": "./node_modules/moment/locale/ga.js",
	"./ga.js": "./node_modules/moment/locale/ga.js",
	"./gd": "./node_modules/moment/locale/gd.js",
	"./gd.js": "./node_modules/moment/locale/gd.js",
	"./gl": "./node_modules/moment/locale/gl.js",
	"./gl.js": "./node_modules/moment/locale/gl.js",
	"./gom-latn": "./node_modules/moment/locale/gom-latn.js",
	"./gom-latn.js": "./node_modules/moment/locale/gom-latn.js",
	"./gu": "./node_modules/moment/locale/gu.js",
	"./gu.js": "./node_modules/moment/locale/gu.js",
	"./he": "./node_modules/moment/locale/he.js",
	"./he.js": "./node_modules/moment/locale/he.js",
	"./hi": "./node_modules/moment/locale/hi.js",
	"./hi.js": "./node_modules/moment/locale/hi.js",
	"./hr": "./node_modules/moment/locale/hr.js",
	"./hr.js": "./node_modules/moment/locale/hr.js",
	"./hu": "./node_modules/moment/locale/hu.js",
	"./hu.js": "./node_modules/moment/locale/hu.js",
	"./hy-am": "./node_modules/moment/locale/hy-am.js",
	"./hy-am.js": "./node_modules/moment/locale/hy-am.js",
	"./id": "./node_modules/moment/locale/id.js",
	"./id.js": "./node_modules/moment/locale/id.js",
	"./is": "./node_modules/moment/locale/is.js",
	"./is.js": "./node_modules/moment/locale/is.js",
	"./it": "./node_modules/moment/locale/it.js",
	"./it-ch": "./node_modules/moment/locale/it-ch.js",
	"./it-ch.js": "./node_modules/moment/locale/it-ch.js",
	"./it.js": "./node_modules/moment/locale/it.js",
	"./ja": "./node_modules/moment/locale/ja.js",
	"./ja.js": "./node_modules/moment/locale/ja.js",
	"./jv": "./node_modules/moment/locale/jv.js",
	"./jv.js": "./node_modules/moment/locale/jv.js",
	"./ka": "./node_modules/moment/locale/ka.js",
	"./ka.js": "./node_modules/moment/locale/ka.js",
	"./kk": "./node_modules/moment/locale/kk.js",
	"./kk.js": "./node_modules/moment/locale/kk.js",
	"./km": "./node_modules/moment/locale/km.js",
	"./km.js": "./node_modules/moment/locale/km.js",
	"./kn": "./node_modules/moment/locale/kn.js",
	"./kn.js": "./node_modules/moment/locale/kn.js",
	"./ko": "./node_modules/moment/locale/ko.js",
	"./ko.js": "./node_modules/moment/locale/ko.js",
	"./ku": "./node_modules/moment/locale/ku.js",
	"./ku.js": "./node_modules/moment/locale/ku.js",
	"./ky": "./node_modules/moment/locale/ky.js",
	"./ky.js": "./node_modules/moment/locale/ky.js",
	"./lb": "./node_modules/moment/locale/lb.js",
	"./lb.js": "./node_modules/moment/locale/lb.js",
	"./lo": "./node_modules/moment/locale/lo.js",
	"./lo.js": "./node_modules/moment/locale/lo.js",
	"./lt": "./node_modules/moment/locale/lt.js",
	"./lt.js": "./node_modules/moment/locale/lt.js",
	"./lv": "./node_modules/moment/locale/lv.js",
	"./lv.js": "./node_modules/moment/locale/lv.js",
	"./me": "./node_modules/moment/locale/me.js",
	"./me.js": "./node_modules/moment/locale/me.js",
	"./mi": "./node_modules/moment/locale/mi.js",
	"./mi.js": "./node_modules/moment/locale/mi.js",
	"./mk": "./node_modules/moment/locale/mk.js",
	"./mk.js": "./node_modules/moment/locale/mk.js",
	"./ml": "./node_modules/moment/locale/ml.js",
	"./ml.js": "./node_modules/moment/locale/ml.js",
	"./mn": "./node_modules/moment/locale/mn.js",
	"./mn.js": "./node_modules/moment/locale/mn.js",
	"./mr": "./node_modules/moment/locale/mr.js",
	"./mr.js": "./node_modules/moment/locale/mr.js",
	"./ms": "./node_modules/moment/locale/ms.js",
	"./ms-my": "./node_modules/moment/locale/ms-my.js",
	"./ms-my.js": "./node_modules/moment/locale/ms-my.js",
	"./ms.js": "./node_modules/moment/locale/ms.js",
	"./mt": "./node_modules/moment/locale/mt.js",
	"./mt.js": "./node_modules/moment/locale/mt.js",
	"./my": "./node_modules/moment/locale/my.js",
	"./my.js": "./node_modules/moment/locale/my.js",
	"./nb": "./node_modules/moment/locale/nb.js",
	"./nb.js": "./node_modules/moment/locale/nb.js",
	"./ne": "./node_modules/moment/locale/ne.js",
	"./ne.js": "./node_modules/moment/locale/ne.js",
	"./nl": "./node_modules/moment/locale/nl.js",
	"./nl-be": "./node_modules/moment/locale/nl-be.js",
	"./nl-be.js": "./node_modules/moment/locale/nl-be.js",
	"./nl.js": "./node_modules/moment/locale/nl.js",
	"./nn": "./node_modules/moment/locale/nn.js",
	"./nn.js": "./node_modules/moment/locale/nn.js",
	"./pa-in": "./node_modules/moment/locale/pa-in.js",
	"./pa-in.js": "./node_modules/moment/locale/pa-in.js",
	"./pl": "./node_modules/moment/locale/pl.js",
	"./pl.js": "./node_modules/moment/locale/pl.js",
	"./pt": "./node_modules/moment/locale/pt.js",
	"./pt-br": "./node_modules/moment/locale/pt-br.js",
	"./pt-br.js": "./node_modules/moment/locale/pt-br.js",
	"./pt.js": "./node_modules/moment/locale/pt.js",
	"./ro": "./node_modules/moment/locale/ro.js",
	"./ro.js": "./node_modules/moment/locale/ro.js",
	"./ru": "./node_modules/moment/locale/ru.js",
	"./ru.js": "./node_modules/moment/locale/ru.js",
	"./sd": "./node_modules/moment/locale/sd.js",
	"./sd.js": "./node_modules/moment/locale/sd.js",
	"./se": "./node_modules/moment/locale/se.js",
	"./se.js": "./node_modules/moment/locale/se.js",
	"./si": "./node_modules/moment/locale/si.js",
	"./si.js": "./node_modules/moment/locale/si.js",
	"./sk": "./node_modules/moment/locale/sk.js",
	"./sk.js": "./node_modules/moment/locale/sk.js",
	"./sl": "./node_modules/moment/locale/sl.js",
	"./sl.js": "./node_modules/moment/locale/sl.js",
	"./sq": "./node_modules/moment/locale/sq.js",
	"./sq.js": "./node_modules/moment/locale/sq.js",
	"./sr": "./node_modules/moment/locale/sr.js",
	"./sr-cyrl": "./node_modules/moment/locale/sr-cyrl.js",
	"./sr-cyrl.js": "./node_modules/moment/locale/sr-cyrl.js",
	"./sr.js": "./node_modules/moment/locale/sr.js",
	"./ss": "./node_modules/moment/locale/ss.js",
	"./ss.js": "./node_modules/moment/locale/ss.js",
	"./sv": "./node_modules/moment/locale/sv.js",
	"./sv.js": "./node_modules/moment/locale/sv.js",
	"./sw": "./node_modules/moment/locale/sw.js",
	"./sw.js": "./node_modules/moment/locale/sw.js",
	"./ta": "./node_modules/moment/locale/ta.js",
	"./ta.js": "./node_modules/moment/locale/ta.js",
	"./te": "./node_modules/moment/locale/te.js",
	"./te.js": "./node_modules/moment/locale/te.js",
	"./tet": "./node_modules/moment/locale/tet.js",
	"./tet.js": "./node_modules/moment/locale/tet.js",
	"./tg": "./node_modules/moment/locale/tg.js",
	"./tg.js": "./node_modules/moment/locale/tg.js",
	"./th": "./node_modules/moment/locale/th.js",
	"./th.js": "./node_modules/moment/locale/th.js",
	"./tl-ph": "./node_modules/moment/locale/tl-ph.js",
	"./tl-ph.js": "./node_modules/moment/locale/tl-ph.js",
	"./tlh": "./node_modules/moment/locale/tlh.js",
	"./tlh.js": "./node_modules/moment/locale/tlh.js",
	"./tr": "./node_modules/moment/locale/tr.js",
	"./tr.js": "./node_modules/moment/locale/tr.js",
	"./tzl": "./node_modules/moment/locale/tzl.js",
	"./tzl.js": "./node_modules/moment/locale/tzl.js",
	"./tzm": "./node_modules/moment/locale/tzm.js",
	"./tzm-latn": "./node_modules/moment/locale/tzm-latn.js",
	"./tzm-latn.js": "./node_modules/moment/locale/tzm-latn.js",
	"./tzm.js": "./node_modules/moment/locale/tzm.js",
	"./ug-cn": "./node_modules/moment/locale/ug-cn.js",
	"./ug-cn.js": "./node_modules/moment/locale/ug-cn.js",
	"./uk": "./node_modules/moment/locale/uk.js",
	"./uk.js": "./node_modules/moment/locale/uk.js",
	"./ur": "./node_modules/moment/locale/ur.js",
	"./ur.js": "./node_modules/moment/locale/ur.js",
	"./uz": "./node_modules/moment/locale/uz.js",
	"./uz-latn": "./node_modules/moment/locale/uz-latn.js",
	"./uz-latn.js": "./node_modules/moment/locale/uz-latn.js",
	"./uz.js": "./node_modules/moment/locale/uz.js",
	"./vi": "./node_modules/moment/locale/vi.js",
	"./vi.js": "./node_modules/moment/locale/vi.js",
	"./x-pseudo": "./node_modules/moment/locale/x-pseudo.js",
	"./x-pseudo.js": "./node_modules/moment/locale/x-pseudo.js",
	"./yo": "./node_modules/moment/locale/yo.js",
	"./yo.js": "./node_modules/moment/locale/yo.js",
	"./zh-cn": "./node_modules/moment/locale/zh-cn.js",
	"./zh-cn.js": "./node_modules/moment/locale/zh-cn.js",
	"./zh-hk": "./node_modules/moment/locale/zh-hk.js",
	"./zh-hk.js": "./node_modules/moment/locale/zh-hk.js",
	"./zh-tw": "./node_modules/moment/locale/zh-tw.js",
	"./zh-tw.js": "./node_modules/moment/locale/zh-tw.js"
};


function webpackContext(req) {
	var id = webpackContextResolve(req);
	return __webpack_require__(id);
}
function webpackContextResolve(req) {
	if(!__webpack_require__.o(map, req)) {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	}
	return map[req];
}
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = "./node_modules/moment/locale sync recursive ^\\.\\/.*$";

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html":
/*!**************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html ***!
  \**************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-app>\n  <ion-router-outlet></ion-router-outlet>\n</ion-app>\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/info/info.page.html":
/*!*********************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/info/info.page.html ***!
  \*********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header [translucent]=\"true\">\n  <ion-toolbar>\n    <ion-grid>\n      <ion-row>\n        <ion-col id=\"left\" size=\"4\">\n          <ion-icon (click)=\"dismiss()\" src=\"assets/icon/left-arrow.svg\"></ion-icon>\n        </ion-col>\n        <ion-col id=\"right\" size=\"8\">\n          Product Information\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content class=\"background\">\n  <br />\n  <img  *ngIf=\"AA\" id=\"profilePic\" src=\"../../../assets/images/angus.png\" />\n  <img  *ngIf=\"HO\" id=\"profilePic\" src=\"../../../assets/images/hf.png\" />\n  <img  *ngIf=\"HF\" id=\"profilePic\" src=\"../../../assets/images/hf.png\" />\n  <img  *ngIf=\"CH\" id=\"profilePic\" src=\"../../../assets/images/ch.png\" />\n  <img  *ngIf=\"SI\" id=\"profilePic\" src=\"../../../assets/images/simental.png\" />\n  <img  *ngIf=\"HE\" id=\"profilePic\" src=\"../../../assets/images/hereford.png\" />\n  <img  *ngIf=\"BB\" id=\"profilePic\" src=\"../../../assets/images/bb.png\" />\n  <img  *ngIf=\"BA\" id=\"profilePic\" src=\"../../../assets/images/blonde.png\" />\n  <img  *ngIf=\"OTHER\" id=\"profilePic\" src=\"../../../assets/images/other.png\" />\n  <h1 id=\"name\">{{name}}</h1>\n  <h1 id=\"name\">{{animal_tag}}</h1>\n  <h1 id=\"details\">{{sex}}, {{origin}}</h1>\n  <!-- <ion-button\n    (click)=\"onSignIn()\"\n    id=\"historyBtn\"\n    shape=\"round\"\n    expand=\"block\"\n    color=\"tertiary\"\n    ><span style=\"text-transform: none !important;\"\n      >View History</span\n    ></ion-button -->\n  \n  <br />\n  <ion-card id=\"meatInfoCard\">\n    <!-- <ion-card-title><span id=\"title\">General info</span></ion-card-title>\n      <br> -->\n    <ion-item class=\"item\">\n      <span class=\"key\">Cut of meat</span\n      ><ion-label text-right class=\"data\">{{cut}}</ion-label>\n    </ion-item>\n    <ion-item>\n      <span class=\"key\">Farmer Name</span\n      ><ion-label text-right class=\"data\">{{farmer}}</ion-label>\n    </ion-item>\n    <ion-item>\n      <span class=\"key\">Breed</span\n      ><ion-label text-right class=\"data\">{{breed_name}}</ion-label>\n    </ion-item>\n    <ion-item>\n      <span class=\"key\">Dry Aged</span\n      ><ion-label text-right class=\"data\">{{dry}}</ion-label>\n    </ion-item>\n  </ion-card>\n  <ion-card id=\"meatQualityCard\">\n    <ion-label id=\"quality\">Meat Quality Rating</ion-label>\n    <ion-grid>\n      <ion-row id=\"rating\">\n        <ion-col *ngFor=\"let item of stars\">\n          <ion-icon src=\"assets/icon/star.svg\"></ion-icon>\n        </ion-col>\n        <ion-col *ngFor=\"let item of blank\">\n          <ion-icon src=\"assets/icon/starOutline.svg\"></ion-icon>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n  </ion-card>\n  <ion-card id=\"greenRatingCard\">\n    <div>\n      <ion-label  id=\"quality\">Green Rating</ion-label>\n    </div>\n    <br>\n    <div id=\"globeDiv\">\n      <circle-progress id=\"graphic\"\n      slot=\"secondary\"\n      [percent]= 'greenRating'\n      [subtitle]=\"'Progress'\"\n      [radius]=\"70\"\n      [maxPercent]=\"100\"\n      [outerStrokeWidth]=\"8\"\n      [innerStrokeWidth]=\"4\"\n      [outerStrokeColor]=\"'#78C000'\"\n      [innerStrokeColor]=\"'#C7E596'\"\n      [animation]=\"true\"\n      [animationDuration]=\"1500\"\n    >\n    </circle-progress> \n    </div>\n    <div id=\"percent\">\n      <label>{{greenRating}}%</label>\n    </div>\n  </ion-card>\n</ion-content>\n<ion-footer>\n  <ion-toolbar>\n    <ion-grid>\n      <ion-row>\n        <ion-col>\n          <ion-button color=\"success\" expand=\"block\" (click)=\"startPurchase()\">I will take it</ion-button>\n        </ion-col>\n        <ion-col>\n          <ion-button color=\"danger\" expand=\"block\" (click)=\"dismiss()\">I dont want it</ion-button>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n  </ion-toolbar>\n</ion-footer>\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/rate/rate.page.html":
/*!*********************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/rate/rate.page.html ***!
  \*********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header [translucent]=\"true\">\n  <ion-toolbar>\n    <ion-grid>\n      <ion-row>\n        <ion-col id=\"left\" size=\"4\">\n          <ion-icon\n            (click)=\"dismiss()\"\n            src=\"assets/icon/left-arrow.svg\"\n          ></ion-icon>\n        </ion-col>\n        <ion-col id=\"right\" size=\"8\">How was your steak?</ion-col>\n      </ion-row>\n    </ion-grid>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content class=\"background\">\n  <ion-grid id=\"grid\">\n    <ion-row>\n      <ion-col>\n        <label id=\"category\">Appearance</label>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col class=\"rating\">\n        <ion-rating [rate]=\"0\" size=\"large\" style=\"font-size:200%;\" (rateChange)=\"onRateChange5($event)\">\n        </ion-rating>\n      </ion-col>\n    </ion-row>\n    <br>\n    <ion-row>\n      <ion-col>\n        <label id=\"category\">Marbling</label>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col class=\"rating\">\n        <ion-rating [rate]=\"0\" size=\"large\" style=\"font-size:200%;\" (rateChange)=\"onRateChange4($event)\">\n        </ion-rating>\n      </ion-col>\n    </ion-row>\n    <br>\n    <ion-row>\n      <ion-col>\n        <label id=\"category\">Tenderness</label>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col class=\"rating\">\n        <ion-rating [rate]=\"0\" size=\"large\" style=\"font-size:200%;\" (rateChange)=\"onRateChange1($event)\">\n        </ion-rating>\n      </ion-col>\n    </ion-row>\n    <br>\n    <ion-row>\n      <ion-col>\n        <label id=\"category\">Juiciness</label>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col class=\"rating\">\n        <ion-rating [rate]=\"0\" size=\"large\" style=\"font-size:200%;\" (rateChange)=\"onRateChange2($event)\">\n        </ion-rating>\n      </ion-col>\n    </ion-row>\n    <br>\n    <ion-row>\n      <ion-col>\n        <label id=\"category\">Flavour</label>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col class=\"rating\">\n        <ion-rating [rate]=\"0\" size=\"large\" style=\"font-size:200%;\" (rateChange)=\"onRateChange3($event)\">\n        </ion-rating>\n      </ion-col>\n    </ion-row>\n    <br>\n    <ion-row>\n      <ion-col>\n        <label id=\"category\">Overall Satisfaction</label>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col class=\"rating\">\n        <ion-rating [rate]=\"0\" size=\"large\" style=\"font-size:200%;\" (rateChange)=\"onRateChange6($event)\">\n        </ion-rating>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col class=\"ratingDrop\">\n        <br>\n        <ion-item color=\"--ion-color-secondary-contrast\" style=\"border-bottom:1px solid white;\">\n          <ion-label style=\"background-color: black; color:white;\">How was you steak cooked?</ion-label>\n          <ion-select style=\"background-color: black; color:white;\" [(ngModel)]=\"doneness\" cancelText=\"Dismiss\">\n            <ion-select-option value=\"Rare\">Rare</ion-select-option>\n            <ion-select-option value=\"Medium Rare\">Medium-Rare</ion-select-option>\n            <ion-select-option value=\"Medium\">Medium</ion-select-option>\n            <ion-select-option value=\"Medium - Well Done\">Medium-Well Done</ion-select-option>\n            <ion-select-option value=\"Well Done\">Well Done</ion-select-option>\n          </ion-select>\n        </ion-item>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col class=\"ratingDrop\">\n        <ion-item color=\"--ion-color-secondary-contrast\" style=\"border-bottom:1px solid white;\">\n          <ion-label style=\"background-color: black; color:white;\">Cooking Method</ion-label>\n          <ion-select style=\"background-color: black; color:white;\"  [(ngModel)]=\"method\" cancelText=\"Dismiss\" required>\n            <ion-select-option value=\"Pan Fried\">Pan-Fried</ion-select-option>\n            <ion-select-option value=\"Grilled\">Grilled</ion-select-option>\n            <ion-select-option value=\"Oven\">Oven</ion-select-option>\n          </ion-select>\n        </ion-item>\n        <br>\n        \n      </ion-col>\n    </ion-row>\n  </ion-grid>\n  <br>\n  \n</ion-content>\n<ion-footer id=\"foot\">\n  <div id=\"rateCol\">\n    <ion-button id=\"rateBtn\" color=\"success\" expand=\"block\" (click)=\"submit()\">Rate!</ion-button>\n  </div>\n  <br>\n</ion-footer>\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/tab2/tab2.page.html":
/*!*********************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/tab2/tab2.page.html ***!
  \*********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header [translucent]=\"true\">\n  <ion-toolbar>\n    <ion-title>\n      <ion-icon (click)=\"home()\" id=\"home\" src=\"assets/icon/home.svg\" ></ion-icon>\n    </ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content [fullscreen]=\"true\" class=\"background\">\n  <br>\n  <ion-card *ngFor=\"let item of purchases; let i = index\" class=\"purchase\" id={{i}}>\n    <ion-grid id=\"grid\">\n      <ion-row id=\"animalRow\">\n        <ion-col id=\"imgCol\" size=\"4\">\n          <img  *ngIf=\"item.breed == 'AA'\" class=\"profilePic\" src=\"../../../assets/images/angus.png\" />\n          <img  *ngIf=\"item.breed == 'HO'\" class=\"profilePic\" src=\"../../../assets/images/hf.png\" />\n          <img  *ngIf=\"item.breed == 'HF'\" class=\"profilePic\" src=\"../../../assets/images/hf.png\" />\n          <img  *ngIf=\"item.breed == 'CH'\" class=\"profilePic\" src=\"../../../assets/images/ch.png\" />\n          <img  *ngIf=\"item.breed == 'SI'\" class=\"profilePic\" src=\"../../../assets/images/simental.png\" />\n          <img  *ngIf=\"item.breed == 'HE'\" class=\"profilePic\" src=\"../../../assets/images/hereford.png\" />\n          <img  *ngIf=\"item.breed == 'BB'\" class=\"profilePic\" src=\"../../../assets/images/bb.png\" />\n          <img  *ngIf=\"item.breed == 'BA'\" class=\"profilePic\" src=\"../../../assets/images/blonde.png\" />\n          <img  *ngIf=\"item.breed == 'LM'\" class=\"profilePic\" src=\"../../../assets/images/limousin.png\" />\n          <img  *ngIf=\"item.breed != 'LM' && item.breed != 'AA' && item.breed != 'HO' && item.breed != 'HF' && item.breed != 'CH' && item.breed != 'SI' && item.breed != 'HE' && item.breed != 'BB' && item.breed != 'BA'\" \n            class=\"profilePic\" src=\"../../../assets/images/other.png\" \n          />\n        </ion-col>\n        <ion-col id=\"infoCol\" size=\"8\">\n          <!-- <label class=\"name\">{{item.animal_name}}</label>\n          <br> -->\n          <ion-grid id=\"grid2\">\n            <ion-label id=\"tag\">{{item.animal_tag}}</ion-label>\n            <ion-row>\n              <ion-col>\n                <span class=\"left\" id=\"origin\">Origin</span>\n              </ion-col>\n              <ion-col>\n                <span class=\"right\">{{item.origin}}</span>\n              </ion-col>\n            </ion-row>\n            <ion-row>\n              <ion-col>\n                <span class=\"left\">Breed</span>\n              </ion-col>\n              <ion-col>\n                <span class=\"right\">{{item.breed_name}}</span>\n              </ion-col>\n            </ion-row>\n            <ion-row>\n              <ion-col>\n                <span class=\"left\">Purchase date</span>\n              </ion-col>\n              <ion-col>\n                <span class=\"right\">{{item.purchase_date}}</span>\n              </ion-col>\n            </ion-row>\n          </ion-grid>\n        </ion-col>\n      </ion-row>\n      <ion-row id=\"buttonRow\">\n        <ion-col *ngIf=\"item.overall_rating == null \">\n          <ion-button color=\"success\" expand=\"block\" (click)=\"rate(item)\">Rate this Steak</ion-button>\n        </ion-col>\n        <ion-col *ngIf=\"item.overall_rating == null \">\n          <ion-button color=\"danger\" expand=\"block\" (click)=\"dismiss(item, i)\">Dismiss</ion-button>\n        </ion-col>\n        <ion-col id=\"labelCol\" *ngIf=\"item.overall_rating != null \" size=\"5\">\n          <ion-label id=\"yourRating\">Your Rating:</ion-label>\n        </ion-col>\n        <ion-col id=\"starCol\" *ngIf=\"item.overall_rating != null \" size=\"7\">\n          <ion-rating readonly=\"readOnly\" [rate]=\"item.overall_rating\" size=\"medium\" style=\"font-size:200%;\"></ion-rating>\n        </ion-col>\n        <!-- <ion-col id=\"ratingCol\" *ngIf=\"item.overall_rating != null \" size=\"12\">\n          <ion-label id=\"yourRating\">Your Rating:</ion-label>\n          <ion-rating id=\"ionRating\" style=\"display:inline-block; padding-left:20%;\" disabled [rate]=\"item.overall_rating\" size=\"large\" style=\"font-size:200%;\" (rateChange)=\"onRateChange5($event)\"></ion-rating>\n        </ion-col> -->\n      </ion-row>\n    </ion-grid>\n  </ion-card>\n</ion-content>\n");

/***/ }),

/***/ "./node_modules/tslib/tslib.es6.js":
/*!*****************************************!*\
  !*** ./node_modules/tslib/tslib.es6.js ***!
  \*****************************************/
/*! exports provided: __extends, __assign, __rest, __decorate, __param, __metadata, __awaiter, __generator, __exportStar, __values, __read, __spread, __spreadArrays, __await, __asyncGenerator, __asyncDelegator, __asyncValues, __makeTemplateObject, __importStar, __importDefault, __classPrivateFieldGet, __classPrivateFieldSet */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__extends", function() { return __extends; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__assign", function() { return __assign; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__rest", function() { return __rest; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__decorate", function() { return __decorate; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__param", function() { return __param; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__metadata", function() { return __metadata; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__awaiter", function() { return __awaiter; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__generator", function() { return __generator; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__exportStar", function() { return __exportStar; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__values", function() { return __values; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__read", function() { return __read; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__spread", function() { return __spread; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__spreadArrays", function() { return __spreadArrays; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__await", function() { return __await; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__asyncGenerator", function() { return __asyncGenerator; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__asyncDelegator", function() { return __asyncDelegator; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__asyncValues", function() { return __asyncValues; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__makeTemplateObject", function() { return __makeTemplateObject; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__importStar", function() { return __importStar; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__importDefault", function() { return __importDefault; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__classPrivateFieldGet", function() { return __classPrivateFieldGet; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__classPrivateFieldSet", function() { return __classPrivateFieldSet; });
/*! *****************************************************************************
Copyright (c) Microsoft Corporation. All rights reserved.
Licensed under the Apache License, Version 2.0 (the "License"); you may not use
this file except in compliance with the License. You may obtain a copy of the
License at http://www.apache.org/licenses/LICENSE-2.0

THIS CODE IS PROVIDED ON AN *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
MERCHANTABLITY OR NON-INFRINGEMENT.

See the Apache Version 2.0 License for specific language governing permissions
and limitations under the License.
***************************************************************************** */
/* global Reflect, Promise */

var extendStatics = function(d, b) {
    extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return extendStatics(d, b);
};

function __extends(d, b) {
    extendStatics(d, b);
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
}

var __assign = function() {
    __assign = Object.assign || function __assign(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
        }
        return t;
    }
    return __assign.apply(this, arguments);
}

function __rest(s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
}

function __decorate(decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
}

function __param(paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
}

function __metadata(metadataKey, metadataValue) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(metadataKey, metadataValue);
}

function __awaiter(thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
}

function __generator(thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
}

function __exportStar(m, exports) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}

function __values(o) {
    var s = typeof Symbol === "function" && Symbol.iterator, m = s && o[s], i = 0;
    if (m) return m.call(o);
    if (o && typeof o.length === "number") return {
        next: function () {
            if (o && i >= o.length) o = void 0;
            return { value: o && o[i++], done: !o };
        }
    };
    throw new TypeError(s ? "Object is not iterable." : "Symbol.iterator is not defined.");
}

function __read(o, n) {
    var m = typeof Symbol === "function" && o[Symbol.iterator];
    if (!m) return o;
    var i = m.call(o), r, ar = [], e;
    try {
        while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
    }
    catch (error) { e = { error: error }; }
    finally {
        try {
            if (r && !r.done && (m = i["return"])) m.call(i);
        }
        finally { if (e) throw e.error; }
    }
    return ar;
}

function __spread() {
    for (var ar = [], i = 0; i < arguments.length; i++)
        ar = ar.concat(__read(arguments[i]));
    return ar;
}

function __spreadArrays() {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
};

function __await(v) {
    return this instanceof __await ? (this.v = v, this) : new __await(v);
}

function __asyncGenerator(thisArg, _arguments, generator) {
    if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
    var g = generator.apply(thisArg, _arguments || []), i, q = [];
    return i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i;
    function verb(n) { if (g[n]) i[n] = function (v) { return new Promise(function (a, b) { q.push([n, v, a, b]) > 1 || resume(n, v); }); }; }
    function resume(n, v) { try { step(g[n](v)); } catch (e) { settle(q[0][3], e); } }
    function step(r) { r.value instanceof __await ? Promise.resolve(r.value.v).then(fulfill, reject) : settle(q[0][2], r); }
    function fulfill(value) { resume("next", value); }
    function reject(value) { resume("throw", value); }
    function settle(f, v) { if (f(v), q.shift(), q.length) resume(q[0][0], q[0][1]); }
}

function __asyncDelegator(o) {
    var i, p;
    return i = {}, verb("next"), verb("throw", function (e) { throw e; }), verb("return"), i[Symbol.iterator] = function () { return this; }, i;
    function verb(n, f) { i[n] = o[n] ? function (v) { return (p = !p) ? { value: __await(o[n](v)), done: n === "return" } : f ? f(v) : v; } : f; }
}

function __asyncValues(o) {
    if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
    var m = o[Symbol.asyncIterator], i;
    return m ? m.call(o) : (o = typeof __values === "function" ? __values(o) : o[Symbol.iterator](), i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i);
    function verb(n) { i[n] = o[n] && function (v) { return new Promise(function (resolve, reject) { v = o[n](v), settle(resolve, reject, v.done, v.value); }); }; }
    function settle(resolve, reject, d, v) { Promise.resolve(v).then(function(v) { resolve({ value: v, done: d }); }, reject); }
}

function __makeTemplateObject(cooked, raw) {
    if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
    return cooked;
};

function __importStar(mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result.default = mod;
    return result;
}

function __importDefault(mod) {
    return (mod && mod.__esModule) ? mod : { default: mod };
}

function __classPrivateFieldGet(receiver, privateMap) {
    if (!privateMap.has(receiver)) {
        throw new TypeError("attempted to get private field on non-instance");
    }
    return privateMap.get(receiver);
}

function __classPrivateFieldSet(receiver, privateMap, value) {
    if (!privateMap.has(receiver)) {
        throw new TypeError("attempted to set private field on non-instance");
    }
    privateMap.set(receiver, value);
    return value;
}


/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");



const routes = [
    { path: '', loadChildren: './pages/login/login.module#LoginPageModule' },
    { path: 'login', loadChildren: './pages/login/login.module#LoginPageModule' },
    { path: 'tabs', loadChildren: './pages/tabs/tabs.module#TabsPageModule' },
    {
        path: 'dashboard',
        loadChildren: () => __webpack_require__.e(/*! import() | pages-dashboard-dashboard-module */ "pages-dashboard-dashboard-module").then(__webpack_require__.bind(null, /*! ./pages/dashboard/dashboard.module */ "./src/app/pages/dashboard/dashboard.module.ts")).then(m => m.DashboardPageModule)
    },
    { path: '', loadChildren: './pages/tabs/tabs.module#TabsPageModule' },
    {
        path: 'info',
        loadChildren: () => Promise.resolve(/*! import() */).then(__webpack_require__.bind(null, /*! ./pages/info/info.module */ "./src/app/pages/info/info.module.ts")).then(m => m.InfoPageModule)
    },
    {
        path: 'rate',
        loadChildren: () => Promise.resolve(/*! import() */).then(__webpack_require__.bind(null, /*! ./pages/rate/rate.module */ "./src/app/pages/rate/rate.module.ts")).then(m => m.RatePageModule)
    },
    {
        path: 'validate',
        loadChildren: () => __webpack_require__.e(/*! import() | pages-validate-validate-module */ "pages-validate-validate-module").then(__webpack_require__.bind(null, /*! ./pages/validate/validate.module */ "./src/app/pages/validate/validate.module.ts")).then(m => m.ValidatePageModule)
    },
];
let AppRoutingModule = class AppRoutingModule {
};
AppRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(routes, { preloadingStrategy: _angular_router__WEBPACK_IMPORTED_MODULE_2__["PreloadAllModules"] })
        ],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })
], AppRoutingModule);



/***/ }),

/***/ "./src/app/app.component.scss":
/*!************************************!*\
  !*** ./src/app/app.component.scss ***!
  \************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuc2NzcyJ9 */");

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _ionic_native_splash_screen_ngx__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic-native/splash-screen/ngx */ "./node_modules/@ionic-native/splash-screen/ngx/index.js");
/* harmony import */ var _ionic_native_status_bar_ngx__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic-native/status-bar/ngx */ "./node_modules/@ionic-native/status-bar/ngx/index.js");





let AppComponent = class AppComponent {
    constructor(platform, splashScreen, statusBar) {
        this.platform = platform;
        this.splashScreen = splashScreen;
        this.statusBar = statusBar;
        this.initializeApp();
    }
    initializeApp() {
        this.platform.ready().then(() => {
            this.statusBar.styleDefault();
            this.splashScreen.hide();
        });
    }
};
AppComponent.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["Platform"] },
    { type: _ionic_native_splash_screen_ngx__WEBPACK_IMPORTED_MODULE_3__["SplashScreen"] },
    { type: _ionic_native_status_bar_ngx__WEBPACK_IMPORTED_MODULE_4__["StatusBar"] }
];
AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-root',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./app.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./app.component.scss */ "./src/app/app.component.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["Platform"],
        _ionic_native_splash_screen_ngx__WEBPACK_IMPORTED_MODULE_3__["SplashScreen"],
        _ionic_native_status_bar_ngx__WEBPACK_IMPORTED_MODULE_4__["StatusBar"]])
], AppComponent);



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm2015/platform-browser.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _ionic_native_splash_screen_ngx__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic-native/splash-screen/ngx */ "./node_modules/@ionic-native/splash-screen/ngx/index.js");
/* harmony import */ var _ionic_native_status_bar_ngx__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic-native/status-bar/ngx */ "./node_modules/@ionic-native/status-bar/ngx/index.js");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _node_modules_angular_http__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../node_modules/@angular/http */ "./node_modules/@angular/http/fesm2015/http.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _ionic_native_secure_storage_ngx__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @ionic-native/secure-storage/ngx */ "./node_modules/@ionic-native/secure-storage/ngx/index.js");
/* harmony import */ var _ionic_native_native_storage_ngx__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @ionic-native/native-storage/ngx */ "./node_modules/@ionic-native/native-storage/ngx/index.js");
/* harmony import */ var _node_modules_ionic_native_sqlite_ngx__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ../../node_modules/@ionic-native/sqlite/ngx */ "./node_modules/@ionic-native/sqlite/ngx/index.js");
/* harmony import */ var _services_database_database_service__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./services/database/database.service */ "./src/app/services/database/database.service.ts");
/* harmony import */ var _pages_info_info_module__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./pages/info/info.module */ "./src/app/pages/info/info.module.ts");
/* harmony import */ var _pages_rate_rate_module__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./pages/rate/rate.module */ "./src/app/pages/rate/rate.module.ts");
/* harmony import */ var _ionic_native_network_ngx__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! @ionic-native/network/ngx */ "./node_modules/@ionic-native/network/ngx/index.js");
/* harmony import */ var _ionic_native_barcode_scanner_ngx__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! @ionic-native/barcode-scanner/ngx */ "./node_modules/@ionic-native/barcode-scanner/ngx/index.js");
/* harmony import */ var _pages_tab2_tab2_page__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./pages/tab2/tab2.page */ "./src/app/pages/tab2/tab2.page.ts");
/* harmony import */ var _ionic_native_app_version_ngx__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! @ionic-native/app-version/ngx */ "./node_modules/@ionic-native/app-version/ngx/index.js");





















let AppModule = class AppModule {
};
AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        declarations: [_app_component__WEBPACK_IMPORTED_MODULE_8__["AppComponent"]],
        entryComponents: [],
        imports: [
            _angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__["BrowserModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"].forRoot(),
            _app_routing_module__WEBPACK_IMPORTED_MODULE_7__["AppRoutingModule"],
            _angular_common_http__WEBPACK_IMPORTED_MODULE_10__["HttpClientModule"],
            _node_modules_angular_http__WEBPACK_IMPORTED_MODULE_9__["HttpModule"],
            _pages_info_info_module__WEBPACK_IMPORTED_MODULE_15__["InfoPageModule"],
            _pages_rate_rate_module__WEBPACK_IMPORTED_MODULE_16__["RatePageModule"]
        ],
        providers: [
            _ionic_native_status_bar_ngx__WEBPACK_IMPORTED_MODULE_6__["StatusBar"],
            _ionic_native_splash_screen_ngx__WEBPACK_IMPORTED_MODULE_5__["SplashScreen"],
            _ionic_native_secure_storage_ngx__WEBPACK_IMPORTED_MODULE_11__["SecureStorage"],
            _ionic_native_native_storage_ngx__WEBPACK_IMPORTED_MODULE_12__["NativeStorage"],
            _services_database_database_service__WEBPACK_IMPORTED_MODULE_14__["DatabaseService"],
            //QRScanner,
            _ionic_native_barcode_scanner_ngx__WEBPACK_IMPORTED_MODULE_18__["BarcodeScanner"],
            _node_modules_ionic_native_sqlite_ngx__WEBPACK_IMPORTED_MODULE_13__["SQLite"],
            _ionic_native_network_ngx__WEBPACK_IMPORTED_MODULE_17__["Network"],
            _pages_tab2_tab2_page__WEBPACK_IMPORTED_MODULE_19__["Tab2Page"],
            _ionic_native_app_version_ngx__WEBPACK_IMPORTED_MODULE_20__["AppVersion"],
            { provide: _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouteReuseStrategy"], useClass: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicRouteStrategy"] }
        ],
        bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_8__["AppComponent"]]
    })
], AppModule);



/***/ }),

/***/ "./src/app/auth.service.ts":
/*!*********************************!*\
  !*** ./src/app/auth.service.ts ***!
  \*********************************/
/*! exports provided: AuthService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthService", function() { return AuthService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _environments_environments__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./environments/environments */ "./src/app/environments/environments.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var capacitor_secure_storage_plugin__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! capacitor-secure-storage-plugin */ "./node_modules/capacitor-secure-storage-plugin/dist/esm/index.js");
/* harmony import */ var _capacitor_core__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @capacitor/core */ "./node_modules/@capacitor/core/dist/esm/index.js");





// Capacitor


const { SecureStoragePlugin } = _capacitor_core__WEBPACK_IMPORTED_MODULE_6__["Plugins"];
let AuthService = class AuthService {
    constructor(http) {
        this.http = http;
        this.authUrl = _environments_environments__WEBPACK_IMPORTED_MODULE_2__["environment"].authUrl;
        this.clientId = "meat_eating";
        this.secret = "meat";
        this.redirectUri = _environments_environments__WEBPACK_IMPORTED_MODULE_2__["environment"].redirectUri;
        this.browserActive = false;
        this.isConnecting = false;
        this.stagingConfig = {
            authUrl: _environments_environments__WEBPACK_IMPORTED_MODULE_2__["environment"].authUrl,
            clientId: "meat_eating",
            secret: "meat",
            redirectUri: _environments_environments__WEBPACK_IMPORTED_MODULE_2__["environment"].redirectUri
        };
        this.localConfig = {
            authUrl: this.authUrl,
            clientId: this.clientId,
            secret: this.secret,
            redirectUri: this.redirectUri
        };
        console.log("Welcome to the auth constructor");
    }
    test() {
        console.log(this.authUrl);
    }
    authenticate() {
        let that = this;
        return new Promise(function (resolve, reject) {
            that.getTokens()
                .then((token) => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
                console.log("Got token from storage", token);
                that.currentTokens = token;
                if (moment__WEBPACK_IMPORTED_MODULE_4__().isAfter(that.currentTokens.expiry_time)) {
                    console.log("Access Token Expired, use refresh token");
                    yield that.useRefreshToken(that.currentTokens.refresh_token)
                        .then(data => {
                        console.log("successfully used refresh token", data);
                        console.log("Ready to access webapp!");
                        that.currentTokens = data;
                        resolve(data);
                    })
                        .catch(errors => {
                        console.error("Error retrieving refresh token", errors);
                        if (errors.status != undefined && errors.status == 0 /* unknown */) {
                            console.log("Possible issue with signal, returning current tokens");
                            resolve(that.currentTokens);
                        }
                        else {
                            console.log("Refresh token rejected, begin icbfLogin process");
                            that.icbfLogin()
                                .then(success => console.log("After refresh token attempt, ready to access webapp", success))
                                .catch(error => console.error("Error on logging in ", error));
                        }
                    });
                }
                else {
                    console.log("Ready to access webapp!");
                    resolve(token);
                }
            }))
                .catch(errors => {
                console.error("No saved tokens available", errors);
                that.icbfLogin()
                    .then(data => {
                    console.log("Access token obtained after icbfLogin: ", data);
                    resolve(data);
                }).catch(error => console.error("Error on logging in ", error));
            });
        });
    }
    retCurrentAccessToken() {
        return new Promise((resolve, reject) => {
            console.log("retCurrentAccessToken");
            if (window.cordova) {
                this.getTokens()
                    .then((token) => {
                    this.currentTokens = token;
                    console.log("-----------------------");
                    console.log(this.currentTokens);
                    if (moment__WEBPACK_IMPORTED_MODULE_4__().isAfter(this.currentTokens.expiry_time)) {
                        console.log("Access Token Expired, use refresh token");
                        this.useRefreshToken(this.currentTokens.refresh_token)
                            .then(data => {
                            console.log("successfully used refresh token", data);
                            console.log("Ready to access webapp!");
                            this.currentTokens = data;
                            resolve(data);
                        }).catch(err => {
                            console.log("Refresh token use failed", err);
                            reject(err);
                        });
                    }
                    else {
                        console.log("Access Token Still Valid");
                        this.token = this.currentTokens;
                        resolve(this.token);
                    }
                });
            }
        });
    }
    icbfLogin() {
        let browserRef;
        let complete = false;
        let that = this;
        console.log("Starting log in process");
        return new Promise(function (resolve, reject) {
            if (window.cordova) {
                that.isConnecting = true;
                var options = "location=no,clearcache=yes,toolbar=yes,clearsessioncache=yes,hidden=yes,hardwareback=no,zoom=no";
                browserRef = window.cordova.InAppBrowser.open(that.authUrl + "/authorize?response_type=code&client_id=" + that.clientId + "&redirect_uri=" + that.redirectUri + "&state=1", "_blank", options);
                console.log('URL FOR AUTH ' + that.authUrl + "/authorize?response_type=code&client_id=" + that.clientId + "&redirect_uri=" + that.redirectUri + "&state=1");
                that.browserActive = true;
                var parsedResponse;
                browserRef.addEventListener('loadstop', function (e) {
                    if (browserRef != undefined) {
                        browserRef.show();
                        that.isConnecting = false;
                    }
                });
                browserRef.addEventListener("loadstart", (event) => {
                    if ((event.url).indexOf(that.redirectUri) === 0) {
                        browserRef.hide();
                        browserRef.removeEventListener("exit", (event) => { });
                        that.isConnecting = false;
                        that.browserActive = false;
                        parsedResponse = that.getAuthorizationCode(event.url);
                        console.log("Authorisation code", parsedResponse);
                        if (parsedResponse["code"] !== undefined && parsedResponse["code"] !== null) {
                            console.log("parsedResponse[code] !== undefined : " + parsedResponse["code"]);
                            that.getAccessToken(parsedResponse["code"])
                                .then((token) => {
                                complete = true;
                                browserRef.close();
                                console.log("Access token obtained", token.access_token);
                                that.currentTokens = token;
                                that.saveTokens(token);
                                resolve(token);
                            })
                                .catch(error => {
                                console.error("Access code request failed", error);
                                reject(error);
                            });
                        }
                        else if (parsedResponse["error"] !== undefined && parsedResponse["error"] !== null) {
                            console.log("parsedResponse[error] !== undefined  : " + parsedResponse["error_description"]);
                            parsedResponse["error_description"] = parsedResponse["error_description"].replace(/\+/g, " "); //replace the + with space
                            reject(parsedResponse["error_description"]);
                        }
                        else {
                            console.error("rejected");
                            reject("Problem authenticating with ICBF");
                        }
                    }
                });
                browserRef.addEventListener("exit", function (event) {
                    that.browserActive = false;
                    that.isConnecting = false;
                    if (complete) {
                        console.log("Exiting inAppBrowser");
                        resolve(that.currentTokens);
                    }
                    else {
                        reject("The ICBF sign in flow was cancelled");
                    }
                });
            }
            else {
                console.log("Cordova not available, so can't open window");
                reject("Cordova not available, so can't open window");
            }
        });
    }
    getAccessToken(authCode) {
        console.log("Get access token for authorization code " + authCode);
        let that = this;
        let msgData = {
            "grant_type": "authorization_code",
            "client_id": this.clientId,
            "client_secret": this.secret,
            "code": authCode,
            "redirect_uri": this.redirectUri
        };
        return new Promise(function (resolve, reject) {
            that.http.post(that.authUrl, msgData, {})
                .toPromise()
                .then((res) => {
                console.log('getAccessToken', res);
                resolve(res);
            }).catch(errors => {
                console.error("Unsuccessful authRequest (Access token) ", errors);
                reject(errors);
            });
        });
    }
    getAuthorizationCode(url) {
        var key, value;
        var responseParameters = ((url).split("?")[1]).split("&");
        var parsedResponse = {};
        for (var i = 0; i < responseParameters.length; i++) {
            key = responseParameters[i].split("=")[0];
            value = responseParameters[i].split("=")[1];
            parsedResponse[key] = value;
        }
        return parsedResponse;
    }
    authRequest(msgData) {
        return this.http.post(this.authUrl, msgData, {}).toPromise();
    }
    useRefreshToken(refreshToken) {
        let that = this;
        var msgData = {
            "grant_type": "refresh_token",
            "refresh_token": refreshToken,
            "client_id": this.clientId,
            "client_secret": this.secret
        };
        return new Promise(function (resolve, reject) {
            console.log("Attempt to use refresh token");
            that.authRequest(msgData)
                .then(data => {
                console.log("Successful authRequest, saving Tokens");
                that.saveTokens(data);
                resolve(data);
                console.log("Access token obtained");
            }).catch(errors => {
                console.error("Unsuccessful authRequest (Refresh Token)", errors);
                reject(errors);
            });
        });
    }
    getTokens() {
        let that = this;
        return new Promise(function (resolve, reject) {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
                if (window.cordova) {
                    yield SecureStoragePlugin.get({ key: 'auth' }).then(data => {
                        data = JSON.parse(data.value);
                        console.log("Received token from native storage", data);
                        resolve(data);
                    })
                        .catch((error) => reject(error));
                }
            });
        });
    }
    saveTokens(data) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            console.log("savTokens()");
            if (window.cordova) {
                let request = {
                    access_token: data.access_token,
                    expiry_time: moment__WEBPACK_IMPORTED_MODULE_4__().add(data.expires_in, 's').format(),
                    token_type: data.token_type,
                    scope: data.scope,
                    refresh_token: data.refresh_token
                };
                console.log("request", request);
                console.log("storage creating...");
                yield SecureStoragePlugin.set({
                    key: 'auth',
                    value: JSON.stringify(request)
                });
            }
        });
    }
    clearCredentials() {
        var that = this;
        this.clearCurrentToken();
        if (window.cordova) {
            SecureStoragePlugin.clear();
        }
        else {
            console.log("Cordova not available to clear storage");
        }
    }
    clearCurrentToken() {
        for (var property in this.currentTokens) {
            if (this.currentTokens.hasOwnProperty(property)) {
                delete this.currentTokens[property];
            }
        }
    }
};
AuthService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"] }
];
AuthService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"]])
], AuthService);



/***/ }),

/***/ "./src/app/environments/environments.ts":
/*!**********************************************!*\
  !*** ./src/app/environments/environments.ts ***!
  \**********************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
// export const environment = {
//     //prod
// api_url: 'https://api.icbf.com/'
// //dev
// //api_url: 'http://nc.api.dev64.icbf.com.dev64.icbf.com'
// }

const environment = {
    production: false,
    baseUrl: 'https://api.icbf.com',
    authUrl: 'https://api.icbf.com/oauth',
    redirectUri: "https://api.icbf.com/client/callback"
};


/***/ }),

/***/ "./src/app/pages/info/info-routing.module.ts":
/*!***************************************************!*\
  !*** ./src/app/pages/info/info-routing.module.ts ***!
  \***************************************************/
/*! exports provided: InfoPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InfoPageRoutingModule", function() { return InfoPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _info_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./info.page */ "./src/app/pages/info/info.page.ts");




const routes = [
    {
        path: '',
        component: _info_page__WEBPACK_IMPORTED_MODULE_3__["InfoPage"]
    }
];
let InfoPageRoutingModule = class InfoPageRoutingModule {
};
InfoPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], InfoPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/info/info.module.ts":
/*!*******************************************!*\
  !*** ./src/app/pages/info/info.module.ts ***!
  \*******************************************/
/*! exports provided: InfoPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InfoPageModule", function() { return InfoPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _info_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./info-routing.module */ "./src/app/pages/info/info-routing.module.ts");
/* harmony import */ var _info_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./info.page */ "./src/app/pages/info/info.page.ts");
/* harmony import */ var ng_circle_progress__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ng-circle-progress */ "./node_modules/ng-circle-progress/fesm2015/ng-circle-progress.js");








let InfoPageModule = class InfoPageModule {
};
InfoPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _info_routing_module__WEBPACK_IMPORTED_MODULE_5__["InfoPageRoutingModule"],
            ng_circle_progress__WEBPACK_IMPORTED_MODULE_7__["NgCircleProgressModule"].forRoot({
                // set defaults here
                radius: 70,
                outerStrokeWidth: 16,
                innerStrokeWidth: 8,
                outerStrokeColor: "#78C000",
                innerStrokeColor: "#C7E596",
                animationDuration: 300,
                imageSrc: 'assets/images/earth.png',
                imageWidth: 130,
                imageHeight: 130,
                showImage: true
            })
        ],
        declarations: [_info_page__WEBPACK_IMPORTED_MODULE_6__["InfoPage"]]
    })
], InfoPageModule);



/***/ }),

/***/ "./src/app/pages/info/info.page.scss":
/*!*******************************************!*\
  !*** ./src/app/pages/info/info.page.scss ***!
  \*******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("#home {\n  color: white;\n  font-size: 150%;\n  padding-top: 1%;\n}\n\n.background {\n  --background: black;\n  text-align: center;\n}\n\n#left {\n  display: flex;\n  padding-left: 3%;\n  color: white;\n  font-size: 180%;\n}\n\n#right {\n  padding-top: 2%;\n  display: flex;\n  align-items: center;\n  justify-content: center;\n  color: white;\n  font-size: 140%;\n  font-weight: 500;\n}\n\n#profilePic {\n  width: 35%;\n  padding-top: 2%;\n}\n\n#profileRow {\n  text-align: center;\n}\n\n#name {\n  color: white;\n  font-size: 110%;\n  text-align: center;\n}\n\n#details {\n  color: white;\n  font-size: 90%;\n  text-align: center;\n  margin-top: 0%;\n}\n\n#historyBtn {\n  width: 30%;\n  margin-left: auto;\n  margin-right: auto;\n}\n\n#meatInfoCard {\n  height: 33%;\n  background: #232b2b;\n  color: white;\n  padding-top: 2%;\n  padding-left: 5%;\n  padding-right: 5%;\n}\n\n#title {\n  color: white;\n  text-align: left;\n}\n\n.key {\n  font-size: 120%;\n  color: #939393;\n}\n\n.data {\n  padding-left: 3%;\n  font-size: 120%;\n  color: #d3d3d3;\n  text-align: right;\n}\n\nion-item {\n  text-align: center;\n  --ion-background-color:#232b2b;\n}\n\n#meatQualityCard {\n  height: 23%;\n  background: #232b2b;\n  color: #d3d3d3;\n  padding-top: 5%;\n  padding-left: 5%;\n  padding-right: 5%;\n}\n\n#quality {\n  font-size: 150%;\n}\n\n#rating {\n  padding-top: 5%;\n  font-size: 310%;\n  color: #9acd32;\n}\n\n#greenRatingCard {\n  height: 40%;\n  background: #232b2b;\n  color: #d3d3d3;\n  padding-top: 5%;\n  padding-left: 5%;\n  padding-right: 5%;\n}\n\n#greenLabel {\n  font-size: 150%;\n}\n\n#globeDiv {\n  float: left;\n  text-align: left;\n}\n\n#percent {\n  float: right;\n  font-size: 400%;\n  display: flex;\n  align-items: center;\n  justify-content: center;\n  width: 50%;\n  height: 70%;\n}\n\n#percentLabel {\n  float: right;\n  font-size: 300%;\n  margin: auto;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9saWFtY2hhbWJlcnMvRG9jdW1lbnRzL21lYXRlYXRpbmdxdWFsaXR5L3NyYy9hcHAvcGFnZXMvaW5mby9pbmZvLnBhZ2Uuc2NzcyIsInNyYy9hcHAvcGFnZXMvaW5mby9pbmZvLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLFlBQUE7RUFDQSxlQUFBO0VBQ0EsZUFBQTtBQ0NKOztBREVFO0VBRUUsbUJBQUE7RUFDQSxrQkFBQTtBQ0FKOztBRElFO0VBQ0UsYUFBQTtFQUNBLGdCQUFBO0VBQ0EsWUFBQTtFQUNBLGVBQUE7QUNESjs7QURJRTtFQUNFLGVBQUE7RUFDQSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSx1QkFBQTtFQUNBLFlBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7QUNESjs7QURJRTtFQUNFLFVBQUE7RUFDQSxlQUFBO0FDREo7O0FESUU7RUFDRSxrQkFBQTtBQ0RKOztBRElFO0VBQ0UsWUFBQTtFQUNBLGVBQUE7RUFDQSxrQkFBQTtBQ0RKOztBRElFO0VBQ0UsWUFBQTtFQUNBLGNBQUE7RUFDQSxrQkFBQTtFQUNBLGNBQUE7QUNESjs7QURJRTtFQUNFLFVBQUE7RUFDQSxpQkFBQTtFQUNBLGtCQUFBO0FDREo7O0FESUU7RUFDRSxXQUFBO0VBQ0EsbUJBQUE7RUFDQSxZQUFBO0VBQ0EsZUFBQTtFQUNBLGdCQUFBO0VBQ0EsaUJBQUE7QUNESjs7QURJRTtFQUNFLFlBQUE7RUFDQSxnQkFBQTtBQ0RKOztBRElFO0VBQ0ksZUFBQTtFQUNBLGNBQUE7QUNETjs7QURJRTtFQUNFLGdCQUFBO0VBQ0EsZUFBQTtFQUNBLGNBQUE7RUFDQSxpQkFBQTtBQ0RKOztBRElFO0VBQ0ksa0JBQUE7RUFDQSw4QkFBQTtBQ0ROOztBRElHO0VBQ0csV0FBQTtFQUNBLG1CQUFBO0VBQ0EsY0FBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtFQUNBLGlCQUFBO0FDRE47O0FESUU7RUFDSSxlQUFBO0FDRE47O0FESUU7RUFDSSxlQUFBO0VBQ0EsZUFBQTtFQUNBLGNBQUE7QUNETjs7QURJRTtFQUNFLFdBQUE7RUFDQSxtQkFBQTtFQUNBLGNBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQSxpQkFBQTtBQ0RKOztBRElFO0VBQ0UsZUFBQTtBQ0RKOztBRElBO0VBQ0UsV0FBQTtFQUNBLGdCQUFBO0FDREY7O0FESUE7RUFDRSxZQUFBO0VBQ0EsZUFBQTtFQUNBLGFBQUE7RUFDRSxtQkFBQTtFQUNBLHVCQUFBO0VBQ0EsVUFBQTtFQUNBLFdBQUE7QUNESjs7QURJQTtFQUNFLFlBQUE7RUFDQSxlQUFBO0VBQ0YsWUFBQTtBQ0RBIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvaW5mby9pbmZvLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIiNob21lIHtcbiAgICBjb2xvcjogd2hpdGU7XG4gICAgZm9udC1zaXplOiAxNTAlO1xuICAgIHBhZGRpbmctdG9wOiAxJTtcbiAgfVxuICBcbiAgLmJhY2tncm91bmQge1xuICAgIC8vLS1iYWNrZ3JvdW5kOiB1cmwoLi4vLi4vLi4vYXNzZXRzL2ltYWdlcy9pbWFnZS5qcGcpIDAgMC8xMDAlIDEwMCUgbm8tcmVwZWF0O1xuICAgIC0tYmFja2dyb3VuZDogYmxhY2s7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIC8vb3ZlcmZsb3cteTogc2Nyb2xsO1xuICB9XG4gIFxuICAjbGVmdCB7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBwYWRkaW5nLWxlZnQ6IDMlO1xuICAgIGNvbG9yOiB3aGl0ZTtcbiAgICBmb250LXNpemU6IDE4MCU7XG4gIH1cbiAgXG4gICNyaWdodCB7XG4gICAgcGFkZGluZy10b3A6IDIlO1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICBjb2xvcjogd2hpdGU7XG4gICAgZm9udC1zaXplOiAxNDAlO1xuICAgIGZvbnQtd2VpZ2h0OiA1MDA7XG4gIH1cbiAgXG4gICNwcm9maWxlUGljIHtcbiAgICB3aWR0aDogMzUlO1xuICAgIHBhZGRpbmctdG9wOiAyJTtcbiAgfVxuICBcbiAgI3Byb2ZpbGVSb3cge1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgfVxuICBcbiAgI25hbWUge1xuICAgIGNvbG9yOiB3aGl0ZTtcbiAgICBmb250LXNpemU6IDExMCU7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICB9XG4gIFxuICAjZGV0YWlscyB7XG4gICAgY29sb3I6IHdoaXRlO1xuICAgIGZvbnQtc2l6ZTogOTAlO1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICBtYXJnaW4tdG9wOiAwJTtcbiAgfVxuICBcbiAgI2hpc3RvcnlCdG4ge1xuICAgIHdpZHRoOiAzMCU7XG4gICAgbWFyZ2luLWxlZnQ6IGF1dG87XG4gICAgbWFyZ2luLXJpZ2h0OiBhdXRvO1xuICB9XG4gIFxuICAjbWVhdEluZm9DYXJkIHtcbiAgICBoZWlnaHQ6IDMzJTtcbiAgICBiYWNrZ3JvdW5kOiAjMjMyYjJiO1xuICAgIGNvbG9yOiB3aGl0ZTtcbiAgICBwYWRkaW5nLXRvcDogMiU7XG4gICAgcGFkZGluZy1sZWZ0OiA1JTtcbiAgICBwYWRkaW5nLXJpZ2h0OiA1JTtcbiAgfVxuICBcbiAgI3RpdGxlIHtcbiAgICBjb2xvcjogd2hpdGU7XG4gICAgdGV4dC1hbGlnbjogbGVmdDtcbiAgfVxuICBcbiAgLmtleSB7XG4gICAgICBmb250LXNpemU6MTIwJTtcbiAgICAgIGNvbG9yOiM5MzkzOTM7XG4gIH1cbiAgXG4gIC5kYXRhIHtcbiAgICBwYWRkaW5nLWxlZnQ6IDMlO1xuICAgIGZvbnQtc2l6ZToxMjAlO1xuICAgIGNvbG9yOiNkM2QzZDM7XG4gICAgdGV4dC1hbGlnbjpyaWdodDtcbiAgfVxuICBcbiAgaW9uLWl0ZW17XG4gICAgICB0ZXh0LWFsaWduOmNlbnRlcjtcbiAgICAgIC0taW9uLWJhY2tncm91bmQtY29sb3I6IzIzMmIyYjtcbiAgIH1cbiAgXG4gICAjbWVhdFF1YWxpdHlDYXJkIHtcbiAgICAgIGhlaWdodDogMjMlO1xuICAgICAgYmFja2dyb3VuZDogIzIzMmIyYjtcbiAgICAgIGNvbG9yOiNkM2QzZDM7XG4gICAgICBwYWRkaW5nLXRvcDogNSU7XG4gICAgICBwYWRkaW5nLWxlZnQ6IDUlO1xuICAgICAgcGFkZGluZy1yaWdodDogNSU7XG4gICAgfVxuICBcbiAgI3F1YWxpdHl7XG4gICAgICBmb250LXNpemU6MTUwJTtcbiAgfVxuICBcbiAgI3JhdGluZ3tcbiAgICAgIHBhZGRpbmctdG9wOjUlO1xuICAgICAgZm9udC1zaXplOjMxMCU7XG4gICAgICBjb2xvcjogIzlhY2QzMjtcbiAgfVxuXG4gICNncmVlblJhdGluZ0NhcmQge1xuICAgIGhlaWdodDogNDAlO1xuICAgIGJhY2tncm91bmQ6ICMyMzJiMmI7XG4gICAgY29sb3I6I2QzZDNkMztcbiAgICBwYWRkaW5nLXRvcDogNSU7XG4gICAgcGFkZGluZy1sZWZ0OiA1JTtcbiAgICBwYWRkaW5nLXJpZ2h0OiA1JTtcbiAgfVxuXG4gICNncmVlbkxhYmVse1xuICAgIGZvbnQtc2l6ZToxNTAlO1xufVxuXG4jZ2xvYmVEaXZ7XG4gIGZsb2F0OmxlZnQ7XG4gIHRleHQtYWxpZ246bGVmdDtcbn1cblxuI3BlcmNlbnR7XG4gIGZsb2F0OnJpZ2h0O1xuICBmb250LXNpemU6NDAwJTtcbiAgZGlzcGxheTogZmxleDtcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgIHdpZHRoOjUwJTtcbiAgICBoZWlnaHQ6NzAlO1xufVxuXG4jcGVyY2VudExhYmVse1xuICBmbG9hdDpyaWdodDtcbiAgZm9udC1zaXplOjMwMCU7XG5tYXJnaW46YXV0bztcbn1cbiIsIiNob21lIHtcbiAgY29sb3I6IHdoaXRlO1xuICBmb250LXNpemU6IDE1MCU7XG4gIHBhZGRpbmctdG9wOiAxJTtcbn1cblxuLmJhY2tncm91bmQge1xuICAtLWJhY2tncm91bmQ6IGJsYWNrO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG5cbiNsZWZ0IHtcbiAgZGlzcGxheTogZmxleDtcbiAgcGFkZGluZy1sZWZ0OiAzJTtcbiAgY29sb3I6IHdoaXRlO1xuICBmb250LXNpemU6IDE4MCU7XG59XG5cbiNyaWdodCB7XG4gIHBhZGRpbmctdG9wOiAyJTtcbiAgZGlzcGxheTogZmxleDtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgZm9udC1zaXplOiAxNDAlO1xuICBmb250LXdlaWdodDogNTAwO1xufVxuXG4jcHJvZmlsZVBpYyB7XG4gIHdpZHRoOiAzNSU7XG4gIHBhZGRpbmctdG9wOiAyJTtcbn1cblxuI3Byb2ZpbGVSb3cge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG5cbiNuYW1lIHtcbiAgY29sb3I6IHdoaXRlO1xuICBmb250LXNpemU6IDExMCU7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxuI2RldGFpbHMge1xuICBjb2xvcjogd2hpdGU7XG4gIGZvbnQtc2l6ZTogOTAlO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIG1hcmdpbi10b3A6IDAlO1xufVxuXG4jaGlzdG9yeUJ0biB7XG4gIHdpZHRoOiAzMCU7XG4gIG1hcmdpbi1sZWZ0OiBhdXRvO1xuICBtYXJnaW4tcmlnaHQ6IGF1dG87XG59XG5cbiNtZWF0SW5mb0NhcmQge1xuICBoZWlnaHQ6IDMzJTtcbiAgYmFja2dyb3VuZDogIzIzMmIyYjtcbiAgY29sb3I6IHdoaXRlO1xuICBwYWRkaW5nLXRvcDogMiU7XG4gIHBhZGRpbmctbGVmdDogNSU7XG4gIHBhZGRpbmctcmlnaHQ6IDUlO1xufVxuXG4jdGl0bGUge1xuICBjb2xvcjogd2hpdGU7XG4gIHRleHQtYWxpZ246IGxlZnQ7XG59XG5cbi5rZXkge1xuICBmb250LXNpemU6IDEyMCU7XG4gIGNvbG9yOiAjOTM5MzkzO1xufVxuXG4uZGF0YSB7XG4gIHBhZGRpbmctbGVmdDogMyU7XG4gIGZvbnQtc2l6ZTogMTIwJTtcbiAgY29sb3I6ICNkM2QzZDM7XG4gIHRleHQtYWxpZ246IHJpZ2h0O1xufVxuXG5pb24taXRlbSB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgLS1pb24tYmFja2dyb3VuZC1jb2xvcjojMjMyYjJiO1xufVxuXG4jbWVhdFF1YWxpdHlDYXJkIHtcbiAgaGVpZ2h0OiAyMyU7XG4gIGJhY2tncm91bmQ6ICMyMzJiMmI7XG4gIGNvbG9yOiAjZDNkM2QzO1xuICBwYWRkaW5nLXRvcDogNSU7XG4gIHBhZGRpbmctbGVmdDogNSU7XG4gIHBhZGRpbmctcmlnaHQ6IDUlO1xufVxuXG4jcXVhbGl0eSB7XG4gIGZvbnQtc2l6ZTogMTUwJTtcbn1cblxuI3JhdGluZyB7XG4gIHBhZGRpbmctdG9wOiA1JTtcbiAgZm9udC1zaXplOiAzMTAlO1xuICBjb2xvcjogIzlhY2QzMjtcbn1cblxuI2dyZWVuUmF0aW5nQ2FyZCB7XG4gIGhlaWdodDogNDAlO1xuICBiYWNrZ3JvdW5kOiAjMjMyYjJiO1xuICBjb2xvcjogI2QzZDNkMztcbiAgcGFkZGluZy10b3A6IDUlO1xuICBwYWRkaW5nLWxlZnQ6IDUlO1xuICBwYWRkaW5nLXJpZ2h0OiA1JTtcbn1cblxuI2dyZWVuTGFiZWwge1xuICBmb250LXNpemU6IDE1MCU7XG59XG5cbiNnbG9iZURpdiB7XG4gIGZsb2F0OiBsZWZ0O1xuICB0ZXh0LWFsaWduOiBsZWZ0O1xufVxuXG4jcGVyY2VudCB7XG4gIGZsb2F0OiByaWdodDtcbiAgZm9udC1zaXplOiA0MDAlO1xuICBkaXNwbGF5OiBmbGV4O1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgd2lkdGg6IDUwJTtcbiAgaGVpZ2h0OiA3MCU7XG59XG5cbiNwZXJjZW50TGFiZWwge1xuICBmbG9hdDogcmlnaHQ7XG4gIGZvbnQtc2l6ZTogMzAwJTtcbiAgbWFyZ2luOiBhdXRvO1xufSJdfQ== */");

/***/ }),

/***/ "./src/app/pages/info/info.page.ts":
/*!*****************************************!*\
  !*** ./src/app/pages/info/info.page.ts ***!
  \*****************************************/
/*! exports provided: InfoPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InfoPage", function() { return InfoPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _services_database_product_info_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/database/product-info.service */ "./src/app/services/database/product-info.service.ts");






let InfoPage = class InfoPage {
    constructor(router, modalController, db, alertController) {
        this.router = router;
        this.modalController = modalController;
        this.db = db;
        this.alertController = alertController;
        this.stars = [];
        this.blank = [];
        this.loadInfo();
    }
    ngOnInit() { }
    dismiss() {
        this.modalController.dismiss();
    }
    dismissSave() {
        this.modalController.dismiss("save");
    }
    dismissInvalid() {
        console.log("invalid dismiss function");
        this.modalController.dismiss("invalid");
    }
    loadInfo() {
        let populated;
        console.log("loading herd");
        this.db.loadInfo().then((info) => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            if (info[0] == undefined) {
                populated = "N";
            }
            else {
                console.log("res" + info[0].meat_quality_rating);
                this.name = info[0].animal_name;
                this.sex = info[0].sex;
                this.origin = info[0].origin;
                this.cut = info[0].cut_of_meat;
                this.breed = info[0].breed;
                this.meatRating = info[0].meat_quality_rating;
                this.greenRating = info[0].green_rating;
                this.description = info[0].animal_breed_description;
                this.dry = info[0].dry_aged;
                this.farmer = info[0].farmer_name;
                this.animal_tag = info[0].animal_tag;
                this.breed_name = info[0].breed_name;
                if (this.sex == "HO") {
                    this.HO = true;
                }
                else if (this.sex == "SI") {
                    this.SI = true;
                }
                else if (this.sex == "CH") {
                    this.CH = true;
                }
                else if (this.sex == "LM") {
                    this.CH = true;
                }
                else if (this.sex == "BB") {
                    this.BB = true;
                }
                else if (this.sex == "HF") {
                    this.HF = true;
                }
                else if (this.sex == "HE") {
                    this.HE = true;
                }
                else if (this.sex == "BA") {
                    this.BA = true;
                }
                else if (this.sex == "AA") {
                    this.AA = true;
                }
                else {
                    this.OTHER = true;
                }
                console.log("meat rating" + this.meatRating);
                for (let i = 0; i < this.meatRating; i++) {
                    console.log("hello");
                    this.stars.push(0);
                }
                console.log(this.stars);
                let blanks = 5 - this.stars.length;
                console.log(blanks);
                for (let i = 0; i < blanks; i++) {
                    this.blank.push(0);
                }
            }
            if (populated == 'N') {
                console.log("populated = N");
                // this.dismissInvalid();
                const alert = yield this.alertController.create({
                    header: 'Invalid Tag',
                    message: 'Plase scan or enter another tag.',
                    buttons: [
                        {
                            text: 'Ok',
                            role: 'Ok',
                            handler: (ok) => {
                                this.modalController.dismiss("invalid");
                            }
                        }
                    ]
                });
                alert.present();
            }
        }));
    }
    startPurchase() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            // const alert = await this.alertController.create({
            //   header: 'Where did you purchase this steak?',
            //   cssClass: 'alertcss',
            //   inputs: [
            //     {
            //       type: 'checkbox',
            //       label: 'ICBF',
            //       value: 'ICBF',
            //     },
            //     {
            //       type: 'checkbox',
            //       label: 'Tully',
            //       value: 'Tully'
            //     }
            //   ],
            //   buttons: [
            //     {
            //       text: 'Continue',
            //       handler: data => {
            //       this.purchase(data);
            //     }
            //     }
            //   ]
            // });
            //alert.present();
            let data = "ICBF";
            this.purchase(data);
        });
    }
    purchase(source) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            let date = this.formatDate();
            yield this.db.savePurchase(this.tag, this.cut, date, source);
            yield this.db.uploadPurchase().then(() => {
                console.log("purchase saved");
            });
            this.dismissSave();
        });
    }
    formatDate() {
        var today = new Date();
        var dd = String(today.getDate()).padStart(2, "0");
        var mm = String(today.getMonth() + 1).padStart(2, "0"); //January is 0!
        var yyyy = today.getFullYear();
        var date = mm + "/" + dd + "/" + yyyy;
        return date;
    }
};
InfoPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ModalController"] },
    { type: _services_database_product_info_service__WEBPACK_IMPORTED_MODULE_4__["ProductInfoService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["AlertController"] }
];
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Number)
], InfoPage.prototype, "tag", void 0);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])("rating", { static: false }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
], InfoPage.prototype, "rating", void 0);
InfoPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: "app-info",
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./info.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/info/info.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./info.page.scss */ "./src/app/pages/info/info.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ModalController"],
        _services_database_product_info_service__WEBPACK_IMPORTED_MODULE_4__["ProductInfoService"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["AlertController"]])
], InfoPage);



/***/ }),

/***/ "./src/app/pages/rate/rate-routing.module.ts":
/*!***************************************************!*\
  !*** ./src/app/pages/rate/rate-routing.module.ts ***!
  \***************************************************/
/*! exports provided: RatePageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RatePageRoutingModule", function() { return RatePageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _rate_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./rate.page */ "./src/app/pages/rate/rate.page.ts");




const routes = [
    {
        path: '',
        component: _rate_page__WEBPACK_IMPORTED_MODULE_3__["RatePage"]
    }
];
let RatePageRoutingModule = class RatePageRoutingModule {
};
RatePageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], RatePageRoutingModule);



/***/ }),

/***/ "./src/app/pages/rate/rate.module.ts":
/*!*******************************************!*\
  !*** ./src/app/pages/rate/rate.module.ts ***!
  \*******************************************/
/*! exports provided: RatePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RatePageModule", function() { return RatePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _rate_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./rate-routing.module */ "./src/app/pages/rate/rate-routing.module.ts");
/* harmony import */ var _rate_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./rate.page */ "./src/app/pages/rate/rate.page.ts");
/* harmony import */ var ionic_rating__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ionic-rating */ "./node_modules/ionic-rating/fesm2015/ionic-rating.js");







// import { NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

let RatePageModule = class RatePageModule {
};
RatePageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _rate_routing_module__WEBPACK_IMPORTED_MODULE_5__["RatePageRoutingModule"],
            ionic_rating__WEBPACK_IMPORTED_MODULE_7__["IonicRatingModule"]
        ],
        // schemas: [
        //   CUSTOM_ELEMENTS_SCHEMA,
        //   NO_ERRORS_SCHEMA 
        // ],
        declarations: [_rate_page__WEBPACK_IMPORTED_MODULE_6__["RatePage"]]
    })
], RatePageModule);



/***/ }),

/***/ "./src/app/pages/rate/rate.page.scss":
/*!*******************************************!*\
  !*** ./src/app/pages/rate/rate.page.scss ***!
  \*******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".background {\n  --background: black;\n  text-align: center;\n}\n\n#left {\n  display: flex;\n  padding-left: 3%;\n  color: white;\n  font-size: 180%;\n}\n\n#label {\n  color: white;\n  font-size: 200%;\n  text-align: center;\n}\n\n#category {\n  color: #939393;\n  font-size: 150%;\n  padding-top: 10%;\n}\n\n#grid {\n  margin-top: 10%;\n  height: 70%;\n}\n\n#rateCol {\n  padding-left: 5%;\n  padding-right: 5%;\n  padding-top: 4%;\n}\n\n#foot {\n  background-color: #212121;\n  height: 10%;\n}\n\n#right {\n  padding-top: 2%;\n  display: flex;\n  align-items: center;\n  justify-content: center;\n  color: white;\n  font-size: 140%;\n  font-weight: 500;\n}\n\n.alertcss {\n  color: white !important;\n}\n\n#back {\n  background-color: black;\n}\n\nion-item {\n  background-color: black;\n}\n\n.ratingDrop {\n  padding-left: 5%;\n  padding-right: 5%;\n  padding-bottom: 5%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9saWFtY2hhbWJlcnMvRG9jdW1lbnRzL21lYXRlYXRpbmdxdWFsaXR5L3NyYy9hcHAvcGFnZXMvcmF0ZS9yYXRlLnBhZ2Uuc2NzcyIsInNyYy9hcHAvcGFnZXMvcmF0ZS9yYXRlLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUVJLG1CQUFBO0VBQ0Esa0JBQUE7QUNBSjs7QURJRTtFQUNFLGFBQUE7RUFDQSxnQkFBQTtFQUNBLFlBQUE7RUFDQSxlQUFBO0FDREo7O0FESUU7RUFDSSxZQUFBO0VBQ0EsZUFBQTtFQUNBLGtCQUFBO0FDRE47O0FESUU7RUFDRSxjQUFBO0VBQ0EsZUFBQTtFQUNBLGdCQUFBO0FDREo7O0FESUU7RUFDRSxlQUFBO0VBQ0EsV0FBQTtBQ0RKOztBRElDO0VBQ0UsZ0JBQUE7RUFDQSxpQkFBQTtFQUNBLGVBQUE7QUNESDs7QURJQztFQUNFLHlCQUFBO0VBQ0EsV0FBQTtBQ0RIOztBRElDO0VBQ0MsZUFBQTtFQUNBLGFBQUE7RUFDQSxtQkFBQTtFQUNBLHVCQUFBO0VBQ0EsWUFBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtBQ0RGOztBRElBO0VBQ0ksdUJBQUE7QUNESjs7QURJQTtFQUNFLHVCQUFBO0FDREY7O0FESUE7RUFDRSx1QkFBQTtBQ0RGOztBRElBO0VBQ0UsZ0JBQUE7RUFDQSxpQkFBQTtFQUNBLGtCQUFBO0FDREYiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9yYXRlL3JhdGUucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmJhY2tncm91bmQge1xuICAgIC8vLS1iYWNrZ3JvdW5kOiB1cmwoLi4vLi4vLi4vYXNzZXRzL2ltYWdlcy9pbWFnZS5qcGcpIDAgMC8xMDAlIDEwMCUgbm8tcmVwZWF0O1xuICAgIC0tYmFja2dyb3VuZDogYmxhY2s7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIC8vb3ZlcmZsb3cteTogc2Nyb2xsO1xuICB9XG4gIFxuICAjbGVmdCB7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBwYWRkaW5nLWxlZnQ6IDMlO1xuICAgIGNvbG9yOiB3aGl0ZTtcbiAgICBmb250LXNpemU6IDE4MCU7XG4gIH1cblxuICAjbGFiZWx7XG4gICAgICBjb2xvcjp3aGl0ZTtcbiAgICAgIGZvbnQtc2l6ZToyMDAlO1xuICAgICAgdGV4dC1hbGlnbjpjZW50ZXI7XG4gIH1cblxuICAjY2F0ZWdvcnl7XG4gICAgY29sb3I6IzkzOTM5MztcbiAgICBmb250LXNpemU6MTUwJTtcbiAgICBwYWRkaW5nLXRvcDoxMCU7XG4gIH1cblxuICAjZ3JpZHtcbiAgICBtYXJnaW4tdG9wOjEwJTtcbiAgICBoZWlnaHQ6NzAlO1xuICB9XG5cbiAjcmF0ZUNvbHtcbiAgIHBhZGRpbmctbGVmdDo1JTtcbiAgIHBhZGRpbmctcmlnaHQ6NSU7XG4gICBwYWRkaW5nLXRvcDo0JTtcbiB9XG5cbiAjZm9vdHtcbiAgIGJhY2tncm91bmQtY29sb3I6ICMyMTIxMjE7XG4gICBoZWlnaHQ6MTAlO1xuIH1cblxuICNyaWdodCB7XG4gIHBhZGRpbmctdG9wOiAyJTtcbiAgZGlzcGxheTogZmxleDtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgZm9udC1zaXplOiAxNDAlO1xuICBmb250LXdlaWdodDogNTAwO1xufVxuXG4uYWxlcnRjc3N7XG4gICAgY29sb3I6d2hpdGUgIWltcG9ydGFudDtcbn1cblxuI2JhY2t7XG4gIGJhY2tncm91bmQtY29sb3I6YmxhY2s7XG59XG5cbmlvbi1pdGVte1xuICBiYWNrZ3JvdW5kLWNvbG9yOmJsYWNrO1xufVxuXG4ucmF0aW5nRHJvcHtcbiAgcGFkZGluZy1sZWZ0OjUlO1xuICBwYWRkaW5nLXJpZ2h0OjUlO1xuICBwYWRkaW5nLWJvdHRvbTo1JTtcbn0iLCIuYmFja2dyb3VuZCB7XG4gIC0tYmFja2dyb3VuZDogYmxhY2s7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxuI2xlZnQge1xuICBkaXNwbGF5OiBmbGV4O1xuICBwYWRkaW5nLWxlZnQ6IDMlO1xuICBjb2xvcjogd2hpdGU7XG4gIGZvbnQtc2l6ZTogMTgwJTtcbn1cblxuI2xhYmVsIHtcbiAgY29sb3I6IHdoaXRlO1xuICBmb250LXNpemU6IDIwMCU7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxuI2NhdGVnb3J5IHtcbiAgY29sb3I6ICM5MzkzOTM7XG4gIGZvbnQtc2l6ZTogMTUwJTtcbiAgcGFkZGluZy10b3A6IDEwJTtcbn1cblxuI2dyaWQge1xuICBtYXJnaW4tdG9wOiAxMCU7XG4gIGhlaWdodDogNzAlO1xufVxuXG4jcmF0ZUNvbCB7XG4gIHBhZGRpbmctbGVmdDogNSU7XG4gIHBhZGRpbmctcmlnaHQ6IDUlO1xuICBwYWRkaW5nLXRvcDogNCU7XG59XG5cbiNmb290IHtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzIxMjEyMTtcbiAgaGVpZ2h0OiAxMCU7XG59XG5cbiNyaWdodCB7XG4gIHBhZGRpbmctdG9wOiAyJTtcbiAgZGlzcGxheTogZmxleDtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgZm9udC1zaXplOiAxNDAlO1xuICBmb250LXdlaWdodDogNTAwO1xufVxuXG4uYWxlcnRjc3Mge1xuICBjb2xvcjogd2hpdGUgIWltcG9ydGFudDtcbn1cblxuI2JhY2sge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiBibGFjaztcbn1cblxuaW9uLWl0ZW0ge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiBibGFjaztcbn1cblxuLnJhdGluZ0Ryb3Age1xuICBwYWRkaW5nLWxlZnQ6IDUlO1xuICBwYWRkaW5nLXJpZ2h0OiA1JTtcbiAgcGFkZGluZy1ib3R0b206IDUlO1xufSJdfQ== */");

/***/ }),

/***/ "./src/app/pages/rate/rate.page.ts":
/*!*****************************************!*\
  !*** ./src/app/pages/rate/rate.page.ts ***!
  \*****************************************/
/*! exports provided: RatePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RatePage", function() { return RatePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _services_database_rate_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/database/rate.service */ "./src/app/services/database/rate.service.ts");





let RatePage = class RatePage {
    constructor(modalController, db, alertController) {
        this.modalController = modalController;
        this.db = db;
        this.alertController = alertController;
        this.tenderness = 0;
        this.juciness = 0;
        this.flavour = 0;
        this.marbelling = 0;
        this.appearance = 0;
        this.overall = 0;
    }
    ngOnInit() { }
    // tenderness
    onRateChange1(event) {
        this.tenderness = event;
    }
    //juciness
    onRateChange2(event) {
        this.juciness = event;
    }
    //flavour
    onRateChange3(event) {
        this.flavour = event;
    }
    //marbelling
    onRateChange4(event) {
        this.marbelling = event;
    }
    //appearance
    onRateChange5(event) {
        this.appearance = event;
    }
    //overall
    onRateChange6(event) {
        this.overall = event;
    }
    dismiss() {
        this.modalController.dismiss("user");
    }
    submit() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            if (this.method == undefined || this.doneness == undefined) {
                if (this.method == undefined) {
                    const alert1 = yield this.alertController.create({
                        header: "Please select your cooking method.",
                        cssClass: "alertcss",
                        buttons: [
                            {
                                text: "Continue",
                                handler: (blah) => {
                                    alert1.dismiss();
                                },
                            },
                        ],
                    });
                    alert1.present();
                }
                if (this.doneness == undefined) {
                    const alert2 = yield this.alertController.create({
                        header: "Please select how your steak was cooked.",
                        cssClass: "alertcss",
                        buttons: [
                            {
                                text: "Continue",
                                handler: (blah) => {
                                    alert2.dismiss();
                                },
                            },
                        ],
                    });
                    alert2.present();
                }
            }
            else if (this.doneness == 0 || this.flavour == 0 || this.juciness == 0 || this.marbelling == 0 || this.overall == 0) {
                const alert3 = yield this.alertController.create({
                    header: "Please fill out all ratings.",
                    cssClass: "alertcss",
                    buttons: [
                        {
                            text: "Continue",
                            handler: (blah) => {
                                alert3.dismiss();
                            },
                        },
                    ],
                });
                alert3.present();
            }
            else {
                console.log(this.tenderness, this.juciness, this.flavour, this.marbelling, this.id, "method" + this.method, "doness" + this.doneness);
                yield this.db.saveRating(this.appearance, this.tenderness, this.juciness, this.flavour, this.marbelling, this.overall, this.method, this.doneness, this.id);
                yield this.db.uploadRating();
                const alert = yield this.alertController.create({
                    header: "Thank you for your feedback!",
                    cssClass: "alertcss",
                    buttons: [
                        {
                            text: "Continue",
                            handler: (blah) => {
                                this.dismiss();
                            },
                        },
                    ],
                });
                alert.present();
            }
        });
    }
};
RatePage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"] },
    { type: _services_database_rate_service__WEBPACK_IMPORTED_MODULE_3__["RateService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"] }
];
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Number)
], RatePage.prototype, "id", void 0);
RatePage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: "app-rate",
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./rate.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/rate/rate.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./rate.page.scss */ "./src/app/pages/rate/rate.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"],
        _services_database_rate_service__WEBPACK_IMPORTED_MODULE_3__["RateService"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"]])
], RatePage);



/***/ }),

/***/ "./src/app/pages/tab2/tab2.page.scss":
/*!*******************************************!*\
  !*** ./src/app/pages/tab2/tab2.page.scss ***!
  \*******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".background {\n  --background: black;\n}\n\n#home {\n  color: white;\n  font-size: 150%;\n  padding-top: 1%;\n}\n\n.purchase {\n  padding-top: 2%;\n  height: 32%;\n  background-color: #232b2b;\n}\n\n.profilePic {\n  width: 100%;\n}\n\n#buttonRow {\n  padding-top: 5%;\n}\n\n#imgCol {\n  padding-top: 3%;\n  padding-left: 5%;\n  height: 100%;\n}\n\n#infoCol {\n  height: 100%;\n}\n\n#animalRow {\n  height: 100%;\n}\n\n#grid {\n  height: 65%;\n}\n\n.name {\n  font-size: 118%;\n  color: white;\n  font-weight: bold;\n}\n\n.left {\n  padding-top: 3%;\n  float: left;\n  color: #939393;\n}\n\n.right {\n  padding-top: 3%;\n  float: left;\n  color: #d3d3d3;\n}\n\n#origin {\n  margin-top: 8%;\n}\n\n#yourRating {\n  color: white;\n  font-size: 150%;\n  display: inline-block;\n}\n\n#labelCol {\n  display: flex;\n  align-items: center;\n  justify-content: center;\n  padding-left: 2%;\n}\n\n#starCol {\n  padding-bottom: 3%;\n  padding-left: 5%;\n}\n\n#ratingCol {\n  text-align: center;\n  display: flex;\n  align-items: center;\n  justify-content: center;\n  padding-left: 5%;\n}\n\n#tag {\n  color: white;\n  margin-left: 3%;\n  margin-top: -10%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9saWFtY2hhbWJlcnMvRG9jdW1lbnRzL21lYXRlYXRpbmdxdWFsaXR5L3NyYy9hcHAvcGFnZXMvdGFiMi90YWIyLnBhZ2Uuc2NzcyIsInNyYy9hcHAvcGFnZXMvdGFiMi90YWIyLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUVJLG1CQUFBO0FDQUo7O0FER0E7RUFDSSxZQUFBO0VBQ0EsZUFBQTtFQUNBLGVBQUE7QUNBSjs7QURHQTtFQUNJLGVBQUE7RUFDQSxXQUFBO0VBQ0EseUJBQUE7QUNBSjs7QURHQTtFQUNJLFdBQUE7QUNBSjs7QURHQTtFQUNJLGVBQUE7QUNBSjs7QURHQTtFQUNJLGVBQUE7RUFDQSxnQkFBQTtFQUNBLFlBQUE7QUNBSjs7QURHQTtFQUNJLFlBQUE7QUNBSjs7QURHQTtFQUNJLFlBQUE7QUNBSjs7QURHQTtFQUNJLFdBQUE7QUNBSjs7QURHQTtFQUNJLGVBQUE7RUFDQSxZQUFBO0VBQ0EsaUJBQUE7QUNBSjs7QURHQTtFQUNJLGVBQUE7RUFDQSxXQUFBO0VBQ0EsY0FBQTtBQ0FKOztBREdBO0VBQ0ksZUFBQTtFQUNBLFdBQUE7RUFDQSxjQUFBO0FDQUo7O0FESUE7RUFDSSxjQUFBO0FDREo7O0FESUE7RUFDSSxZQUFBO0VBQ0EsZUFBQTtFQUNBLHFCQUFBO0FDREo7O0FESUE7RUFDSSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSx1QkFBQTtFQUNBLGdCQUFBO0FDREo7O0FESUE7RUFDSSxrQkFBQTtFQUNBLGdCQUFBO0FDREo7O0FESUE7RUFDSSxrQkFBQTtFQUNBLGFBQUE7RUFDQSxtQkFBQTtFQUNBLHVCQUFBO0VBQ0EsZ0JBQUE7QUNESjs7QURJQTtFQUNJLFlBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7QUNESiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3RhYjIvdGFiMi5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuYmFja2dyb3VuZHtcbiAgICAvLy0tYmFja2dyb3VuZDogdXJsKC4uLy4uLy4uL2Fzc2V0cy9pbWFnZXMvaW1hZ2UuanBnKSAwIDAvMTAwJSAxMDAlIG5vLXJlcGVhdDtcbiAgICAtLWJhY2tncm91bmQ6IGJsYWNrO1xufVxuXG4jaG9tZXtcbiAgICBjb2xvcjp3aGl0ZTtcbiAgICBmb250LXNpemU6MTUwJTtcbiAgICBwYWRkaW5nLXRvcDoxJTtcbn1cblxuLnB1cmNoYXNle1xuICAgIHBhZGRpbmctdG9wOjIlO1xuICAgIGhlaWdodDozMiU7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogIzIzMmIyYjtcbn1cblxuLnByb2ZpbGVQaWN7XG4gICAgd2lkdGg6MTAwJTtcbn1cblxuI2J1dHRvblJvd3tcbiAgICBwYWRkaW5nLXRvcDo1JTtcbn1cblxuI2ltZ0NvbHtcbiAgICBwYWRkaW5nLXRvcDozJTtcbiAgICBwYWRkaW5nLWxlZnQ6NSU7XG4gICAgaGVpZ2h0OjEwMCU7XG59XG5cbiNpbmZvQ29se1xuICAgIGhlaWdodDoxMDAlO1xufVxuXG4jYW5pbWFsUm93e1xuICAgIGhlaWdodDoxMDAlO1xufVxuXG4jZ3JpZHtcbiAgICBoZWlnaHQ6NjUlO1xufVxuXG4ubmFtZXtcbiAgICBmb250LXNpemU6MTE4JTtcbiAgICBjb2xvcjp3aGl0ZTtcbiAgICBmb250LXdlaWdodDpib2xkO1xufVxuXG4ubGVmdHtcbiAgICBwYWRkaW5nLXRvcDozJTtcbiAgICBmbG9hdDpsZWZ0O1xuICAgIGNvbG9yOiM5MzkzOTM7XG59XG5cbi5yaWdodHtcbiAgICBwYWRkaW5nLXRvcDozJTtcbiAgICBmbG9hdDpsZWZ0O1xuICAgIGNvbG9yOiNkM2QzZDM7XG59XG5cblxuI29yaWdpbntcbiAgICBtYXJnaW4tdG9wOjglO1xufVxuXG4jeW91clJhdGluZ3tcbiAgICBjb2xvcjp3aGl0ZTtcbiAgICBmb250LXNpemU6MTUwJTtcbiAgICBkaXNwbGF5OmlubGluZS1ibG9jaztcbn1cblxuI2xhYmVsQ29se1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjsgXG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgcGFkZGluZy1sZWZ0OjIlO1xufVxuXG4jc3RhckNvbHtcbiAgICBwYWRkaW5nLWJvdHRvbTozJTtcbiAgICBwYWRkaW5nLWxlZnQ6NSU7XG59XG5cbiNyYXRpbmdDb2x7XG4gICAgdGV4dC1hbGlnbjpjZW50ZXI7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBhbGlnbi1pdGVtczogY2VudGVyOyBcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICBwYWRkaW5nLWxlZnQ6NSU7XG59XG5cbiN0YWd7XG4gICAgY29sb3I6d2hpdGU7XG4gICAgbWFyZ2luLWxlZnQ6MyU7XG4gICAgbWFyZ2luLXRvcDotMTAlO1xufVxuXG5cbiIsIi5iYWNrZ3JvdW5kIHtcbiAgLS1iYWNrZ3JvdW5kOiBibGFjaztcbn1cblxuI2hvbWUge1xuICBjb2xvcjogd2hpdGU7XG4gIGZvbnQtc2l6ZTogMTUwJTtcbiAgcGFkZGluZy10b3A6IDElO1xufVxuXG4ucHVyY2hhc2Uge1xuICBwYWRkaW5nLXRvcDogMiU7XG4gIGhlaWdodDogMzIlO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMjMyYjJiO1xufVxuXG4ucHJvZmlsZVBpYyB7XG4gIHdpZHRoOiAxMDAlO1xufVxuXG4jYnV0dG9uUm93IHtcbiAgcGFkZGluZy10b3A6IDUlO1xufVxuXG4jaW1nQ29sIHtcbiAgcGFkZGluZy10b3A6IDMlO1xuICBwYWRkaW5nLWxlZnQ6IDUlO1xuICBoZWlnaHQ6IDEwMCU7XG59XG5cbiNpbmZvQ29sIHtcbiAgaGVpZ2h0OiAxMDAlO1xufVxuXG4jYW5pbWFsUm93IHtcbiAgaGVpZ2h0OiAxMDAlO1xufVxuXG4jZ3JpZCB7XG4gIGhlaWdodDogNjUlO1xufVxuXG4ubmFtZSB7XG4gIGZvbnQtc2l6ZTogMTE4JTtcbiAgY29sb3I6IHdoaXRlO1xuICBmb250LXdlaWdodDogYm9sZDtcbn1cblxuLmxlZnQge1xuICBwYWRkaW5nLXRvcDogMyU7XG4gIGZsb2F0OiBsZWZ0O1xuICBjb2xvcjogIzkzOTM5Mztcbn1cblxuLnJpZ2h0IHtcbiAgcGFkZGluZy10b3A6IDMlO1xuICBmbG9hdDogbGVmdDtcbiAgY29sb3I6ICNkM2QzZDM7XG59XG5cbiNvcmlnaW4ge1xuICBtYXJnaW4tdG9wOiA4JTtcbn1cblxuI3lvdXJSYXRpbmcge1xuICBjb2xvcjogd2hpdGU7XG4gIGZvbnQtc2l6ZTogMTUwJTtcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xufVxuXG4jbGFiZWxDb2wge1xuICBkaXNwbGF5OiBmbGV4O1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgcGFkZGluZy1sZWZ0OiAyJTtcbn1cblxuI3N0YXJDb2wge1xuICBwYWRkaW5nLWJvdHRvbTogMyU7XG4gIHBhZGRpbmctbGVmdDogNSU7XG59XG5cbiNyYXRpbmdDb2wge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBwYWRkaW5nLWxlZnQ6IDUlO1xufVxuXG4jdGFnIHtcbiAgY29sb3I6IHdoaXRlO1xuICBtYXJnaW4tbGVmdDogMyU7XG4gIG1hcmdpbi10b3A6IC0xMCU7XG59Il19 */");

/***/ }),

/***/ "./src/app/pages/tab2/tab2.page.ts":
/*!*****************************************!*\
  !*** ./src/app/pages/tab2/tab2.page.ts ***!
  \*****************************************/
/*! exports provided: Tab2Page */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Tab2Page", function() { return Tab2Page; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _rate_rate_page__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../rate/rate.page */ "./src/app/pages/rate/rate.page.ts");
/* harmony import */ var _services_database_history_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../services/database/history.service */ "./src/app/services/database/history.service.ts");







let Tab2Page = class Tab2Page {
    constructor(loadingCtrl, router, modalController, history) {
        this.loadingCtrl = loadingCtrl;
        this.router = router;
        this.modalController = modalController;
        this.history = history;
        // this.loadPurchases();
    }
    // In this case, we are waiting for the Nav with reference variable of "#myNav"
    ionViewWillEnter() {
        console.log("entered!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
        this.purchases = [];
        this.loadPurchases();
    }
    home() {
        this.router.navigateByUrl("dashboard");
    }
    rate(item) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const modal = yield this.modalController.create({
                component: _rate_rate_page__WEBPACK_IMPORTED_MODULE_4__["RatePage"],
                componentProps: {
                    id: item.id
                }
            });
            modal.present();
            modal.onDidDismiss()
                .then((data) => {
                this.loadPurchases();
            });
        });
    }
    dismiss(item, index) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            console.log(index);
            let card = document.getElementById(index);
            card.style.display = 'none';
            console.log("---------------- DISMISS 1 - IN tab2.ts ------------------------");
            this.loader = yield this.loadingCtrl.create({
                spinner: 'crescent',
                // duration: 240000, // E Fitz 13-sep-2019 will spin for 4 minutes or until the task is finished
                message: 'Removing product...'
            });
            // Show the spinner
            // this.loader.present().then(() => {});
            let id = item.id;
            yield this.history.saveDismiss(id);
            yield this.history.uploadDismiss();
            //this.loadPurchases();
            // this.loader.dismiss();
        });
    }
    loadPurchases() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            this.purchases = [];
            this.loader = yield this.loadingCtrl.create({
                spinner: 'crescent',
                // duration: 240000, // E Fitz 13-sep-2019 will spin for 4 minutes or until the task is finished
                message: 'Loading Products...'
            });
            // this.loader.present().then(() => {});
            yield this.history.callPurchaseHistory();
            this.history.loadPurchases().then((purchases) => {
                console.log(JSON.stringify(purchases));
                console.log("res ooooooooooooo" + purchases);
                this.purchases = purchases;
            });
            // this.loadingCtrl.dismiss();
        });
    }
};
Tab2Page.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ModalController"] },
    { type: _services_database_history_service__WEBPACK_IMPORTED_MODULE_5__["HistoryService"] }
];
Tab2Page = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-tab2',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./tab2.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/tab2/tab2.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./tab2.page.scss */ "./src/app/pages/tab2/tab2.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ModalController"], _services_database_history_service__WEBPACK_IMPORTED_MODULE_5__["HistoryService"]])
], Tab2Page);



/***/ }),

/***/ "./src/app/services/api/api-global.service.ts":
/*!****************************************************!*\
  !*** ./src/app/services/api/api-global.service.ts ***!
  \****************************************************/
/*! exports provided: ApiglobalService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ApiglobalService", function() { return ApiglobalService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _auth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../auth.service */ "./src/app/auth.service.ts");




let ApiglobalService = class ApiglobalService {
    constructor(auth) {
        this.auth = auth;
        this.baseurl = 'https://api.icbf.com';
        this.errorlist = [];
        // PROD
        // public bearerstring = '';
        // public baseurl = 'https://api.icbf.com';
        this.headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]();
    }
    getBearer() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            console.log('---- GETTING BEARER ----');
            yield this.auth.retCurrentAccessToken()
                .then((data) => {
                this.tokens = data;
            }).catch(error => console.error("Error on getting Token Object ", error));
            console.log("Token: ", this.tokens.access_token);
            return this.tokens.access_token;
        });
    }
};
ApiglobalService.ctorParameters = () => [
    { type: _auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"] }
];
ApiglobalService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"]])
], ApiglobalService);



/***/ }),

/***/ "./src/app/services/api/api-history.service.ts":
/*!*****************************************************!*\
  !*** ./src/app/services/api/api-history.service.ts ***!
  \*****************************************************/
/*! exports provided: ApiHistoryService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ApiHistoryService", function() { return ApiHistoryService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _api_api_global_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../api/api-global.service */ "./src/app/services/api/api-global.service.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _database_database_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../database/database.service */ "./src/app/services/database/database.service.ts");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm2015/http.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");







let ApiHistoryService = class ApiHistoryService {
    constructor(apiglobal, httpclient, db, http) {
        this.apiglobal = apiglobal;
        this.httpclient = httpclient;
        this.db = db;
        this.http = http;
        this.history = [];
    }
    // Get the url for herd details, each function will need this as its recursive
    getUrl() {
        let url = this.apiglobal.baseurl + "/meat-eating-quality/customer-purchase?par_id_customer=7";
        console.log("ApiPurchaseHistoryService: getUrl: " + url);
        return url;
    }
    retPurchaseHistory(url) {
        let promise = new Promise((resolve, reject) => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            var token = yield this.apiglobal.getBearer();
            try {
                let data;
                let HttpOptions = {
                    headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpHeaders"]().set('Authorization', 'Bearer ' + token)
                };
                console.log('HttpOptions = ' + HttpOptions);
                console.log('Bearer Token = ' + token);
                //console.log('ApiBullService:getbulls bearer', this.apiglobal.bearerstring);
                this.httpclient.get(url, HttpOptions).subscribe((response) => {
                    console.log("HistoryService:herd url");
                    if (response != null) {
                        data = response;
                        console.log("response from the api" + JSON.stringify(data));
                        console.log(this.history);
                        //NB Adding the next page of results to the array when copying make sure to check\change these parameters
                        this.history = this.history.concat(data._embedded.customer_purchase);
                        console.log("HistoryService:retPurchaseHistory: data.page: " + data.page);
                        console.log("HistoryService:retPurchaseHistory: data.page_count: " +
                            data.page_count);
                        if (data.page != data.page_count) {
                            console.log("HistoryService:retPurchaseHistory: Looping url:" +
                                data._links.next.href);
                            this.retPurchaseHistory(data._links.next.href)
                                .then((data) => {
                                resolve(data);
                            })
                                .catch(error => {
                                // To handle an error from the purchase history api
                                this.apiglobal.errorlist.push("Purchase History:" + error.message);
                                reject(error);
                            });
                        }
                        else {
                            console.log("HistoryService:retPurchaseHistory: resolved data.page ><");
                            console.log("product info api data" + JSON.stringify(data));
                            resolve(data);
                        }
                    }
                    if (response == null) {
                        console.log("HistoryService:retPurchaseHistory: resolved response null ><");
                        resolve(data);
                    }
                }, 
                // To handle an error from the herd details api
                (error) => {
                    this.apiglobal.errorlist.push("ProductInfo:" + error.message);
                    console.log(error.message);
                    reject(error);
                });
            }
            catch (error) {
                console.log("HistoryService:retPurchaseHistory error:" + error.message);
                // To handle an error from the herd details api
                this.apiglobal.errorlist.push("Health:" + error.message);
                reject(error);
            }
            finally {
            }
        }));
        console.log("end of retPurchaseHistory" + this.history);
        return promise;
    }
    sendPostRequestDismiss(ID) {
        console.log("---------------- DISMISS 4 - ApiHistoryService: sendPostRequestDismiss------------------------");
        console.log("data going in last" + ID);
        return new Promise((resolve, reject) => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            let url = this.apiglobal.baseurl + "/meat-eating-quality/customer-purchase/" + ID;
            var token = yield this.apiglobal.getBearer();
            let cpHeaders = new _angular_http__WEBPACK_IMPORTED_MODULE_5__["Headers"]({
                "Content-Type": "application/json",
                Authorization: "Bearer " + token,
            });
            let postData = {
                meq_purchase_history_id: ID
            };
            return this.http.put(url.toString(), JSON.stringify(postData), { headers: cpHeaders })
                .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["map"])((result) => result.json()))
                .subscribe(
            //.toPromise().then
            (result) => {
                console.log("------------------ ApiRateService sendPostRequestDismiss result" + JSON.stringify(result));
                resolve(result);
            }, (error) => {
                console.error("ApiRateService sendPostRequestDismiss Error:" +
                    error +
                    " validation_messages:" +
                    error.validation_messages +
                    "JSON.stringify(error)" +
                    JSON.stringify(error));
                reject(error);
            });
        }));
    }
    // Called when getbulls has finished loading the array bulls
    getHistoryArray() {
        console.log("returning history array" + JSON.stringify(this.history));
        return this.history;
    }
    // Clear Herd array
    clearHistoryArray() {
        this.history = [];
    }
};
ApiHistoryService.ctorParameters = () => [
    { type: _api_api_global_service__WEBPACK_IMPORTED_MODULE_2__["ApiglobalService"] },
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"] },
    { type: _database_database_service__WEBPACK_IMPORTED_MODULE_4__["DatabaseService"] },
    { type: _angular_http__WEBPACK_IMPORTED_MODULE_5__["Http"] }
];
ApiHistoryService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_api_api_global_service__WEBPACK_IMPORTED_MODULE_2__["ApiglobalService"],
        _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"],
        _database_database_service__WEBPACK_IMPORTED_MODULE_4__["DatabaseService"],
        _angular_http__WEBPACK_IMPORTED_MODULE_5__["Http"]])
], ApiHistoryService);



/***/ }),

/***/ "./src/app/services/api/api-product-info.service.ts":
/*!**********************************************************!*\
  !*** ./src/app/services/api/api-product-info.service.ts ***!
  \**********************************************************/
/*! exports provided: ApiProductInfoService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ApiProductInfoService", function() { return ApiProductInfoService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _api_api_global_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../api/api-global.service */ "./src/app/services/api/api-global.service.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _database_database_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../database/database.service */ "./src/app/services/database/database.service.ts");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm2015/http.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");







let ApiProductInfoService = class ApiProductInfoService {
    constructor(apiglobal, httpclient, db, http) {
        this.apiglobal = apiglobal;
        this.httpclient = httpclient;
        this.db = db;
        this.http = http;
        this.info = [];
    }
    // Get the url for herd details, each function will need this as its recursive
    getUrl() {
        let url = this.apiglobal.baseurl + "/meat-eating-quality/product-info?";
        console.log("ApiEventService: getUrl: " + url);
        return url;
    }
    retProductInfo(url, tag) {
        let promise = new Promise((resolve, reject) => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            var token = yield this.apiglobal.getBearer();
            try {
                let data;
                let HttpOptions = {
                    headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpHeaders"]().set('Authorization', 'Bearer ' + token),
                    params: new _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpParams"]().set("barcode", tag),
                };
                console.log('HttpOptions = ' + HttpOptions);
                console.log('Bearer Token = ' + token);
                //console.log('ApiBullService:getbulls bearer', this.apiglobal.bearerstring);
                this.httpclient.get(url, HttpOptions).subscribe((response) => {
                    console.log("ProductInfoService:herd url", url);
                    if (response != null) {
                        data = response;
                        console.log("response from the api" + JSON.stringify(data));
                        //NB Adding the next page of results to the array when copying make sure to check\change these parameters
                        this.info = this.info.concat(data._embedded.product_info);
                        console.log("ProductInfoService:retProductInfo: data.page: " + data.page);
                        console.log("ProductInfoService:retProductInfo: data.page_count: " +
                            data.page_count);
                        if (data.page != data.page_count) {
                            console.log("ProductInfoService:retProductInfo: Looping url:" +
                                data._links.next.href);
                            this.retProductInfo(data._links.next.href, tag)
                                .then((data) => {
                                resolve(data);
                            })
                                .catch(error => {
                                // To handle an error from the herd details api
                                this.apiglobal.errorlist.push("ProductInfo:" + error.message);
                                reject(error);
                            });
                        }
                        else {
                            console.log("ProductInfoService:retProductInfo: resolved data.page ><");
                            console.log("product info api data" + JSON.stringify(data));
                            resolve(data);
                        }
                    }
                    if (response == null) {
                        console.log("ProductInfoService:retProductInfo: resolved response null ><");
                        resolve(data);
                    }
                }, 
                // To handle an error from the herd details api
                (error) => {
                    this.apiglobal.errorlist.push("ProductInfo:" + error.message);
                    console.log(error.message);
                    reject(error);
                });
            }
            catch (error) {
                console.log("ProductInfoService:retProductInfo error:" + error.message);
                // To handle an error from the herd details api
                this.apiglobal.errorlist.push("Health:" + error.message);
                reject(error);
            }
            finally {
            }
        }));
        return promise;
    }
    sendPostRequest(TAG, CUT, SOURCE) {
        console.log("data going in last" + TAG);
        return new Promise((resolve, reject) => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            let url = this.apiglobal.baseurl + '/meat-eating-quality/customer-purchase';
            var token = yield this.apiglobal.getBearer();
            console.log("bearer" + token);
            let cpHeaders = new _angular_http__WEBPACK_IMPORTED_MODULE_5__["Headers"]({
                "Content-Type": "application/json",
                Authorization: "Bearer " + token,
            });
            let postData = {
                barcode: TAG,
                purchased_from: SOURCE
            };
            return this.http
                .post(url.toString(), JSON.stringify(postData), { headers: cpHeaders })
                .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["map"])((result) => result.json()))
                .subscribe(
            //.toPromise().then
            (result) => {
                console.log("ProductInfo API sendPostRequest result" + result);
                resolve(result);
            }, (error) => {
                console.error("ProductInfo API  sendPostRequest Error:" +
                    error +
                    " validation_messages:" +
                    error.validation_messages +
                    "JSON.stringify(error)" +
                    JSON.stringify(error));
                reject(error);
            });
        }));
    }
    // Called when getbulls has finished loading the array bulls
    getInfoArray() {
        return this.info;
    }
    // Clear Herd array
    clearInfoArray() {
        this.info = [];
    }
};
ApiProductInfoService.ctorParameters = () => [
    { type: _api_api_global_service__WEBPACK_IMPORTED_MODULE_2__["ApiglobalService"] },
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"] },
    { type: _database_database_service__WEBPACK_IMPORTED_MODULE_4__["DatabaseService"] },
    { type: _angular_http__WEBPACK_IMPORTED_MODULE_5__["Http"] }
];
ApiProductInfoService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_api_api_global_service__WEBPACK_IMPORTED_MODULE_2__["ApiglobalService"],
        _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"],
        _database_database_service__WEBPACK_IMPORTED_MODULE_4__["DatabaseService"],
        _angular_http__WEBPACK_IMPORTED_MODULE_5__["Http"]])
], ApiProductInfoService);



/***/ }),

/***/ "./src/app/services/api/api-rate.service.ts":
/*!**************************************************!*\
  !*** ./src/app/services/api/api-rate.service.ts ***!
  \**************************************************/
/*! exports provided: ApiRateService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ApiRateService", function() { return ApiRateService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _api_api_global_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../api/api-global.service */ "./src/app/services/api/api-global.service.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm2015/http.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");







let ApiRateService = class ApiRateService {
    constructor(apiglobal, httpclient, http) {
        this.apiglobal = apiglobal;
        this.httpclient = httpclient;
        this.http = http;
    }
    // Get the url for herd details, each function will need this as its recursive
    getUrl() {
        let url = this.apiglobal.baseurl + "/herd-health/general-health";
        console.log("ApiEventService: getUrl: " + url);
        return url;
    }
    sendPostRequest(APPEARANCE, TENDERNESS, JUCINESS, FLAVOUR, MARBLING, OVERALL, METHOD, DONENESS, ID) {
        console.log("data going in last ---------------------------------------- " +
            "meq_purchase_id" + rxjs__WEBPACK_IMPORTED_MODULE_6__["identity"], "appearance: " + APPEARANCE, "marbling" + MARBLING, "tenderness: " + TENDERNESS, "juciness" + JUCINESS, "flavour" + FLAVOUR, "overall" + OVERALL, "comment" + "null", "method" + METHOD, "doneness" + DONENESS);
        return new Promise((resolve, reject) => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            var token = yield this.apiglobal.getBearer();
            let url = this.apiglobal.baseurl + "/meat-eating-quality/customer-rating";
            let cpHeaders = new _angular_http__WEBPACK_IMPORTED_MODULE_4__["Headers"]({
                "Content-Type": "application/json",
                Authorization: "Bearer " + token,
            });
            let postData = {
                meq_purchase_history_id: ID,
                appearance: APPEARANCE,
                marbling: MARBLING,
                tenderness: TENDERNESS,
                juiciness: JUCINESS,
                flavour: FLAVOUR,
                overall: OVERALL,
                comment: null,
                cooking_method: METHOD,
                doneness: DONENESS
            };
            console.log("post data" + JSON.stringify(postData));
            return this.http
                .post(url.toString(), JSON.stringify(postData), { headers: cpHeaders })
                .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["map"])((result) => result.json()))
                .subscribe(
            //.toPromise().then
            (result) => {
                console.log("ApiRateService sendPostRequest result" + result);
                resolve(result);
            }, (error) => {
                console.error("ApiRateService sendPostRequest Error:" +
                    error +
                    " validation_messages:" +
                    error.validation_messages +
                    "JSON.stringify(error)" +
                    JSON.stringify(error));
                reject(error);
            });
        }));
    }
};
ApiRateService.ctorParameters = () => [
    { type: _api_api_global_service__WEBPACK_IMPORTED_MODULE_2__["ApiglobalService"] },
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"] },
    { type: _angular_http__WEBPACK_IMPORTED_MODULE_4__["Http"] }
];
ApiRateService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: "root",
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_api_api_global_service__WEBPACK_IMPORTED_MODULE_2__["ApiglobalService"],
        _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"],
        _angular_http__WEBPACK_IMPORTED_MODULE_4__["Http"]])
], ApiRateService);



/***/ }),

/***/ "./src/app/services/database/database.service.ts":
/*!*******************************************************!*\
  !*** ./src/app/services/database/database.service.ts ***!
  \*******************************************************/
/*! exports provided: DatabaseService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DatabaseService", function() { return DatabaseService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _ionic_native_sqlite_ngx__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic-native/sqlite/ngx */ "./node_modules/@ionic-native/sqlite/ngx/index.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");




let DatabaseService = class DatabaseService {
    constructor(sqlite, plat) {
        this.sqlite = sqlite;
        this.plat = plat;
        this.databaseName = "icbfMEQ";
        this.isCreated = false;
        this.plat.ready().then(() => {
            console.log("calling DB create ");
            this.createDatabaseAndTable();
        });
    }
    //********* CREATE DATABASE START *************/
    createDatabaseAndTable() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            console.log("create database and table");
            // Define the application SQLite database for herd details
            yield this.sqlite
                .create({
                name: this.databaseName,
                location: "default"
            })
                .then((db) => {
                this.connection = db;
            })
                .catch(e => {
                console.log(e);
            });
            yield this.creatAll();
        });
    }
    creatAll() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            // Added the await command to wait for each function to finish before proceeding with the next
            yield this.createRatingTable();
            yield this.createScanTable();
            yield this.createProductInfoTable();
            yield this.createSavePurchaseTable();
            yield this.createPurchaseHistoryTable();
            yield this.createRatingTable();
        });
    }
    createScanTable() {
        return new Promise((resolve, reject) => {
            try {
                this.connection
                    .executeSql("CREATE TABLE IF NOT EXISTS SCAN(TAG text, EVENT_DATE text, API_POST_DTM VARCHAR(20) NULL)", [])
                    .then(res => {
                    console.log("Executed SQL - TABLE SCAN created");
                    resolve(res);
                })
                    .catch(e => {
                    console.log(e);
                    reject(e);
                });
            }
            catch (error) {
                console.log("error: createScanTable" + error);
                reject(error);
            }
        });
    }
    createProductInfoTable() {
        return new Promise((resolve, reject) => {
            try {
                this.connection
                    .executeSql("CREATE TABLE IF NOT EXISTS INFO(ANI_ID number, ANIMAL_NAME text, ORIGIN text, SEX text, CUT_OF_MEAT text, BREED text, MEAT_QUALITY_RATING number, GREEN_RATING number, ANIMAL_BREED_DESCRIPTION text, DRY_AGED text, FARMER_NAME text, ANIMAL_TAG text, BREED_NAME text)", [])
                    .then(res => {
                    console.log("Executed SQL - TABLE INFO created");
                    resolve(res);
                })
                    .catch(e => {
                    console.log(e);
                    reject(e);
                });
            }
            catch (error) {
                console.log("error: createInfoTable" + error);
                reject(error);
            }
        });
    }
    createRatingTable() {
        return new Promise((resolve, reject) => {
            try {
                this.connection
                    .executeSql("CREATE TABLE IF NOT EXISTS RATING(ID number, APPEARANCE number, MARBLING number, TENDERNESS number, JUCINESS number, FLAVOUR number, OVERALL number, DONENESS text, METHOD text, API_POST_DTM VARCHAR(20) NULL)", [])
                    .then(res => {
                    console.log("Executed SQL - TABLE RATING created");
                    resolve(res);
                })
                    .catch(e => {
                    console.log(e);
                    reject(e);
                });
            }
            catch (error) {
                console.log("error: createRatingTable" + error);
                reject(error);
            }
        });
    }
    createPurchaseHistoryTable() {
        return new Promise((resolve, reject) => {
            try {
                this.connection
                    .executeSql("CREATE TABLE IF NOT EXISTS PURCHASE_HISTORY(ID number, ANIMAL_NAME text, ORIGIN text, BREED text, PURCHASE_DATE text, DISMISS text, OVERALL_RATING number, ANIMAL_TAG text, BREED_NAME text)", [])
                    .then(res => {
                    console.log("Executed SQL - TABLE PURCHASE_HISTORY created");
                    resolve(res);
                })
                    .catch(e => {
                    console.log(e);
                    reject(e);
                });
            }
            catch (error) {
                console.log("error: createPurchaseHistoryTable" + error);
                reject(error);
            }
        });
    }
    createSavePurchaseTable() {
        return new Promise((resolve, reject) => {
            try {
                this.connection
                    .executeSql("CREATE TABLE IF NOT EXISTS SAVE_PURCHASE(TAG text, CUT text, EVENT_DATE text, SOURCE text, API_POST_DTM VARCHAR(20) NULL)", [])
                    .then(res => {
                    console.log("Executed SQL - TABLE SAVE_PURCHASE created");
                    resolve(res);
                })
                    .catch(e => {
                    console.log(e);
                    reject(e);
                });
            }
            catch (error) {
                console.log("error: createScanTable" + error);
                reject(error);
            }
        });
    }
};
DatabaseService.ctorParameters = () => [
    { type: _ionic_native_sqlite_ngx__WEBPACK_IMPORTED_MODULE_2__["SQLite"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["Platform"] }
];
DatabaseService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: "root"
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_native_sqlite_ngx__WEBPACK_IMPORTED_MODULE_2__["SQLite"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["Platform"]])
], DatabaseService);



/***/ }),

/***/ "./src/app/services/database/history.service.ts":
/*!******************************************************!*\
  !*** ./src/app/services/database/history.service.ts ***!
  \******************************************************/
/*! exports provided: HistoryService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HistoryService", function() { return HistoryService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _database_database_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../database/database.service */ "./src/app/services/database/database.service.ts");
/* harmony import */ var _api_api_history_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../api/api-history.service */ "./src/app/services/api/api-history.service.ts");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _ionic_native_network_ngx__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic-native/network/ngx */ "./node_modules/@ionic-native/network/ngx/index.js");






let HistoryService = class HistoryService {
    constructor(db, api, network) {
        this.db = db;
        this.api = api;
        this.network = network;
        this.history = [];
        this.url = api.getUrl();
        this.connection = this.network.type;
        this.network.onDisconnect().subscribe(() => {
            console.log("network was disconnected :-(");
            this.connection = this.network.type;
        });
        // watch network for a connection
        this.network.onConnect().subscribe(() => {
            console.log("network connected!");
            // We just got a connection but we need to wait briefly
            // before we determine the connection type. Might need to wait.
            // prior to doing any api requests as well.
            setTimeout(() => {
                if (this.network.type === "wifi") {
                    console.log("we got a wifi connection, woohoo!");
                    this.connection = this.network.type;
                }
            }, 3000);
        });
    }
    callPurchaseHistory() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            this.clearDatabase();
            try {
                var SQLArray = []; //hold the sql commands
                var i = 0;
                this.history = [];
                console.log("HistoryService: CallPurchaseHistory: 1. ===  START LOADING HISTORY === " +
                    moment__WEBPACK_IMPORTED_MODULE_4__().format("YYYYMMDDHHmmss"));
                console.log("HistoryService: CallPurchaseHistory: 2.url: " + this.url);
                yield this.api.retPurchaseHistory(this.url);
                console.log("HistoryService: CallPurchaseHistory: 3. Called and finished Api.");
                this.history = this.api.getHistoryArray();
                console.log("HistoryService: CallPurchaseHistory: 4. PurchaseHistory Api: ", this.history.length);
                yield this.history.forEach(purchase => {
                    console.log("data going in" + purchase.meq_purchase_history_id, purchase.animal_name, purchase.origin, purchase.breed, purchase.purchase_date, purchase.overall_rating, purchase.animal_tag, purchase.breed_name);
                    SQLArray.push([
                        "INSERT OR REPLACE INTO PURCHASE_HISTORY(ID, ANIMAL_NAME, ORIGIN, BREED, PURCHASE_DATE, OVERALL_RATING, ANIMAL_TAG, BREED_NAME) VALUES (?,?,?,?,?,?,?,?)",
                        [purchase.meq_purchase_history_id,
                            purchase.animal_name,
                            purchase.origin,
                            purchase.breed,
                            purchase.purchase_date,
                            purchase.overall_rating,
                            purchase.animal_tag,
                            purchase.breed_name
                        ]
                    ]);
                    i = i + 1;
                });
                console.log("HistoryService: CallPurchaseHistory: 5. sqlBatch calling for " + i + " rows.");
                yield this.clearDatabase();
                yield this.db.connection.sqlBatch(SQLArray).then(data => {
                    console.log("HistoryService: CallPurchaseHistory: 6. sqlBatch complete");
                }, error => {
                    console.log("HistoryService: CallPurchaseHistory: 6. error:" + error.message);
                });
                console.log("HistoryService: CallPurchaseHistory: 7. Clearing local Array");
                this.history = [];
                SQLArray = [];
                // clear the health array
                console.log("HistoryService: CallPurchaseHistory: 8. reset api array: ");
                this.api.clearHistoryArray();
                console.log("HistoryService: CallPurchaseHistory: 9. === FINISHED LOADING PURCHASE HISTORY === " +
                    moment__WEBPACK_IMPORTED_MODULE_4__().format("YYYYMMDDHHmmss"));
            }
            catch (error) {
                console.log("HistoryService: CallPurchaseHistory" + error.message);
            }
        });
    }
    //  Load Purchases from SQLite db into array
    loadPurchases() {
        return new Promise((resolve, reject) => {
            try {
                console.log("db created - loading into array");
                this.db.connection
                    .executeSql("SELECT * FROM PURCHASE_HISTORY", [])
                    .then(res => {
                    var history = [];
                    console.log("loadPurchases - now loading into array");
                    console.log(res.rows.length);
                    for (var i = 0; i < res.rows.length; i++) {
                        history.push({
                            id: res.rows.item(i).ID,
                            animal_name: res.rows.item(i).ANIMAL_NAME,
                            origin: res.rows.item(i).ORIGIN,
                            breed: res.rows.item(i).BREED,
                            purchase_date: res.rows.item(i).PURCHASE_DATE,
                            overall_rating: res.rows.item(i).OVERALL_RATING,
                            animal_tag: res.rows.item(i).ANIMAL_TAG,
                            breed_name: res.rows.item(i).BREED_NAME
                        });
                    }
                    console.log(history);
                    resolve(history);
                })
                    .catch(e => {
                    console.log(e);
                    reject(e);
                });
            }
            catch (error) {
                console.log("error: loadPurchases" + error);
                reject(error);
            }
        });
    }
    saveDismiss(id) {
        console.log("---------------- DISMISS 2 - HistoryService: dismiss ------------------------");
        console.log("HistoryService: dismiss " + id);
        this.db.connection.executeSql("UPDATE PURCHASE_HISTORY set DISMISS = 'TRUE' WHERE ID = ?", [id]);
    }
    // Post API Data to the API Server  for all Serves that haven't already been sent
    uploadDismiss() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            console.log("---------------- DISMISS 3 - HistoryService: uploadDismiss ------------------------");
            try {
                console.log("PostAPIEventData");
                var i = 0;
                var k;
                // this code will kick off the api POST PUT and will update the record if sent back
                // As its an observable can't wait for it to finish with promises
                console.log("(A) SendData : ==GETTING RECORDS TO SEND===");
                this.db.connection
                    .executeSql("SELECT rowid, * FROM PURCHASE_HISTORY WHERE DISMISS IS NOT NULL", [])
                    .then((data) => {
                    console.log("sending  " + data.rows.length + " ratings.");
                    console.log(JSON.stringify(data));
                    if (data.rows.length > 0) {
                        var currentRowId;
                        // iterate through returned records and send via the api
                        for (k = 0; k < data.rows.length; k++) {
                            currentRowId = data.rows.item(k).rowid;
                            console.log("(B) calling sendEvent ID", data.rows.item(k).ID);
                            if (this.connection != "none") {
                                this.api
                                    .sendPostRequestDismiss(data.rows.item(k).ID)
                                    .then((res) => {
                                    console.log("dismiss sent");
                                    this.callPurchaseHistory();
                                })
                                    .catch((error) => {
                                    console.log("(ERROR) sendEvent " + error.message);
                                }); // sendServe
                            }
                        } // end of for
                        // wait for the promises to be over before moving on
                    }
                })
                    .catch((error) => {
                    console.log("uploadDismiss (ERROR) sendData database Error " + error.message);
                });
                console.log("Data saveEventAPIData FINISHED");
                //console.log("UploadEvent() :loading event table data");
                //this.loadEvent(); -- To view the data in event table
            }
            catch (error) {
                console.log("uploadDismiss Error" + error.message);
            }
        });
    }
    clearDatabase() {
        this.db.connection.executeSql("DELETE FROM PURCHASE_HISTORY", []);
    }
};
HistoryService.ctorParameters = () => [
    { type: _database_database_service__WEBPACK_IMPORTED_MODULE_2__["DatabaseService"] },
    { type: _api_api_history_service__WEBPACK_IMPORTED_MODULE_3__["ApiHistoryService"] },
    { type: _ionic_native_network_ngx__WEBPACK_IMPORTED_MODULE_5__["Network"] }
];
HistoryService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_database_database_service__WEBPACK_IMPORTED_MODULE_2__["DatabaseService"], _api_api_history_service__WEBPACK_IMPORTED_MODULE_3__["ApiHistoryService"], _ionic_native_network_ngx__WEBPACK_IMPORTED_MODULE_5__["Network"]])
], HistoryService);



/***/ }),

/***/ "./src/app/services/database/product-info.service.ts":
/*!***********************************************************!*\
  !*** ./src/app/services/database/product-info.service.ts ***!
  \***********************************************************/
/*! exports provided: ProductInfoService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductInfoService", function() { return ProductInfoService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _database_database_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../database/database.service */ "./src/app/services/database/database.service.ts");
/* harmony import */ var _api_api_product_info_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../api/api-product-info.service */ "./src/app/services/api/api-product-info.service.ts");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _ionic_native_network_ngx__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic-native/network/ngx */ "./node_modules/@ionic-native/network/ngx/index.js");






let ProductInfoService = class ProductInfoService {
    constructor(db, api, network) {
        this.db = db;
        this.api = api;
        this.network = network;
        this.info = [];
        this.url = api.getUrl();
        this.connection = this.network.type;
        this.network.onDisconnect().subscribe(() => {
            console.log("network was disconnected :-(");
            this.connection = this.network.type;
        });
        // watch network for a connection
        this.network.onConnect().subscribe(() => {
            console.log("network connected!");
            // We just got a connection but we need to wait briefly
            // before we determine the connection type. Might need to wait.
            // prior to doing any api requests as well.
            setTimeout(() => {
                if (this.network.type === "wifi") {
                    console.log("we got a wifi connection, woohoo!");
                    this.connection = this.network.type;
                }
            }, 3000);
        });
    }
    saveScan(tag, date) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            console.log("Event Service: saveEvent");
            this.db.connection.executeSql("INSERT INTO SCAN(TAG, EVENT_DATE) VALUES (?,?)", [
                tag,
                date
            ]);
        });
    }
    callProductInfo(tag) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            try {
                var SQLArray = []; //hold the sql commands
                var i = 0;
                this.info = [];
                console.log("ProductInfoService: CallProductInfo: 1. ===  START LOADING INFO === " +
                    moment__WEBPACK_IMPORTED_MODULE_4__().format("YYYYMMDDHHmmss"));
                console.log("ProductInfoService: CallProductInfo: 2.url: " + this.url);
                yield this.api.retProductInfo(this.url, tag);
                console.log("ProductInfoService: CallProductInfo: 3. Called and finished Api.");
                this.info = this.api.getInfoArray();
                console.log("ProductInfoService: CallProductInfo: 4. ProductInfo Api: ", this.info.length);
                yield this.info.forEach(info => {
                    console.log("data going in" + info.ani_id, info.animal_name, info.origin, info.sex, info.cut_of_meat, info.breed, info.meat_quality_rating, info.green_rating, info.animal_breed_description, info.dry_aged, info.farmer_name, info.animal_tag, info.breed_name);
                    SQLArray.push([
                        "INSERT OR REPLACE INTO INFO(ANI_ID, ANIMAL_NAME, ORIGIN, SEX, CUT_OF_MEAT, BREED, MEAT_QUALITY_RATING, GREEN_RATING, ANIMAL_BREED_DESCRIPTION, DRY_AGED, FARMER_NAME, ANIMAL_TAG, BREED_NAME) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)",
                        [info.ani_id,
                            info.animal_name,
                            info.origin,
                            info.sex,
                            info.cut_of_meat,
                            info.breed,
                            info.meat_quality_rating,
                            info.green_rating,
                            info.animal_breed_description,
                            info.dry_aged,
                            info.farmer_name,
                            info.animal_tag,
                            info.breed_name
                        ]
                    ]);
                    i = i + 1;
                });
                console.log("ProductInfoService: CallProductInfo: 5. sqlBatch calling for " + i + " rows.");
                yield this.clearDatabase();
                yield this.db.connection.sqlBatch(SQLArray).then(data => {
                    console.log("ProductInfoService: CallProductInfo: 6. sqlBatch complete");
                }, error => {
                    console.log("ProductInfoService: CallProductInfo: 6. error:" + error.message);
                });
                console.log("ProductInfoService: CallProductInfo: 7. Clearing local Array");
                this.info = [];
                SQLArray = [];
                // clear the health array
                console.log("ProductInfoService: CallProductInfo: 8. reset api array: ");
                this.api.clearInfoArray();
                console.log("ProductInfoService: CallProductInfo: 9. === FINISHED LOADING PRODUCT INFO === " +
                    moment__WEBPACK_IMPORTED_MODULE_4__().format("YYYYMMDDHHmmss"));
            }
            catch (error) {
                console.log("ProductInfoService: CallProductInfo" + error.message);
            }
        });
    }
    //  Load herd from SQLite db into array
    loadInfo() {
        return new Promise((resolve, reject) => {
            try {
                console.log("db created - loading into array");
                this.db.connection
                    .executeSql("SELECT * FROM INFO", [])
                    .then(res => {
                    var info = [];
                    console.log("loadInfo - now loading into array");
                    console.log(res.rows.length);
                    for (var i = 0; i < res.rows.length; i++) {
                        info.push({
                            ani_id: res.rows.item(i).ANI_ID,
                            animal_name: res.rows.item(i).ANIMAL_NAME,
                            origin: res.rows.item(i).ORIGIN,
                            sex: res.rows.item(i).SEX,
                            cut_of_meat: res.rows.item(i).CUT_OF_MEAT,
                            breed: res.rows.item(i).BREED,
                            meat_quality_rating: res.rows.item(i).MEAT_QUALITY_RATING,
                            green_rating: res.rows.item(i).GREEN_RATING,
                            dry_aged: res.rows.item(i).DRY_AGED,
                            farmer_name: res.rows.item(i).FARMER_NAME,
                            animal_tag: res.rows.item(i).ANIMAL_TAG,
                            breed_name: res.rows.item(i).BREED_NAME,
                        });
                    }
                    this.clearDatabase();
                    console.log(info);
                    resolve(info);
                })
                    .catch(e => {
                    console.log(e);
                    reject(e);
                });
            }
            catch (error) {
                console.log("error: loadHerd - herd" + error);
                reject(error);
            }
        });
    }
    savePurchase(tag, cut, date, source) {
        console.log("Event Service: saveEvent");
        this.db.connection.executeSql("INSERT INTO SAVE_PURCHASE(TAG, CUT, EVENT_DATE, SOURCE ) VALUES (?,?,?,?)", [
            tag,
            cut,
            date,
            source
        ]);
    }
    // Post API Data to the API Server  for all Serves that haven't already been sent
    uploadPurchase() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            try {
                console.log("PostAPIEventData");
                var i = 0;
                var k;
                // this code will kick off the api POST PUT and will update the record if sent back
                // As its an observable can't wait for it to finish with promises
                console.log("(A) SendData : ==GETTING RECORDS TO SEND===");
                this.db.connection
                    .executeSql("SELECT rowid, * FROM SAVE_PURCHASE WHERE API_POST_DTM is null", [])
                    .then((data) => {
                    console.log("sending  " + data.rows.length + " events.");
                    console.log(JSON.stringify(data));
                    if (data.rows.length > 0) {
                        var currentRowId;
                        // iterate through returned records and send via the api
                        for (k = 0; k < data.rows.length; k++) {
                            currentRowId = data.rows.item(k).rowid;
                            console.log("(B) calling sendEvent ID", data.rows.item(k).rowid, "API_POST_DTM:", data.rows.item(k).API_POST_DTM, "TAG:", data.rows.item(k).TAG, " EVENT_DATE:", data.rows.item(k).EVENT_DATE, "CUT:", data.rows.item(k).CUT, "SOURCE:", data.rows.item(k).SOURCE);
                            if (this.connection != "none") {
                                console.log("params in db service" + data.rows.item(k).TAG, data.rows.item(k).CUT, data.rows.item(k).SOURCE);
                                this.api
                                    .sendPostRequest(data.rows.item(k).TAG, data.rows.item(k).CUT, data.rows.item(k).SOURCE)
                                    .then((res) => {
                                    this.UpdateSavePurchaseAPISent(currentRowId);
                                })
                                    .catch(error => {
                                    console.log("(ERROR) sendEvent " + error.message);
                                }); // sendServe
                            }
                        } // end of for
                        // wait for the promises to be over before moving on
                    }
                })
                    .catch(error => {
                    console.log("(ERROR) sendData database Error " + error.message);
                });
                console.log("Data saveEventAPIData FINISHED");
                //console.log("UploadEvent() :loading event table data");
                //this.loadEvent(); -- To view the data in event table
            }
            catch (error) {
                console.log("sendData  99 Error" + error.message);
            }
        });
    }
    //update that the event was sent
    UpdateSavePurchaseAPISent(id) {
        let promise = new Promise((resolve, reject) => {
            try {
                this.db.connection
                    .executeSql("UPDATE SAVE_PURCHASE SET API_POST_DTM = ? WHERE rowid = ?", [
                    parseInt(moment__WEBPACK_IMPORTED_MODULE_4__().format("YYYYMMDDHHmmss")),
                    id
                ])
                    .then((data) => {
                    resolve(data);
                    console.log("API_POST_DTM updated");
                });
            }
            catch (error) {
                //  this.loader.duration = 5000;
                reject(error);
                console.log("UpdateSavePurchaseAPISent Error" + error.message);
            }
        });
    }
    clearDatabase() {
        this.db.connection.executeSql("DELETE FROM INFO WHERE ANI_ID IS NOT NULL", []);
    }
};
ProductInfoService.ctorParameters = () => [
    { type: _database_database_service__WEBPACK_IMPORTED_MODULE_2__["DatabaseService"] },
    { type: _api_api_product_info_service__WEBPACK_IMPORTED_MODULE_3__["ApiProductInfoService"] },
    { type: _ionic_native_network_ngx__WEBPACK_IMPORTED_MODULE_5__["Network"] }
];
ProductInfoService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_database_database_service__WEBPACK_IMPORTED_MODULE_2__["DatabaseService"], _api_api_product_info_service__WEBPACK_IMPORTED_MODULE_3__["ApiProductInfoService"], _ionic_native_network_ngx__WEBPACK_IMPORTED_MODULE_5__["Network"]])
], ProductInfoService);



/***/ }),

/***/ "./src/app/services/database/rate.service.ts":
/*!***************************************************!*\
  !*** ./src/app/services/database/rate.service.ts ***!
  \***************************************************/
/*! exports provided: RateService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RateService", function() { return RateService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _database_database_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../database/database.service */ "./src/app/services/database/database.service.ts");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _ionic_native_network_ngx__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic-native/network/ngx */ "./node_modules/@ionic-native/network/ngx/index.js");
/* harmony import */ var _api_api_rate_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../api/api-rate.service */ "./src/app/services/api/api-rate.service.ts");






let RateService = class RateService {
    constructor(db, network, api) {
        this.db = db;
        this.network = network;
        this.api = api;
        this.url = api.getUrl();
        this.connection = this.network.type;
        this.network.onDisconnect().subscribe(() => {
            console.log("network was disconnected :-(");
            this.connection = this.network.type;
        });
        // watch network for a connection
        this.network.onConnect().subscribe(() => {
            console.log("network connected!");
            // We just got a connection but we need to wait briefly
            // before we determine the connection type. Might need to wait.
            // prior to doing any api requests as well.
            setTimeout(() => {
                if (this.network.type === "wifi") {
                    console.log("we got a wifi connection, woohoo!");
                    this.connection = this.network.type;
                }
            }, 3000);
        });
    }
    saveRating(appearance, tenderness, juciness, flavour, marbling, overall, method, doneness, id) {
        console.log("RateService: saveRating " + appearance, tenderness, juciness, flavour, marbling, overall, "doness" + doneness, "method" + method, id);
        this.db.connection.executeSql("INSERT INTO RATING(APPEARANCE, TENDERNESS, JUCINESS, FLAVOUR, MARBLING, OVERALL, DONENESS, METHOD, ID) VALUES (?,?,?,?,?,?,?,?,?)", [appearance, tenderness, juciness, flavour, marbling, overall, doneness, method, id]);
    }
    // Post API Data to the API Server  for all Serves that haven't already been sent
    uploadRating() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            try {
                console.log("PostAPIEventData");
                var i = 0;
                var k;
                // this code will kick off the api POST PUT and will update the record if sent back
                // As its an observable can't wait for it to finish with promises
                console.log("(A) SendData : ==GETTING RECORDS TO SEND===");
                this.db.connection
                    .executeSql("SELECT rowid, * FROM RATING WHERE API_POST_DTM is null", [])
                    .then((data) => {
                    console.log("sending  " + data.rows.length + " ratings.");
                    console.log(JSON.stringify(data));
                    if (data.rows.length > 0) {
                        var currentRowId;
                        // iterate through returned records and send via the api
                        for (k = 0; k < data.rows.length; k++) {
                            currentRowId = data.rows.item(k).rowid;
                            console.log("(B) calling sendEvent ID", data.rows.item(k).TENDERNESS, "APPEARANCE", data.rows.item(k).APPEARANCE, "API_POST_DTM:", data.rows.item(k).API_POST_DTM, "JUCINESS:", data.rows.item(k).JUCINESS, "FLAVOUR:", data.rows.item(k).FLAVOUR, "MARBLING:", data.rows.item(k).MARBLING, "OVERALL:", data.rows.item(k).OVERALL, "METHOD:", data.rows.item(k).METHOD, "DONENESS:", data.rows.item(k).DONENESS, "ID:", data.rows.item(k).ID);
                            console.log("data before send post ------------------------------------ " + data.rows.item(k).TENDERNESS, data.rows.item(k).APPEARANCE, data.rows.item(k).JUCINESS, data.rows.item(k).FLAVOUR, data.rows.item(k).MARBLING, data.rows.item(k).OVERALL, data.rows.item(k).METHOD, data.rows.item(k).DONENESS, data.rows.item(k).ID);
                            if (this.connection != "none") {
                                this.api
                                    .sendPostRequest(data.rows.item(k).APPEARANCE, data.rows.item(k).TENDERNESS, data.rows.item(k).JUCINESS, data.rows.item(k).FLAVOUR, data.rows.item(k).MARBLING, data.rows.item(k).OVERALL, data.rows.item(k).METHOD, data.rows.item(k).DONENESS, data.rows.item(k).ID)
                                    .then((res) => {
                                    this.UpdateRateAPISent(currentRowId);
                                })
                                    .catch((error) => {
                                    console.log("(ERROR) sendEvent " + error.message);
                                }); // sendServe
                            }
                        } // end of for
                        // wait for the promises to be over before moving on
                    }
                })
                    .catch((error) => {
                    console.log("uploadRating (ERROR) sendData database Error " + error.message);
                });
                console.log("Data saveEventAPIData FINISHED");
                //console.log("UploadEvent() :loading event table data");
                //this.loadEvent(); -- To view the data in event table
            }
            catch (error) {
                console.log("uploadRating Error" + error.message);
            }
        });
    }
    //update that the event was sent
    UpdateRateAPISent(id) {
        let promise = new Promise((resolve, reject) => {
            try {
                this.db.connection
                    .executeSql("UPDATE RATING SET API_POST_DTM = ? WHERE rowid = ?", [parseInt(moment__WEBPACK_IMPORTED_MODULE_3__().format("YYYYMMDDHHmmss")), id])
                    .then((data) => {
                    resolve(data);
                    console.log("API_POST_DTM updated");
                });
            }
            catch (error) {
                reject(error);
                console.log("UpdateRateAPISent Error" + error.message);
            }
        });
    }
};
RateService.ctorParameters = () => [
    { type: _database_database_service__WEBPACK_IMPORTED_MODULE_2__["DatabaseService"] },
    { type: _ionic_native_network_ngx__WEBPACK_IMPORTED_MODULE_4__["Network"] },
    { type: _api_api_rate_service__WEBPACK_IMPORTED_MODULE_5__["ApiRateService"] }
];
RateService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: "root",
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_database_database_service__WEBPACK_IMPORTED_MODULE_2__["DatabaseService"],
        _ionic_native_network_ngx__WEBPACK_IMPORTED_MODULE_4__["Network"],
        _api_api_rate_service__WEBPACK_IMPORTED_MODULE_5__["ApiRateService"]])
], RateService);



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

const environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm2015/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");





if (_environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_2__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_3__["AppModule"])
    .catch(err => console.log(err));


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /Users/liamchambers/Documents/meateatingquality/src/main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main-es2015.js.map